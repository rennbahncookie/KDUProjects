
void setup()
{
 size(500, 1000);

}

void draw()
{
 background(190,190,190);
 stroke(255);
 
//Body_Outline
 fill(255);
 stroke(0);
 beginShape();
   vertex(250.0, 93.0);
   vertex(209.0, 93.0);
   vertex(200.0, 99.0);
   vertex(196.0, 115.0);
   vertex(196.0, 126.0);
   vertex(196.0, 140.0);
   vertex(197.0, 152.0);
   vertex(183.0, 152.0);
   vertex(176.0, 160.0);
   vertex(174.0, 148.0);
   vertex(169.0, 149.0);
   vertex(150.0, 63.0);
   vertex(148.0, 64.0);
   vertex(147.0, 54.0);
   vertex(149.0, 53.0);
   vertex(144.0, 31.0);
   vertex(138.0, 23.0);
   vertex(121.0, 26.0);
   vertex(121.0, 35.0);
   vertex(126.0, 60.0);
   vertex(128.0, 59.0);
   vertex(129.0, 68.0);
   vertex(128.0, 69.0);
   vertex(145.0, 153.0);
   vertex(143.0, 155.0);
   vertex(139.0, 150.0);
   vertex(65.0, 140.0);
   vertex(40.0, 151.0);
   vertex(61.0, 217.0);
   vertex(63.0, 220.0);
   vertex(63.0, 227.0);
   vertex(65.0, 230.0);
   vertex(59.0, 262.0);
   vertex(56.0, 298.0);
   vertex(49.0, 298.0);
   vertex(44.0, 323.0);
   vertex(46.0, 326.0);
   vertex(40.0, 350.0);
   vertex(23.0, 446.0);
   vertex(32.0, 446.0);
   vertex(34.0, 457.0);
   vertex(43.0, 457.0);
   vertex(43.0, 459.0);
   vertex(38.0, 459.0);
   vertex(34.0, 474.0);
   vertex(35.0, 486.0);
   vertex(36.0, 506.0);
   vertex(39.5, 514.0);
   vertex(46.0, 517.0);
   vertex(45.0, 518.0);
   vertex(58.0, 527.0);
   vertex(63.0, 530.0);
   vertex(73.0, 534.0);
   vertex(84.0, 534.0);
   vertex(84.0, 533.0);
   vertex(88.0, 533.0);
   vertex(105.0, 519.0);
   vertex(101.0, 515.0);
   vertex(105.0, 507.0);
   vertex(97.0, 505.0);
   vertex(97.0, 493.0);
   vertex(94.0, 489.0);
   vertex(87.0, 491.0);
   vertex(82.0, 500.0);
   vertex(82.0, 506.0);
   vertex(80.0, 506.0);
   vertex(70.0, 478.0);
   vertex(70.0, 473.0);
   vertex(67.0, 468.0);
   vertex(66.0, 461.0);
   vertex(63.0, 460.0);
   vertex(74.0, 460.0);
   vertex(76.0, 455.0);
   vertex(84.0, 456.0);
   vertex(86.0, 451.0);
   vertex(96.0, 450.0);
   vertex(90.0, 435.0);
   vertex(104.0, 354.0);
   vertex(103.0, 334.0);
   vertex(105.0, 334.0);
   vertex(111.0, 308.0);
   vertex(103.0, 304.0);
   vertex(108.0, 283.0);
   vertex(114.0, 245.0);
   vertex(125.0, 250.0);
   vertex(140.0, 238.0);
   vertex(140.0, 230.0);
   vertex(144.0, 230.0);
   vertex(145.0, 237.0);
   vertex(150.0, 238.0);
   vertex(150.0, 259.0);
   vertex(152.0, 275.0);
   vertex(163.0, 280.0);
   vertex(170.0, 312.0);
   vertex(175.0, 319.0);
   vertex(171.0, 342.0);
   vertex(170.0, 355.0);
   vertex(165.0, 355.0);
   vertex(159.0, 343.0);
   vertex(150.0, 340.0);
   vertex(141.0, 345.0);
   vertex(125.0, 397.0);
   vertex(130.0, 408.0);
   vertex(122.0, 435.0);
   vertex(128.0, 452.0);
   vertex(136.0, 455.0);
   vertex(138.0, 452.0);
   vertex(140.0, 453.0);
   vertex(134.0, 492.0);
   vertex(130.0, 534.0);
   vertex(130.0, 579.0);
   vertex(132.0, 597.0);
   vertex(117.0, 599.0);
   vertex(113.0, 629.0);
   vertex(114.0, 632.0);
   vertex(108.0, 645.0);
   vertex(96.0, 674.0);
   vertex(93.0, 705.0);
   vertex(99.0, 754.0);
   vertex(104.0, 785.0);
   vertex(94.0, 837.0);
   vertex(89.0, 842.0);
   vertex(81.0, 868.0);
   vertex(69.0, 870.0);
   vertex(56.0, 887.0);
   vertex(48.0, 893.0);
   vertex(43.0, 932.0);
   vertex(62.0, 939.0);
   vertex(60.0, 944.0);
   vertex(50.0, 959.0);
   vertex(40.0, 993.0);
   vertex(173.0, 993.0);
   vertex(164.0, 951.0);
   vertex(157.0, 951.0);
   vertex(157.0, 945.0);
   vertex(172.0, 942.0);
   vertex(176.0, 940.0);
   vertex(176.0, 900.0);
   vertex(172.0, 895.0);
   vertex(162.0, 894.0);
   vertex(163.0, 850.0);
   vertex(160.0, 846.0);
   vertex(164.0, 790.0);
   vertex(186.0, 746.0);
   vertex(202.0, 690.0);
   vertex(202.0, 663.0);
   vertex(198.0, 660.0);
   vertex(198.0, 642.0);
   vertex(202.0, 611.0);
   vertex(188.0, 605.0);
   vertex(188.0, 598.0);
   vertex(200.0, 565.0);
   vertex(210.0, 530.0);
   vertex(217.0, 495.0);
   vertex(218.0, 482.0);
   vertex(222.0, 482.0);
   vertex(222.0, 491.0);
   vertex(228.0, 503.0);
   vertex(250.0, 503.0);
 endShape();
 
  fill(255);
 stroke(0);
 beginShape();
   vertex_sym(250.0, 93.0);
   vertex_sym(209.0, 93.0);
   vertex_sym(200.0, 99.0);
   vertex_sym(196.0, 115.0);
   vertex_sym(196.0, 126.0);
   vertex_sym(196.0, 140.0);
   vertex_sym(197.0, 152.0);
   vertex_sym(183.0, 152.0);
   vertex_sym(176.0, 160.0);
   vertex_sym(174.0, 148.0);
   vertex_sym(169.0, 149.0);
   vertex_sym(150.0, 63.0);
   vertex_sym(148.0, 64.0);
   vertex_sym(147.0, 54.0);
   vertex_sym(149.0, 53.0);
   vertex_sym(144.0, 31.0);
   vertex_sym(138.0, 23.0);
   vertex_sym(121.0, 26.0);
   vertex_sym(121.0, 35.0);
   vertex_sym(126.0, 60.0);
   vertex_sym(128.0, 59.0);
   vertex_sym(129.0, 68.0);
   vertex_sym(128.0, 69.0);
   vertex_sym(145.0, 153.0);
   vertex_sym(143.0, 155.0);
   vertex_sym(139.0, 150.0);
   vertex_sym(65.0, 140.0);
   vertex_sym(40.0, 151.0);
   vertex_sym(61.0, 217.0);
   vertex_sym(63.0, 220.0);
   vertex_sym(63.0, 227.0);
   vertex_sym(65.0, 230.0);
   vertex_sym(59.0, 262.0);
   vertex_sym(56.0, 298.0);
   vertex_sym(49.0, 298.0);
   vertex_sym(44.0, 323.0);
   vertex_sym(46.0, 326.0);
   vertex_sym(40.0, 350.0);
   vertex_sym(23.0, 446.0);
   vertex_sym(32.0, 446.0);
   vertex_sym(34.0, 457.0);
   vertex_sym(43.0, 457.0);
   vertex_sym(43.0, 459.0);
   vertex_sym(38.0, 459.0);
   vertex_sym(34.0, 474.0);
   vertex_sym(35.0, 486.0);
   vertex_sym(36.0, 506.0);
   vertex_sym(39.5, 514.0);
   vertex_sym(46.0, 517.0);
   vertex_sym(45.0, 518.0);
   vertex_sym(58.0, 527.0);
   vertex_sym(63.0, 530.0);
   vertex_sym(73.0, 534.0);
   vertex_sym(84.0, 534.0);
   vertex_sym(84.0, 533.0);
   vertex_sym(88.0, 533.0);
   vertex_sym(105.0, 519.0);
   vertex_sym(101.0, 515.0);
   vertex_sym(105.0, 507.0);
   vertex_sym(97.0, 505.0);
   vertex_sym(97.0, 493.0);
   vertex_sym(94.0, 489.0);
   vertex_sym(87.0, 491.0);
   vertex_sym(82.0, 500.0);
   vertex_sym(82.0, 506.0);
   vertex_sym(80.0, 506.0);
   vertex_sym(70.0, 478.0);
   vertex_sym(70.0, 473.0);
   vertex_sym(67.0, 468.0);
   vertex_sym(66.0, 461.0);
   vertex_sym(63.0, 460.0);
   vertex_sym(74.0, 460.0);
   vertex_sym(76.0, 455.0);
   vertex_sym(84.0, 456.0);
   vertex_sym(86.0, 451.0);
   vertex_sym(96.0, 450.0);
   vertex_sym(90.0, 435.0);
   vertex_sym(104.0, 354.0);
   vertex_sym(103.0, 334.0);
   vertex_sym(105.0, 334.0);
   vertex_sym(111.0, 308.0);
   vertex_sym(103.0, 304.0);
   vertex_sym(108.0, 283.0);
   vertex_sym(114.0, 245.0);
   vertex_sym(125.0, 250.0);
   vertex_sym(140.0, 238.0);
   vertex_sym(140.0, 230.0);
   vertex_sym(144.0, 230.0);
   vertex_sym(145.0, 237.0);
   vertex_sym(150.0, 238.0);
   vertex_sym(150.0, 259.0);
   vertex_sym(152.0, 275.0);
   vertex_sym(163.0, 280.0);
   vertex_sym(170.0, 312.0);
   vertex_sym(175.0, 319.0);
   vertex_sym(171.0, 342.0);
   vertex_sym(170.0, 355.0);
   vertex_sym(165.0, 355.0);
   vertex_sym(159.0, 343.0);
   vertex_sym(150.0, 340.0);
   vertex_sym(141.0, 345.0);
   vertex_sym(125.0, 397.0);
   vertex_sym(130.0, 408.0);
   vertex_sym(122.0, 435.0);
   vertex_sym(128.0, 452.0);
   vertex_sym(136.0, 455.0);
   vertex_sym(138.0, 452.0);
   vertex_sym(140.0, 453.0);
   vertex_sym(134.0, 492.0);
   vertex_sym(130.0, 534.0);
   vertex_sym(130.0, 579.0);
   vertex_sym(132.0, 597.0);
   vertex_sym(117.0, 599.0);
   vertex_sym(113.0, 629.0);
   vertex_sym(114.0, 632.0);
   vertex_sym(108.0, 645.0);
   vertex_sym(96.0, 674.0);
   vertex_sym(93.0, 705.0);
   vertex_sym(99.0, 754.0);
   vertex_sym(104.0, 785.0);
   vertex_sym(94.0, 837.0);
   vertex_sym(89.0, 842.0);
   vertex_sym(81.0, 868.0);
   vertex_sym(69.0, 870.0);
   vertex_sym(56.0, 887.0);
   vertex_sym(48.0, 893.0);
   vertex_sym(43.0, 932.0);
   vertex_sym(62.0, 939.0);
   vertex_sym(60.0, 944.0);
   vertex_sym(50.0, 959.0);
   vertex_sym(40.0, 993.0);
   vertex_sym(173.0, 993.0);
   vertex_sym(164.0, 951.0);
   vertex_sym(157.0, 951.0);
   vertex_sym(157.0, 945.0);
   vertex_sym(172.0, 942.0);
   vertex_sym(176.0, 940.0);
   vertex_sym(176.0, 900.0);
   vertex_sym(172.0, 895.0);
   vertex_sym(162.0, 894.0);
   vertex_sym(163.0, 850.0);
   vertex_sym(160.0, 846.0);
   vertex_sym(164.0, 790.0);
   vertex_sym(186.0, 746.0);
   vertex_sym(202.0, 690.0);
   vertex_sym(202.0, 663.0);
   vertex_sym(198.0, 660.0);
   vertex_sym(198.0, 642.0);
   vertex_sym(202.0, 611.0);
   vertex_sym(188.0, 605.0);
   vertex_sym(188.0, 598.0);
   vertex_sym(200.0, 565.0);
   vertex_sym(210.0, 530.0);
   vertex_sym(217.0, 495.0);
   vertex_sym(218.0, 482.0);
   vertex_sym(222.0, 482.0);
   vertex_sym(222.0, 491.0);
   vertex_sym(228.0, 503.0);
   vertex_sym(250.0, 503.0);
 endShape();
 
 //Eyes_Red
  
  fill(151,55,73);
  beginShape();
    vertex(250.0, 114.0);
    vertex(235.0, 120.0);
    vertex(230.0, 120.0);
    vertex(222.0, 110.0);
    vertex(231.0, 112.0);
    vertex(241.0, 111.0);
    vertex(247.0, 106.0);
    vertex(250.0, 100.0);
  endShape();
  
    fill(151,55,73);
  beginShape();
    vertex_sym(250.0, 114.0);
    vertex_sym(235.0, 120.0);
    vertex_sym(230.0, 120.0);
    vertex_sym(222.0, 110.0);
    vertex_sym(231.0, 112.0);
    vertex_sym(241.0, 111.0);
    vertex_sym(247.0, 106.0);
    vertex_sym(250.0, 100.0);
  endShape();
  
//Eyes_Gray

  fill(102,94,103);
  beginShape();
    vertex(250.0, 106.0);
    vertex(247.0, 106.0);
    vertex(241.0, 111.0);
    vertex(231.0, 112.0);
    vertex(222.0, 110.0);
    vertex(222.0, 100.0);
    vertex(250.0, 100.0);
  endShape();
  
  fill(102,94,103);
  beginShape();
    vertex_sym(250.0, 106.0);
    vertex_sym(247.0, 106.0);
    vertex_sym(241.0, 111.0);
    vertex_sym(231.0, 112.0);
    vertex_sym(222.0, 110.0);
    vertex_sym(222.0, 100.0);
    vertex_sym(250.0, 100.0);
  endShape();
  
//Eye_line
  
  line(230, 105, 230, 119);
  line(241, 111, 241, 117);
  
  line(270, 105, 270, 119);
  line(259, 111, 259, 117);
  
//Eyes_Yellow
  
  fill(253,245,73);
  beginShape();
    vertex(243.0, 103.0);
    vertex(235.0, 108.0);
    vertex(228.0, 106.0);
    vertex(228.0, 103.0);
  endShape();
  
    fill(253,245,73);
  beginShape();
    vertex_sym(243.0, 103.0);
    vertex_sym(235.0, 108.0);
    vertex_sym(228.0, 106.0);
    vertex_sym(228.0, 103.0);
  endShape();
  

 
  
//Mouth_White
  
  fill(255);
  beginShape();
    vertex(250.0, 115.0);
    vertex(241.0, 117.0);
    vertex(236.0, 119.0);
    vertex(229.0, 120.0);
    vertex(229.0, 144.0);
    vertex(241.0, 151.0);
    vertex(243.0, 137.0);
    vertex(246.0, 134.0);
    vertex(250.0, 133.0);
  endShape();
  
  fill(255);
  beginShape();
    vertex_sym(250.0, 115.0);
    vertex_sym(241.0, 117.0);
    vertex_sym(236.0, 119.0);
    vertex_sym(229.0, 120.0);
    vertex_sym(229.0, 144.0);
    vertex_sym(241.0, 151.0);
    vertex_sym(243.0, 137.0);
    vertex_sym(246.0, 134.0);
    vertex_sym(250.0, 133.0);
  endShape();
  
//Nose_lines
  
  line(244, 121, 250, 120);
  line(244, 127, 250, 125);
  
  line(256, 121, 250, 120);
  line(256, 127, 250, 125);
  
//Cheek_lines

  line(229, 119, 233, 139);
  line(233, 139, 241, 151);
  
  line(271, 119, 267, 139);
  line(267, 139, 259, 151);
  

//TopHelmet_Outline
 fill(255);
 stroke(0);
 beginShape();
   vertex(237.7, 62.0);
   vertex(230.0, 65.0);
   vertex(219.0, 74.0);
   vertex(214.0, 80.0);
   vertex(210.0, 93.0);
   vertex(210.0, 100.0);
   vertex(216.0, 103.0);
   vertex(250.0, 103.0);
   vertex(250.0, 62.0);
 endShape();
 

 beginShape();
   vertex_sym(237.7, 62.0);
   vertex_sym(230.0, 65.0);
   vertex_sym(219.0, 74.0);
   vertex_sym(214.0, 80.0);
   vertex_sym(210.0, 93.0);
   vertex_sym(210.0, 100.0);
   vertex_sym(216.0, 103.0);
   vertex_sym(250.0, 103.0);
   vertex_sym(250.0, 62.0);
 endShape();
 
 beginShape();
   vertex(250.0, 103.0);
   vertex(250.0, 62.0);
   vertex(250.0, 98.0);
   vertex(236.0, 89.0);
   vertex(226.0, 93.0);
   vertex(216.0, 103.0);
 endShape();
 
 beginShape();
   vertex_sym(250.0, 103.0);
   vertex_sym(250.0, 62.0);
   vertex_sym(250.0, 98.0);
   vertex_sym(236.0, 89.0);
   vertex_sym(226.0, 93.0);
   vertex_sym(216.0, 103.0);
 endShape();
 
//Head_Outline_Top
 
 beginShape();
   vertex(250.0, 47.0);
   vertex(240.0, 46.0);
   vertex(237.0, 70.0);
   vertex(240.0, 67.0);
   vertex(242.0, 50.0);
   vertex(250.0, 50.0);
 endShape();
 
 beginShape();
   vertex_sym(250.0, 47.0);
   vertex_sym(240.0, 46.0);
   vertex_sym(237.0, 70.0);
   vertex_sym(240.0, 67.0);
   vertex_sym(242.0, 50.0);
   vertex_sym(250.0, 50.0);
 endShape();
 
//Horn_White
  
 beginShape();
    vertex(236.0, 89.0);
    vertex(191.0, 47.0);
    vertex(239.0, 74.0);
    vertex(238.0, 82.0);
    vertex(236.0, 89.0);
  endShape();
 
 beginShape();
  vertex_sym(236.0, 89.0);
  vertex_sym(191.0, 47.0);
  vertex_sym(239.0, 74.0);
  vertex_sym(238.0, 82.0);
  vertex_sym(236.0, 89.0);
 endShape();
  
//Helm_Red
  
 fill(151,55,73);
 beginShape();
   vertex(250.0, 98.0);
   vertex(250.0, 66.0);
   vertex(242.0, 66.0);
   vertex(240.0, 72.0);
   vertex(240.0, 79.0);
   vertex(237.0, 90.0);
 endShape();
 
 fill(151,55,73);
 beginShape();
   vertex_sym(250.0, 98.0);
   vertex_sym(250.0, 66.0);
   vertex_sym(242.0, 66.0);
   vertex_sym(240.0, 72.0);
   vertex_sym(240.0, 79.0);
   vertex_sym(237.0, 90.0);
 endShape();
 
//Helm_Line

  line(239, 81, 250, 86);
  
  line(261, 81, 250, 86);
  
//Mouth_Red
 
 fill(151,55,73);
 beginShape();
   vertex(250.0, 157.0);
   vertex(241.0, 157.0);
   vertex(242.0, 140.0);
   vertex(245.0, 135.0);
   vertex(250.0, 134.0);
 endShape();
 
  fill(151,55,73);
 beginShape();
   vertex_sym(250.0, 157.0);
   vertex_sym(241.0, 157.0);
   vertex_sym(242.0, 140.0);
   vertex_sym(245.0, 135.0);
   vertex_sym(250.0, 134.0);
 endShape();
 
//Helm_LightRed
  
 fill(232,49,88);
 beginShape();
   vertex(250.0, 51.0);
   vertex(250.0, 66.0);
   vertex(240.0, 66.0);
   vertex(242.0, 51.0);
 endShape();
 
 fill(232,49,88);
 beginShape();
   vertex_sym(250.0, 51.0);
   vertex_sym(250.0, 66.0);
   vertex_sym(240.0, 66.0);
   vertex_sym(242.0, 51.0);
 endShape();
 
//Ears_Gray
  
  fill(102,94,103);
  beginShape();
    vertex(210.0, 101.0);
    vertex(210.0, 109.0);
    vertex(211.0, 111.0);
    vertex(210.0, 112.0);
    vertex(210.0, 118.0);
    vertex(213.0, 120.0);
    vertex(211.0, 124.0);
    vertex(212.0, 126.0);
    vertex(214.0, 129.0);
    vertex(213.0, 131.0);
    vertex(213.0, 136.0);
    vertex(214.0, 140.0);
    vertex(212.0, 142.0);
    vertex(213.0, 147.0);
    vertex(215.0, 150.0);
    vertex(210.0, 153.0);
    vertex(206.0, 153.0);
    vertex(204.0, 130.0);
    vertex(200.0, 116.0);
    vertex(201.0, 101.0);
  endShape();
 
   fill(102,94,103);
  beginShape();
    vertex_sym(210.0, 101.0);
    vertex_sym(210.0, 109.0);
    vertex_sym(211.0, 111.0);
    vertex_sym(210.0, 112.0);
    vertex_sym(210.0, 118.0);
    vertex_sym(213.0, 120.0);
    vertex_sym(211.0, 124.0);
    vertex_sym(212.0, 126.0);
    vertex_sym(214.0, 129.0);
    vertex_sym(213.0, 131.0);
    vertex_sym(213.0, 136.0);
    vertex_sym(214.0, 140.0);
    vertex_sym(212.0, 142.0);
    vertex_sym(213.0, 147.0);
    vertex_sym(215.0, 150.0);
    vertex_sym(210.0, 153.0);
    vertex_sym(206.0, 153.0);
    vertex_sym(204.0, 130.0);
    vertex_sym(200.0, 116.0);
    vertex_sym(201.0, 101.0);
  endShape();
  
//Neck_Gray
 fill(102,94,103);
 beginShape();
   vertex(250.0, 172.0);
   vertex(250.0, 157.0);
   vertex(241.0, 157.0);
   vertex(241.0, 151.0);
   vertex(229.0, 144.0);
   vertex(229.0, 153.0);
   vertex(213.0, 159.0);
   vertex(197.0, 152.0);
   vertex(183.0, 153.0);
   vertex(176.0, 161.0);
   vertex(176.0, 168.0);
   vertex(214.0, 172.0);
 endShape();
 
 fill(102,94,103);
 beginShape();
   vertex_sym(250.0, 172.0);
   vertex_sym(250.0, 157.0);
   vertex_sym(241.0, 157.0);
   vertex_sym(241.0, 151.0);
   vertex_sym(229.0, 144.0);
   vertex_sym(229.0, 153.0);
   vertex_sym(213.0, 159.0);
   vertex_sym(197.0, 152.0);
   vertex_sym(183.0, 153.0);
   vertex_sym(176.0, 161.0);
   vertex_sym(176.0, 168.0);
   vertex_sym(214.0, 172.0);
 endShape();
 
//Cheeks
  
  fill(255);
  beginShape();
    vertex(222.0, 103.0);
    vertex(222.0, 111.0);
    vertex(229.0, 118.0);
    vertex(229.0, 153.0);
    vertex(221.0, 158.0);
    vertex(214.0, 158.0);
    vertex(222.0, 152.0);
    vertex(222.0, 118.0);
    vertex(215.0, 111.0);
    vertex(215.0, 103.0);
  endShape();
  
  fill(255);
  beginShape();
    vertex(197.0, 153.0);
    vertex(212.0, 153.0);
    vertex(209.0, 155.0);
    vertex(220.0, 166.0);
    vertex(217.0, 168.0);
    vertex(213.0, 168.0);
    vertex(200.0, 154.0);
  endShape();

  fill(255);
  beginShape();
    vertex_sym(222.0, 103.0);
    vertex_sym(222.0, 111.0);
    vertex_sym(229.0, 118.0);
    vertex_sym(229.0, 153.0);
    vertex_sym(221.0, 158.0);
    vertex_sym(214.0, 158.0);
    vertex_sym(222.0, 152.0);
    vertex_sym(222.0, 118.0);
    vertex_sym(215.0, 111.0);
    vertex_sym(215.0, 103.0);
  endShape();
  
  fill(255);
  beginShape();
    vertex_sym(197.0, 153.0);
    vertex_sym(212.0, 153.0);
    vertex_sym(209.0, 155.0);
    vertex_sym(220.0, 166.0);
    vertex_sym(217.0, 168.0);
    vertex_sym(213.0, 168.0);
    vertex_sym(200.0, 154.0);
  endShape();
  
//Cheek_Lines

line(216, 110, 221, 110);
line(223, 119, 229, 119);
line(223, 152, 229, 152);

line(284, 110, 279, 110);
line(277, 119, 271, 119);
line(277, 152, 271, 152);

//Chest_Yellow

  fill(244,194,78);
  beginShape();
    vertex(250.0, 172.0);
    vertex(250.0, 202.0);
    vertex(220.0, 202.0);
    vertex(193.0, 169.0);
    vertex(201.0, 155.0);
    vertex(212.0, 166.0);
    vertex(215.0, 172.0);
  endShape();
  
    fill(244,194,78);
  beginShape();
    vertex_sym(250.0, 172.0);
    vertex_sym(250.0, 202.0);
    vertex_sym(220.0, 202.0);
    vertex_sym(193.0, 169.0);
    vertex_sym(201.0, 155.0);
    vertex_sym(212.0, 166.0);
    vertex_sym(215.0, 172.0);
  endShape();
  
  line(215, 172, 223, 198);
  line(223, 198, 250, 198);
  line(250, 189, 221, 189);
  line(250, 171, 215, 171);
  line(250, 181, 218, 181);
  
  line(285, 172, 277, 198);
  line(277, 198, 250, 198);
  line(250, 189, 279, 189);
  line(250, 171, 285, 171);
  line(250, 181, 282, 181);
  
//Chest_red
  fill(151,55,73);
  beginShape();
    vertex(163.0, 279.0);
    vertex(173.0, 267.0);
    vertex(250.0, 260.0);
    vertex(250.0, 362.0);
    vertex(186.0, 362.0);
    vertex(178.0, 355.0);
    vertex(170.0, 355.0);
    vertex(171.0, 341.0);
    vertex(175.0, 319.0);
    vertex(170.0, 311.0);
  endShape();
  
    fill(151,55,73);
  beginShape();
    vertex_sym(163.0, 279.0);
    vertex_sym(173.0, 267.0);
    vertex_sym(250.0, 260.0);
    vertex_sym(250.0, 362.0);
    vertex_sym(186.0, 362.0);
    vertex_sym(178.0, 355.0);
    vertex_sym(170.0, 355.0);
    vertex_sym(171.0, 341.0);
    vertex_sym(175.0, 319.0);
    vertex_sym(170.0, 311.0);
  endShape();
  
//Armpit_gray

  fill(102,94,103);
  beginShape();
    vertex(150.0, 237.0);
    vertex(146.0, 236.0);
    vertex(145.0, 229.0);
    vertex(142.0, 229.0);
    vertex(149.0, 170.0);
    vertex(153.0, 170.0);
  endShape();

  fill(102,94,103);
  beginShape();
    vertex_sym(150.0, 237.0);
    vertex_sym(146.0, 236.0);
    vertex_sym(145.0, 229.0);
    vertex_sym(142.0, 229.0);
    vertex_sym(149.0, 170.0);
    vertex_sym(153.0, 170.0);
  endShape();
  
//Chest_blue
   
  fill(58,80,155);
  beginShape();
    vertex(250.0, 350.0);
    vertex(250.0, 202.0);
    vertex(220.0, 202.0);
    vertex(193.0, 170.0);
    vertex(167.0, 168.0);
    vertex(153.0, 170.0);
    vertex(150.0, 238.0);
    vertex(152.0, 274.0);
    vertex(163.0, 280.0);
    vertex(168.0, 276.0);
    vertex(175.0, 272.0);
    vertex(217.0, 270.0);
    vertex(218.0, 280.0);
    vertex(219.0, 308.0);
    vertex(221.0, 350.0);
  endShape();
  
  fill(58,80,155);
  beginShape();
    vertex_sym(250.0, 350.0);
    vertex_sym(250.0, 202.0);
    vertex_sym(220.0, 202.0);
    vertex_sym(193.0, 170.0);
    vertex_sym(167.0, 168.0);
    vertex_sym(153.0, 170.0);
    vertex_sym(150.0, 238.0);
    vertex_sym(152.0, 274.0);
    vertex_sym(163.0, 280.0);
    vertex_sym(168.0, 276.0);
    vertex_sym(175.0, 272.0);
    vertex_sym(217.0, 270.0);
    vertex_sym(218.0, 280.0);
    vertex_sym(219.0, 308.0);
    vertex_sym(221.0, 350.0);
  endShape();
  
  
//Belly_Blue

  fill(104,133,196);
  beginShape();
    vertex(250.0, 265.0);
    vertex(250.0, 341.0);
    vertex(233.0, 341.0);
    vertex(234.0, 312.0);
    vertex(235.0, 265.0);
  endShape();

  fill(104,133,196);
  beginShape();
    vertex_sym(250.0, 265.0);
    vertex_sym(250.0, 341.0);
    vertex_sym(233.0, 341.0);
    vertex_sym(234.0, 312.0);
    vertex_sym(235.0, 265.0);
  endShape();
  
//Breast_Yellow
  fill(244,194,78);
  beginShape();
    vertex(167.0, 262.0);
    vertex(213.0, 260.0);
    vertex(214.0, 227.0);
    vertex(168.0, 229.0);
  endShape();
  
  fill(244,194,78);
  beginShape();
    vertex_sym(167.0, 262.0);
    vertex_sym(213.0, 260.0);
    vertex_sym(214.0, 227.0);
    vertex_sym(168.0, 229.0);
  endShape();
  
  line(168, 239, 213, 237);
  line(168, 251, 213, 249);
  line_sym(168, 239, 213, 237);
  line_sym(168, 251, 213, 249);


//ChestDetail_White
  
  fill(255);
  beginShape();
    vertex(170.0, 175.0);
    vertex(170.0, 181.0);
    vertex(160.0, 181.0);
    vertex(160.0, 175.0);
    vertex(170.0, 175.0);
  endShape();
    
  beginShape();
    vertex(161.0, 184.0);
    vertex(170.0, 184.0);
    vertex(173.0, 199.0);
    vertex(163.0, 199.0);
    vertex(161.0, 184.0);
  endShape();
  
  beginShape();
    vertex(199.0, 218.0);
    vertex(180.0, 218.0);
    vertex(177.0, 212.0);
    vertex(168.0, 216.0);
    vertex(176.0, 223.0);
    vertex(199.0, 223.0);
    vertex(199.0, 218.0);
  endShape();
  
  beginShape();
    vertex(250.0, 218.0);
    vertex(239.0, 218.0);
    vertex(239.0, 223.0);
    vertex(250.0, 223.0);
    vertex(250.0, 218.0);
  endShape();
  
  beginShape();
    vertex(250.0, 223.0);
    vertex(239.0, 223.0);
    vertex(239.0, 227.0);
    vertex(250.0, 227.0);
    vertex(250.0, 223.0);
  endShape();
  
  beginShape();
    vertex(180.0, 301.0);
    vertex(180.0, 312.0);
    vertex(188.0, 312.0);
    vertex(188.0, 301.0);
    vertex(180.0, 301.0);
  endShape();
  
  beginShape();
    vertex(190.0, 322.0);
    vertex(182.0, 322.0);
    vertex(180.0, 336.0);
    vertex(182.0, 341.0);
    vertex(182.0, 343.0);
    vertex(197.0, 343.0);
    vertex(197.0, 340.0);
    vertex(188.0, 340.0);
    vertex(188.0, 337.0);
    vertex(190.0, 322.0);
  endShape(); 
  
    fill(255);
  beginShape();
    vertex_sym(170.0, 175.0);
    vertex_sym(170.0, 181.0);
    vertex_sym(160.0, 181.0);
    vertex_sym(160.0, 175.0);
    vertex_sym(170.0, 175.0);
  endShape();
    
  beginShape();
    vertex_sym(161.0, 184.0);
    vertex_sym(170.0, 184.0);
    vertex_sym(173.0, 199.0);
    vertex_sym(163.0, 199.0);
    vertex_sym(161.0, 184.0);
  endShape();
  
  beginShape();
    vertex_sym(199.0, 218.0);
    vertex_sym(180.0, 218.0);
    vertex_sym(177.0, 212.0);
    vertex_sym(168.0, 216.0);
    vertex_sym(176.0, 223.0);
    vertex_sym(199.0, 223.0);
    vertex_sym(199.0, 218.0);
  endShape();
  
  beginShape();
    vertex_sym(250.0, 218.0);
    vertex_sym(239.0, 218.0);
    vertex_sym(239.0, 223.0);
    vertex_sym(250.0, 223.0);
    vertex_sym(250.0, 218.0);
  endShape();
  
  beginShape();
    vertex_sym(250.0, 223.0);
    vertex_sym(239.0, 223.0);
    vertex_sym(239.0, 227.0);
    vertex_sym(250.0, 227.0);
    vertex_sym(250.0, 223.0);
  endShape();
  
  beginShape();
    vertex_sym(180.0, 301.0);
    vertex_sym(180.0, 312.0);
    vertex_sym(188.0, 312.0);
    vertex_sym(188.0, 301.0);
    vertex_sym(180.0, 301.0);
  endShape();
  
  beginShape();
    vertex_sym(190.0, 322.0);
    vertex_sym(182.0, 322.0);
    vertex_sym(180.0, 336.0);
    vertex_sym(182.0, 341.0);
    vertex_sym(182.0, 343.0);
    vertex_sym(197.0, 343.0);
    vertex_sym(197.0, 340.0);
    vertex_sym(188.0, 340.0);
    vertex_sym(188.0, 337.0);
    vertex_sym(190.0, 322.0);
  endShape(); 
  
//Abs_LightBlue
  fill(107,137,195);
  beginShape();
    vertex(250.0, 362.0);
    vertex(250.0, 350.0);
    vertex(226.0, 350.0);
    vertex(221.0, 348.0);
    vertex(221.0, 366.0);
    vertex(226.0, 362.0);
  endShape();
  
    fill(107,137,195);
  beginShape();
    vertex_sym(250.0, 362.0);
    vertex_sym(250.0, 350.0);
    vertex_sym(226.0, 350.0);
    vertex_sym(221.0, 348.0);
    vertex_sym(221.0, 366.0);
    vertex_sym(226.0, 362.0);
  endShape();

//LINES! LINES EVERYWHERE T.T
//Body
//  stroke(156,150,151);
  line(121, 34, 142, 30);
  line(132, 31, 137, 56);
  line(127, 59, 149, 54);
  line(129, 68, 150, 64);
  line(140, 65, 162, 168);
  line(169, 149, 145, 153);
  line(214, 194, 158, 219);
  line(197, 172, 204, 160);
  line(223, 198, 221, 202);
  line(221, 201, 221, 217);
  line(221, 217, 250, 217);
  line(217, 223, 221, 217);
  line(217, 223, 217, 270);
  line(232, 254, 222, 226);
  line(221, 217, 222, 226);
  line(222, 226, 217, 223);
  line(232, 254, 250, 254);
  line(226, 258, 232, 254);
  line(226, 258, 217, 230);
  line(224, 261, 217, 246);
  line(224, 261, 226, 258);
  line(224, 261, 250, 261);
  line(224, 261, 224, 350);
  line(235, 265, 250, 265);
  line(235, 276, 250, 276);
  line(235, 303, 250, 303);
  line(235, 331, 250, 331);
  line(152, 184, 158, 218);
  line(159, 226, 217, 226);
  line(159, 226, 158, 218);
  line(159, 226, 159, 265);
  line(175, 273, 159, 265);
  line(165, 226, 165, 267);
  line(159, 277, 165, 267);
  line(153, 274, 159, 265);
  line(170, 312, 178, 312);
  line(219, 312, 178, 312);
  line(174, 317, 219, 316);
  line(174, 274, 178, 312);
  line(168, 276, 173, 312);
  line(178, 312, 180, 317);
  line(181, 317, 178, 346);
  line(178, 346, 189, 346);
  line(189, 346, 196, 344);
  line(196, 344, 219, 344);
  line(176, 355, 178, 346);
  line(189, 346, 189, 362);
  line(233, 147, 230, 169);
  line(214, 169, 250, 169);
  line(243, 158, 243, 169);
  line(223, 157, 223, 169);
  line(134, 95, 145, 93);
  line(154, 135, 143, 137);
  line(141, 152, 149, 163);
  line(134, 234, 142, 166);
  line(140, 238, 134, 234);
  line(149, 163, 140, 238);
  line(125, 241, 125, 250);
  line(125, 241, 134, 234);
  line(115, 246, 64, 221);
  line(142, 166, 134, 155);
  line(134, 155, 139, 151);
  line(142, 166, 150, 164);
  line(134, 155, 65, 146); 
  line(66, 140, 65, 146);
  line(125, 241, 69, 214);
  line(69, 214, 49, 153);
  line(49, 153, 65, 146);
  line(49, 153, 41, 152);
  line(69, 214, 63, 221);
  line(44, 163, 110, 173);
  line(110, 173, 118, 184);
  line(146, 188, 118, 184);
  
    line_sym(121, 34, 142, 30);
  line_sym(132, 31, 137, 56);
  line_sym(127, 59, 149, 54);
  line_sym(129, 68, 150, 64);
  line_sym(140, 65, 162, 168);
  line_sym(169, 149, 145, 153);
  line_sym(214, 194, 158, 219);
  line_sym(197, 172, 204, 160);
  line_sym(223, 198, 221, 202);
  line_sym(221, 201, 221, 217);
  line_sym(221, 217, 250, 217);
  line_sym(217, 223, 221, 217);
  line_sym(217, 223, 217, 270);
  line_sym(232, 254, 222, 226);
  line_sym(221, 217, 222, 226);
  line_sym(222, 226, 217, 223);
  line_sym(232, 254, 250, 254);
  line_sym(226, 258, 232, 254);
  line_sym(226, 258, 217, 230);
  line_sym(224, 261, 217, 246);
  line_sym(224, 261, 226, 258);
  line_sym(224, 261, 250, 261);
  line_sym(224, 261, 224, 350);
  line_sym(235, 265, 250, 265);
  line_sym(235, 276, 250, 276);
  line_sym(235, 303, 250, 303);
  line_sym(235, 331, 250, 331);
  line_sym(152, 184, 158, 218);
  line_sym(159, 226, 217, 226);
  line_sym(159, 226, 158, 218);
  line_sym(159, 226, 159, 265);
  line_sym(175, 273, 159, 265);
  line_sym(165, 226, 165, 267);
  line_sym(159, 277, 165, 267);
  line_sym(153, 274, 159, 265);
  line_sym(170, 312, 178, 312);
  line_sym(219, 312, 178, 312);
  line_sym(174, 317, 219, 316);
  line_sym(174, 274, 178, 312);
  line_sym(168, 276, 173, 312);
  line_sym(178, 312, 180, 317);
  line_sym(181, 317, 178, 346);
  line_sym(178, 346, 189, 346);
  line_sym(189, 346, 196, 344);
  line_sym(196, 344, 219, 344);
  line_sym(176, 355, 178, 346);
  line_sym(189, 346, 189, 362);
  line_sym(233, 147, 230, 169);
  line_sym(214, 169, 250, 169);
  line_sym(243, 158, 243, 169);
  line_sym(223, 157, 223, 169);
  line_sym(134, 95, 145, 93);
  line_sym(154, 135, 143, 137);
  line_sym(141, 152, 149, 163);
  line_sym(134, 234, 142, 166);
  line_sym(140, 238, 134, 234);
  line_sym(149, 163, 140, 238);
  line_sym(125, 241, 125, 250);
  line_sym(125, 241, 134, 234);
  line_sym(115, 246, 64, 221);
  line_sym(142, 166, 134, 155);
  line_sym(134, 155, 139, 151);
  line_sym(142, 166, 150, 164);
  line_sym(134, 155, 65, 146); 
  line_sym(66, 140, 65, 146);
  line_sym(125, 241, 69, 214);
  line_sym(69, 214, 49, 153);
  line_sym(49, 153, 65, 146);
  line_sym(49, 153, 41, 152);
  line_sym(69, 214, 63, 221);
  line_sym(44, 163, 110, 173);
  line_sym(110, 173, 118, 184);
  line_sym(146, 188, 118, 184);
  
//SignArm

  fill(95, 89, 96);
  beginShape();
    vertex(74.0, 182.0);
    vertex(132.0, 190.0);
    vertex(131.0, 201.0);
    vertex(74.0, 193.0);
    vertex(74.0, 182.0);
  endShape();
  
  fill(95, 89, 96);
  beginShape();
    vertex_sym(74.0, 182.0);
    vertex_sym(132.0, 190.0);
    vertex_sym(131.0, 201.0);
    vertex_sym(74.0, 193.0);
    vertex_sym(74.0, 193.0);
  endShape();
  
//Arm_green
  
  fill(94,153,107);
  beginShape();
    vertex(65.0, 144.0);
    vertex(49.0, 151.0);
    vertex(58.0, 156.0);
    vertex(67.0, 151.0);
    vertex(65.0, 144.0);
  endShape();
  
  fill(94,153,107);
  beginShape();
    vertex_sym(65.0, 144.0);
    vertex_sym(49.0, 151.0);
    vertex_sym(58.0, 156.0);
    vertex_sym(67.0, 151.0);
    vertex_sym(65.0, 144.0);
  endShape();
  
//Arm_greys

  fill(77,65,77);
  beginShape();
    vertex(134.0, 205.0);
    vertex(131.0, 218.0);
    vertex(136.0, 219.0);
    vertex(137.0, 206.0);
    vertex(134.0, 205.0);
  endShape();
  
  beginShape();
      vertex(131.0, 220.0);
      vertex(135.0, 220.0);
      vertex(133.0, 235.0);
      vertex(125.0, 240.0);
      vertex(117.0, 237.0);
      vertex(118.0, 227.0);
      vertex(127.0, 231.0);
      vertex(130.0, 228.0);
      vertex(131.0, 220.0);
   endShape();
   
   beginShape();
       vertex(97.0, 216.0);
       vertex(96.0, 225.0);
       vertex(79.0, 219.0);
       vertex(79.0, 208.0);
       vertex(97.0, 216.0);
    endShape();
    
    beginShape();
        vertex(65.0, 190.0);
        vertex(69.0, 190.0);
        vertex(71.0, 200.0);
        vertex(68.0, 200.0);
        vertex(65.0, 190.0);
     endShape();
     
  fill(77,65,77);
  beginShape();
    vertex_sym(134.0, 205.0);
    vertex_sym(131.0, 218.0);
    vertex_sym(136.0, 219.0);
    vertex_sym(137.0, 206.0);
    vertex_sym(134.0, 205.0);
  endShape();
  
  beginShape();
      vertex_sym(131.0, 220.0);
      vertex_sym(135.0, 220.0);
      vertex_sym(133.0, 235.0);
      vertex_sym(125.0, 240.0);
      vertex_sym(117.0, 237.0);
      vertex_sym(118.0, 227.0);
      vertex_sym(127.0, 231.0);
      vertex_sym(130.0, 228.0);
      vertex_sym(131.0, 220.0);
   endShape();
   
   beginShape();
       vertex_sym(97.0, 216.0);
       vertex_sym(96.0, 225.0);
       vertex_sym(79.0, 219.0);
       vertex_sym(79.0, 208.0);
       vertex_sym(97.0, 216.0);
    endShape();
    
    beginShape();
        vertex_sym(65.0, 190.0);
        vertex_sym(69.0, 190.0);
        vertex_sym(71.0, 200.0);
        vertex_sym(68.0, 200.0);
        vertex_sym(65.0, 190.0);
     endShape();
     
//Crotch_Red
  
  fill(151,55,73);
  beginShape();
    vertex(250.0, 369.0);
    vertex(250.0, 422.0);
    vertex(230.0, 422.0);
    vertex(230.0, 369.0);
  endShape();

  fill(151,55,73);
  beginShape();
    vertex_sym(250.0, 369.0);
    vertex_sym(250.0, 422.0);
    vertex_sym(230.0, 422.0);
    vertex_sym(230.0, 369.0);
  endShape(); 

//Crotch_Yellow

  fill(244,194,78);
  beginShape();
      vertex(250.0, 391.0);
      vertex(234.0, 383.0);
      vertex(250.0, 400.0);
   endShape();
  
  fill(244,194,78);
  beginShape();
      vertex_sym(250.0, 391.0);
      vertex_sym(234.0, 383.0);
      vertex_sym(250.0, 400.0);
   endShape();
   
   line(234.0, 383.0, 250.0, 395.0);
   line_sym(234.0, 383.0, 250.0, 395.0);
   
//Thigh_Yellow
  
  fill(244,194,78);
  beginShape();
       vertex(210.0, 376.0);
       vertex(214.0, 380.0);
       vertex(214.0, 427.0);
       vertex(208.0, 432.0);
       vertex(169.0, 432.0);
       vertex(157.0, 420.0);
       vertex(157.0, 380.0);
       vertex(164.0, 376.0);
    endShape();
    
  fill(244,194,78);
  beginShape();
       vertex_sym(210.0, 376.0);
       vertex_sym(214.0, 380.0);
       vertex_sym(214.0, 427.0);
       vertex_sym(208.0, 432.0);
       vertex_sym(169.0, 432.0);
       vertex_sym(157.0, 420.0);
       vertex_sym(157.0, 380.0);
       vertex_sym(164.0, 376.0);
    endShape();
    
    line(214, 419, 207, 426);
    line(207, 426, 207, 432);
    line(207, 426, 171, 426);
    line(171, 426, 169, 432);
    line(171, 426, 162, 415);
    line(162, 415, 157, 420);
    line(162, 415, 162, 381);
    line(162, 381, 157, 380);
    line(169, 376, 162, 381);
    line(210, 376, 164, 376);
    
    line_sym(214, 419, 207, 426);
    line_sym(207, 426, 207, 432);
    line_sym(207, 426, 171, 426);
    line_sym(171, 426, 169, 432);
    line_sym(171, 426, 162, 415);
    line_sym(162, 415, 157, 420);
    line_sym(162, 415, 162, 381);
    line_sym(162, 381, 157, 380);
    line_sym(169, 376, 162, 381);
    line_sym(210, 376, 164, 376);
    
//Crotch_Brown

    fill(199,182,177);
    beginShape();
        vertex(222, 466);
        vertex(222, 493);
        vertex(228, 503);
        vertex(236, 503);
        vertex(236, 469);
        vertex(229, 469);
        vertex(222, 466);
     endShape();
     
    fill(199,182,177);
    beginShape();
        vertex_sym(222, 466);
        vertex_sym(222, 493);
        vertex_sym(228, 503);
        vertex_sym(236, 503);
        vertex_sym(236, 469);
        vertex_sym(229, 469);
        vertex_sym(222, 466);
     endShape();
        
        
    
//Crotch_Lines
    
   line(250, 369, 230, 369);
   line(250, 362, 226, 362);
   line(223, 365, 226, 362);
   line(221, 367, 230, 369);
   line(230, 369, 230, 362);
   line(221, 367, 222, 482);
   line(230, 422, 229, 503);
   line(222, 409, 229, 404);
   line(230, 422, 222, 423);
   line(250, 425, 230, 425);
   line(250, 435, 230, 435);
   line(230, 425, 222, 423);
   line(230, 435, 222, 432);
   line(250, 474, 230, 474);
   line(250, 484, 230, 484);
   line(250, 493, 230, 493);
   line(236, 469, 236, 435);
   
   line_sym(250, 369, 230, 369);
   line_sym(250, 362, 226, 362);
   line_sym(223, 365, 226, 362);
   line_sym(221, 367, 230, 369);
   line_sym(230, 369, 230, 362);
   line_sym(221, 367, 222, 482);
   line_sym(230, 422, 229, 503);
   line_sym(222, 409, 229, 404);
   line_sym(230, 422, 222, 423);
   line_sym(250, 425, 230, 425);
   line_sym(250, 435, 230, 435);
   line_sym(230, 425, 222, 423);
   line_sym(230, 435, 222, 432);
   line_sym(250, 474, 230, 474);
   line_sym(250, 484, 230, 484);
   line_sym(250, 493, 230, 493);
   line_sym(236, 469, 236, 435);
   
//Arms

    fill(202, 185, 173);
    beginShape();
      vertex(68, 278);
      vertex(99, 283);
      vertex(101, 268);
      vertex(70, 263);
      vertex(68, 278);
    endShape();
    
    fill(105, 99, 106);
    beginShape();
      vertex(99, 283);
      vertex(68, 278);
      vertex(55, 348);
      vertex(86, 353);
      vertex(99, 283);
    endShape();
    
    fill(161, 154, 152);
    beginShape();
        vertex(62, 311);
        vertex(72, 314);
        vertex(76, 290);
        vertex(66, 289);
        vertex(62, 311);
      endShape();
      
    beginShape();
        vertex(82, 315);
        vertex(92, 317);
        vertex(98, 292);
        vertex(86, 290);
        vertex(82, 315);
      endShape();
      
    beginShape();
        vertex(60, 322);
        vertex(70, 325);
        vertex(67, 350);
        vertex(55, 348);
        vertex(60, 322);
      endShape();
      
    beginShape();
        vertex(81, 326);
        vertex(91, 329);
        vertex(86, 354);
        vertex(77, 352);
        vertex(81, 326);
      endShape();
    
   //white 
     
    fill(255);
    beginShape();
        vertex(72, 314);
        vertex(82, 315);
        vertex(83, 308);
        vertex(74, 307);
        vertex(72, 314);
      endShape();
      
    beginShape();
        vertex(70, 325);
        vertex(81, 326);
        vertex(78, 334);
        vertex(69, 333);
        vertex(70, 325);
      endShape();
   
 //Brown     
    fill(166,113,104);
    beginShape();
        vertex(70, 279);
        vertex(75, 280);
        vertex(71, 307);
        vertex(66, 306);
        vertex(70, 279);
      endShape();
      
    beginShape();
        vertex(90, 282);
        vertex(95, 283);
        vertex(91, 311);
        vertex(85, 309);
        vertex(90, 282);
      endShape();
      
    beginShape();
        vertex(88, 335);
        vertex(85, 353);
        vertex(78, 351);
        vertex(82, 333);
        vertex(88, 335);
      endShape();
      
    beginShape();
        vertex(61, 331);
        vertex(66, 332);
        vertex(65, 350);
        vertex(58, 349);
        vertex(61, 331);
      endShape();
      
//Wrist
    fill(202, 185, 173);
    beginShape();
        vertex(36, 407);
        vertex(86, 416);
        vertex(79, 454);
        vertex(29, 446);
        vertex(36, 407);
      endShape();
      
     fill(205, 34, 111);
     beginShape();
         vertex(34, 419);
         vertex(84, 429);
         vertex(83, 434);
         vertex(33, 424);
         vertex(34, 419);
       endShape();
       
     fill(217, 117, 86);
     beginShape();
         vertex(46, 444);
         vertex(64, 447);
         vertex(65, 443);
         vertex(46, 440);
         vertex(46, 444);
       endShape();
      
//Arms

    fill(202, 185, 173);
    beginShape();
      vertex_sym(68, 278);
      vertex_sym(99, 283);
      vertex_sym(101, 268);
      vertex_sym(70, 263);
      vertex_sym(68, 278);
    endShape();
    
    fill(105, 99, 106);
    beginShape();
      vertex_sym(99, 283);
      vertex_sym(68, 278);
      vertex_sym(55, 348);
      vertex_sym(86, 353);
      vertex_sym(99, 283);
    endShape();
    
    fill(161, 154, 152);
    beginShape();
        vertex_sym(62, 311);
        vertex_sym(72, 314);
        vertex_sym(76, 290);
        vertex_sym(66, 289);
        vertex_sym(62, 311);
      endShape();
      
    beginShape();
        vertex_sym(82, 315);
        vertex_sym(92, 317);
        vertex_sym(98, 292);
        vertex_sym(86, 290);
        vertex_sym(82, 315);
      endShape();
      
    beginShape();
        vertex_sym(60, 322);
        vertex_sym(70, 325);
        vertex_sym(67, 350);
        vertex_sym(55, 348);
        vertex_sym(60, 322);
      endShape();
      
    beginShape();
        vertex_sym(81, 326);
        vertex_sym(91, 329);
        vertex_sym(86, 354);
        vertex_sym(77, 352);
        vertex_sym(81, 326);
      endShape();
    
   //white 
     
    fill(255);
    beginShape();
        vertex_sym(72, 314);
        vertex_sym(82, 315);
        vertex_sym(83, 308);
        vertex_sym(74, 307);
        vertex_sym(72, 314);
      endShape();
      
    beginShape();
        vertex_sym(70, 325);
        vertex_sym(81, 326);
        vertex_sym(78, 334);
        vertex_sym(69, 333);
        vertex_sym(70, 325);
      endShape();
   
 //Brown     
    fill(166,113,104);
    beginShape();
        vertex_sym(70, 279);
        vertex_sym(75, 280);
        vertex_sym(71, 307);
        vertex_sym(66, 306);
        vertex_sym(70, 279);
      endShape();
      
    beginShape();
        vertex_sym(90, 282);
        vertex_sym(95, 283);
        vertex_sym(91, 311);
        vertex_sym(85, 309);
        vertex_sym(90, 282);
      endShape();
      
    beginShape();
        vertex_sym(88, 335);
        vertex_sym(85, 353);
        vertex_sym(78, 351);
        vertex_sym(82, 333);
        vertex_sym(88, 335);
      endShape();
      
    beginShape();
        vertex_sym(61, 331);
        vertex_sym(66, 332);
        vertex_sym(65, 350);
        vertex_sym(58, 349);
        vertex_sym(61, 331);
      endShape();
      
//Wrist
    fill(202, 185, 173);
    beginShape();
        vertex_sym(36, 407);
        vertex_sym(86, 416);
        vertex_sym(79, 454);
        vertex_sym(29, 446);
        vertex_sym(36, 407);
      endShape();
      
     fill(205, 34, 111);
     beginShape();
         vertex_sym(34, 419);
         vertex_sym(84, 429);
         vertex_sym(83, 434);
         vertex_sym(33, 424);
         vertex_sym(34, 419);
       endShape();
       
     fill(217, 117, 86);
     beginShape();
         vertex_sym(46, 444);
         vertex_sym(64, 447);
         vertex_sym(65, 443);
         vertex_sym(46, 440);
         vertex_sym(46, 444);
       endShape();
 
     fill(44, 43, 45);
     beginShape();
         vertex(86, 451);
         vertex(90, 436);
         vertex(97, 450);
      endShape();
   
     fill(44, 43, 45);
     beginShape();
         vertex_sym(86, 451);
         vertex_sym(90, 436);
         vertex_sym(97, 450);
      endShape();      
      
//Arm_Lines
    
    line(66, 231, 96, 236);
    line(69, 232, 63, 279);
    line(63, 279, 61, 304);
    line(58, 277, 67, 280);
    line(57, 298, 59, 303);
    line(59, 303, 62, 304);
    line(71, 263, 76, 233);
    line(101, 269, 106, 241);
    line(110, 242, 103, 285);
    line(97, 310, 103, 285);
    line(99, 285, 107, 285);
    line(97, 310, 103, 304); 
    line(97, 310, 94, 309); 
    line(47, 325, 50, 321);
    line(50, 321, 59, 323);
    line(55, 309, 62, 311);
    line(93, 317, 100, 318);
    line(50, 321, 53, 325);
    line(53, 325, 58, 326);
    line(53, 325, 47, 350);
    line(47, 350, 86, 358);
    line(86, 358, 86, 355); 
    line(86, 358, 85, 368);
    line(86, 358, 96, 360);
    line(96, 360, 97, 342);
    line(97, 342, 97, 332);
    line(92, 330, 104, 334);
    line(96, 360, 102, 362);
    line(96, 360, 86, 416);
    line(47, 350, 40, 350);
    line(47, 350, 36, 406); 
    line(53, 352, 51, 362);
    line(85, 368, 51, 362);
    line(51, 362, 46, 361);
    line(85, 368, 94, 370);
    line(25, 433, 30, 434); 
    line(82, 443, 87, 444);
    line(30, 442, 80, 450);
    line(55, 446, 54, 450);
    line(37, 448, 39, 456);
    line(71, 453, 69, 460);
    
    line_sym(66, 231, 96, 236);
    line_sym(69, 232, 63, 279);
    line_sym(63, 279, 61, 304);
    line_sym(58, 277, 67, 280);
    line_sym(57, 298, 59, 303);
    line_sym(59, 303, 62, 304);
    line_sym(71, 263, 76, 233);
    line_sym(101, 269, 106, 241);
    line_sym(110, 242, 103, 285);
    line_sym(97, 310, 103, 285);
    line_sym(99, 285, 107, 285);
    line_sym(97, 310, 103, 304); 
    line_sym(97, 310, 94, 309); 
    line_sym(47, 325, 50, 321);
    line_sym(50, 321, 59, 323);
    line_sym(55, 309, 62, 311);
    line_sym(93, 317, 100, 318);
    line_sym(50, 321, 53, 325);
    line_sym(53, 325, 58, 326);
    line_sym(53, 325, 47, 350);
    line_sym(47, 350, 86, 358);
    line_sym(86, 358, 86, 355); 
    line_sym(86, 358, 85, 368);
    line_sym(86, 358, 96, 360);
    line_sym(96, 360, 97, 342);
    line_sym(97, 342, 97, 332);
    line_sym(92, 330, 104, 334);
    line_sym(96, 360, 102, 362);
    line_sym(96, 360, 86, 416);
    line_sym(47, 350, 40, 350);
    line_sym(47, 350, 36, 406); 
    line_sym(53, 352, 51, 362);
    line_sym(85, 368, 51, 362);
    line_sym(51, 362, 46, 361);
    line_sym(85, 368, 94, 370);
    line_sym(25, 433, 30, 434); 
    line_sym(82, 443, 87, 444);
    line_sym(30, 442, 80, 450);
    line_sym(55, 446, 54, 450);
    line_sym(37, 448, 39, 456);
    line_sym(71, 453, 69, 460);

//Leg

    fill(200,180,166);
    beginShape();
        vertex(221, 425);
        vertex(217, 425);
        vertex(217, 420);
        vertex(214, 420);
        vertex(214, 388);
        vertex(217, 388);
        vertex(217, 383);
        vertex(221, 383);
        vertex(221, 425);
      endShape();
      
      fill(85, 74, 86);
      beginShape();
          vertex(220, 430);
          vertex(220, 453);
          vertex(214, 457);
          vertex(190, 450);
          vertex(190, 444);
          vertex(210, 448);
          vertex(216, 444);
          vertex(216, 430);
          vertex(220, 430);
        endShape();
        
      fill(85, 74, 86);
      beginShape();
          vertex(167, 437);
          vertex(167, 445);
          vertex(146, 440);
          vertex(141, 431);
          vertex(142, 430);
          vertex(150, 430);
          vertex(149, 430);
          vertex(152, 434);
          vertex(167, 437);
        endShape();
        
      fill(85,74,86);
      beginShape();
          vertex(152, 395);
          vertex(148, 412);
          vertex(152, 413);
          vertex(152, 416);
          vertex(144, 416);
          vertex(148, 395);
          vertex(152, 395);
        endShape();
        
      fill(200,180,166);
      beginShape();
          vertex(150, 548);
          vertex(185, 553);
          vertex(182, 573);
          vertex(147, 567);
          vertex(150, 548);
        endShape();
        
      fill(160,152,149);
      beginShape();
          vertex(182, 573);
          vertex(147, 567);
          vertex(145, 580);
          vertex(163, 582);
          vertex(181, 584);
          vertex(182, 573);
        endShape();
        
      fill(200,180,166);
      beginShape();
          vertex(102, 704);
          vertex(116, 709);
          vertex(116, 740);
          vertex(113, 750);
          vertex(108, 765);
          vertex(104, 780);
          vertex(102, 770);
          vertex(101, 765);
          vertex(102, 741);
          vertex(102, 704);
        endShape();
        
      beginShape();
          vertex(174, 716);
          vertex(166, 743);
          vertex(165, 789);
          vertex(176, 766);
          vertex(177, 753);
          vertex(179, 744);
          vertex(187, 716);
          vertex(174, 716);
        endShape();
        
      fill(255);
      beginShape();
          vertex(129, 693);
          vertex(129, 724);
          vertex(131, 727);
          vertex(153, 731);
          vertex(157, 729);
          vertex(165, 700);
          vertex(162, 694);
          vertex(132, 690);
          vertex(129, 693);
        endShape();
        
      beginShape();
          vertex(129, 769);
          vertex(145, 771);
          vertex(146, 776);
          vertex(142, 782);
          vertex(129, 781);
          vertex(128, 775);
          vertex(129, 769);
        endShape();
        
      fill(66,57,66);
      beginShape();
          vertex(145, 772);
          vertex(149, 772);
          vertex(161, 748);
          vertex(161, 745);
          vertex(157, 739);
          vertex(145, 767);
          vertex(145, 772);
        endShape();
        
      fill(66,57,66);
      beginShape();
          vertex(129, 769);
          vertex(127, 733);
          vertex(119, 741);
          vertex(121, 753);
          vertex(125, 769);
          vertex(129, 769);
        endShape();
        
      fill(66,57,66);
      beginShape();
          vertex(97, 868);
          vertex(99, 864);
          vertex(118, 864);
          vertex(146, 870);
          vertex(147, 874);
          vertex(146, 927);
          vertex(150, 939);
          vertex(150, 949);
          vertex(139, 932);
          vertex(127, 918);
          vertex(104, 906);
          vertex(107, 915);
          vertex(105, 923);
          vertex(112, 923);
          vertex(110, 926);
          vertex(89, 926);
          vertex(82, 926);
          vertex(82, 923);
          vertex(90, 923);
          vertex(93, 914);
          vertex(78, 915);
          vertex(68, 931);
          vertex(68, 920);
          vertex(70, 914);
          vertex(97, 868);
        endShape();
        
      fill(255);
      beginShape();
          vertex(78, 869);
          vertex(111, 869);
          vertex(147, 875);
          vertex(147, 914);
          vertex(145, 917);
          vertex(138, 917);
          vertex(135, 916);
          vertex(70, 913);
          vertex(64, 913);
          vertex(62, 908);
          vertex(68, 871);
          vertex(72, 868);
          vertex(78, 869);
        endShape();
        
        fill(66,57,66);
        beginShape();
            vertex(135, 886);
            vertex(127, 886);
            vertex(127, 876);
            vertex(87, 873);
            vertex(87, 883);
            vertex(80, 883);
            vertex(80, 875);
            vertex(84, 871);
            vertex(131, 874);
            vertex(135, 877);
            vertex(135, 886);
          endShape();
          
        fill(66,57,66);
        beginShape();
            vertex(133, 898);
            vertex(133, 903);
            vertex(130, 908);
            vertex(89, 907);
            vertex(89, 904);
            vertex(126, 906);
            vertex(127, 898);
            vertex(133, 898);
          endShape();
          
        beginShape();
            vertex(133, 897);
            vertex(133, 893);
            vertex(126, 893);
            vertex(126, 897);
            vertex(133, 897);
          endShape();
          
        beginShape();
            vertex(80, 894);
            vertex(80, 891);
            vertex(86, 891);
            vertex(86, 894);
            vertex(80, 894);
          endShape();
          
        fill(255);
        beginShape();
            vertex(170, 590);
            vertex(177, 590);
            vertex(179, 592);
            vertex(179, 597);
            vertex(177, 602);
            vertex(170, 602);
            vertex(169, 597);
            vertex(169, 595);
            vertex(168, 592);
            vertex(170, 590);
          endShape();
          
         beginShape();
             vertex(155, 589);
             vertex(155, 593);
             vertex(151, 600);
             vertex(144, 599);
             vertex(144, 588);
             vertex(147, 586);
             vertex(153, 587);
             vertex(155, 589);
           endShape();
           
         beginShape();
             vertex(154, 936);
             vertex(139, 917);
             vertex(146, 916);
             vertex(166, 936);
           endShape();
           
         fill(162, 56, 81);
         beginShape();
             vertex(40, 993);
             vertex(173, 993);
             vertex(164, 952);
             vertex(157, 952);
             vertex(162, 962);
             vertex(163, 973);
             vertex(147, 968);
             vertex(133, 956);
             vertex(118, 944);
             vertex(115, 942);
             vertex(62, 942);
             vertex(51, 959);
             vertex(40, 993);
           endShape();
           
               fill(200,180,166);
    beginShape();
        vertex_sym(221, 425);
        vertex_sym(217, 425);
        vertex_sym(217, 420);
        vertex_sym(214, 420);
        vertex_sym(214, 388);
        vertex_sym(217, 388);
        vertex_sym(217, 383);
        vertex_sym(221, 383);
        vertex_sym(221, 425);
      endShape();
      
      fill(85, 74, 86);
      beginShape();
          vertex_sym(220, 430);
          vertex_sym(220, 453);
          vertex_sym(214, 457);
          vertex_sym(190, 450);
          vertex_sym(190, 444);
          vertex_sym(210, 448);
          vertex_sym(216, 444);
          vertex_sym(216, 430);
          vertex_sym(220, 430);
        endShape();
        
      fill(85, 74, 86);
      beginShape();
          vertex_sym(167, 437);
          vertex_sym(167, 445);
          vertex_sym(146, 440);
          vertex_sym(141, 431);
          vertex_sym(142, 430);
          vertex_sym(150, 430);
          vertex_sym(149, 430);
          vertex_sym(152, 434);
          vertex_sym(167, 437);
        endShape();
        
      fill(85,74,86);
      beginShape();
          vertex_sym(152, 395);
          vertex_sym(148, 412);
          vertex_sym(152, 413);
          vertex_sym(152, 416);
          vertex_sym(144, 416);
          vertex_sym(148, 395);
          vertex_sym(152, 395);
        endShape();
        
      fill(200,180,166);
      beginShape();
          vertex_sym(150, 548);
          vertex_sym(185, 553);
          vertex_sym(182, 573);
          vertex_sym(147, 567);
          vertex_sym(150, 548);
        endShape();
        
      fill(160,152,149);
      beginShape();
          vertex_sym(182, 573);
          vertex_sym(147, 567);
          vertex_sym(145, 580);
          vertex_sym(163, 582);
          vertex_sym(181, 584);
          vertex_sym(182, 573);
        endShape();
        
      fill(200,180,166);
      beginShape();
          vertex_sym(102, 704);
          vertex_sym(116, 709);
          vertex_sym(116, 740);
          vertex_sym(113, 750);
          vertex_sym(108, 765);
          vertex_sym(104, 780);
          vertex_sym(102, 770);
          vertex_sym(101, 765);
          vertex_sym(102, 741);
          vertex_sym(102, 704);
        endShape();
        
      beginShape();
          vertex_sym(174, 716);
          vertex_sym(166, 743);
          vertex_sym(165, 789);
          vertex_sym(176, 766);
          vertex_sym(177, 753);
          vertex_sym(179, 744);
          vertex_sym(187, 716);
          vertex_sym(174, 716);
        endShape();
        
      fill(255);
      beginShape();
          vertex_sym(129, 693);
          vertex_sym(129, 724);
          vertex_sym(131, 727);
          vertex_sym(153, 731);
          vertex_sym(157, 729);
          vertex_sym(165, 700);
          vertex_sym(162, 694);
          vertex_sym(132, 690);
          vertex_sym(129, 693);
        endShape();
        
      beginShape();
          vertex_sym(129, 769);
          vertex_sym(145, 771);
          vertex_sym(146, 776);
          vertex_sym(142, 782);
          vertex_sym(129, 781);
          vertex_sym(128, 775);
          vertex_sym(129, 769);
        endShape();
        
      fill(66,57,66);
      beginShape();
          vertex_sym(145, 772);
          vertex_sym(149, 772);
          vertex_sym(161, 748);
          vertex_sym(161, 745);
          vertex_sym(157, 739);
          vertex_sym(145, 767);
          vertex_sym(145, 772);
        endShape();
        
      fill(66,57,66);
      beginShape();
          vertex_sym(129, 769);
          vertex_sym(127, 733);
          vertex_sym(119, 741);
          vertex_sym(121, 753);
          vertex_sym(125, 769);
          vertex_sym(129, 769);
        endShape();
        
      fill(66,57,66);
      beginShape();
          vertex_sym(97, 868);
          vertex_sym(99, 864);
          vertex_sym(118, 864);
          vertex_sym(146, 870);
          vertex_sym(147, 874);
          vertex_sym(146, 927);
          vertex_sym(150, 939);
          vertex_sym(150, 949);
          vertex_sym(139, 932);
          vertex_sym(127, 918);
          vertex_sym(104, 906);
          vertex_sym(107, 915);
          vertex_sym(105, 923);
          vertex_sym(112, 923);
          vertex_sym(110, 926);
          vertex_sym(89, 926);
          vertex_sym(82, 926);
          vertex_sym(82, 923);
          vertex_sym(90, 923);
          vertex_sym(93, 914);
          vertex_sym(78, 915);
          vertex_sym(68, 931);
          vertex_sym(68, 920);
          vertex_sym(70, 914);
          vertex_sym(97, 868);
        endShape();
        
      fill(255);
      beginShape();
          vertex_sym(78, 869);
          vertex_sym(111, 869);
          vertex_sym(147, 875);
          vertex_sym(147, 914);
          vertex_sym(145, 917);
          vertex_sym(138, 917);
          vertex_sym(135, 916);
          vertex_sym(70, 913);
          vertex_sym(64, 913);
          vertex_sym(62, 908);
          vertex_sym(68, 871);
          vertex_sym(72, 868);
          vertex_sym(78, 869);
        endShape();
        
        fill(66,57,66);
        beginShape();
            vertex_sym(135, 886);
            vertex_sym(127, 886);
            vertex_sym(127, 876);
            vertex_sym(87, 873);
            vertex_sym(87, 883);
            vertex_sym(80, 883);
            vertex_sym(80, 875);
            vertex_sym(84, 871);
            vertex_sym(131, 874);
            vertex_sym(135, 877);
            vertex_sym(135, 886);
          endShape();
          
        fill(66,57,66);
        beginShape();
            vertex_sym(133, 898);
            vertex_sym(133, 903);
            vertex_sym(130, 908);
            vertex_sym(89, 907);
            vertex_sym(89, 904);
            vertex_sym(126, 906);
            vertex_sym(127, 898);
            vertex_sym(133, 898);
          endShape();
          
        beginShape();
            vertex_sym(133, 897);
            vertex_sym(133, 893);
            vertex_sym(126, 893);
            vertex_sym(126, 897);
            vertex_sym(133, 897);
          endShape();
          
        beginShape();
            vertex_sym(80, 894);
            vertex_sym(80, 891);
            vertex_sym(86, 891);
            vertex_sym(86, 894);
            vertex_sym(80, 894);
          endShape();
          
        fill(255);
        beginShape();
            vertex_sym(170, 590);
            vertex_sym(177, 590);
            vertex_sym(179, 592);
            vertex_sym(179, 597);
            vertex_sym(177, 602);
            vertex_sym(170, 602);
            vertex_sym(169, 597);
            vertex_sym(169, 595);
            vertex_sym(168, 592);
            vertex_sym(170, 590);
          endShape();
          
         beginShape();
             vertex_sym(155, 589);
             vertex_sym(155, 593);
             vertex_sym(151, 600);
             vertex_sym(144, 599);
             vertex_sym(144, 588);
             vertex_sym(147, 586);
             vertex_sym(153, 587);
             vertex_sym(155, 589);
           endShape();
           
         fill(162, 56, 81);
         beginShape();
             vertex_sym(40, 993);
             vertex_sym(173, 993);
             vertex_sym(164, 952);
             vertex_sym(157, 952);
             vertex_sym(162, 962);
             vertex_sym(163, 973);
             vertex_sym(147, 968);
             vertex_sym(133, 956);
             vertex_sym(118, 944);
             vertex_sym(115, 942);
             vertex_sym(62, 942);
             vertex_sym(51, 959);
             vertex_sym(40, 993);
           endShape();
           
          fill(255);
          beginShape();
             vertex_sym(154, 936);
             vertex_sym(139, 917);
             vertex_sym(146, 916);
             vertex_sym(166, 936);
           endShape();
           
//Leg_line

    line(220, 366, 188, 366);
    line(188, 366, 186, 363);
    line(182, 359, 160, 359);
    line(160, 359, 155, 355);
    line(179, 356, 155, 355);
    line(160, 359, 153, 364);
    line(153, 364, 144, 364);
    line(153, 364, 138, 434);
    line(155, 355, 151, 355);
    line(145, 358, 151, 355);
    line(145, 358, 130, 408);
    line(144, 364, 130, 438);
    line(130, 438, 138, 434);
    line(130, 438, 136, 450);   
    line(138, 434, 142, 441);
    line(142, 441, 136, 450);
    line(142, 441, 216, 460);
    line(136, 450, 214, 473);
    line(216, 460, 214, 473);
    line(214, 473, 222, 473);
    line(216, 460, 222, 456);
    line(140, 380, 158, 380);
    line(133, 418, 158, 418);
    line(162, 459, 150, 547);
    line(197, 468, 185, 554);
    line(159, 485, 165, 492);
    line(187, 495, 165, 492);
    line(187, 495, 194, 489);
    line(151, 456, 143, 524);
    line(143, 524, 141, 552);
    line(141, 552, 140, 580);
    line(140, 580, 145, 580);
    line(144, 454, 136, 517);
    line(136, 517, 133, 556);
    line(133, 556, 134, 593);
    line(134, 593, 140, 580);
    line(134, 593, 133, 597);
    line(139, 467, 160, 470); 
    line(196, 474, 218, 478);
    line(218, 483, 219, 474);
    line(209, 472, 200, 528);
    line(193, 559, 200, 528);
    line(193, 559, 186, 586);
    line(182, 584, 186, 586);
    line(186, 586, 188, 597);
    line(146, 574, 182, 579);
    line(143, 588, 180, 593);
    line(143, 588, 140, 580);
    line(180, 593, 186, 586);
    line(119, 634, 133, 597);
    line(119, 634, 113, 630);
    line(119, 634, 118, 640);
    line(118, 640, 107, 649);
    line(118, 640, 116, 708);
    line(107, 648, 101, 676);
    line(101, 676, 99, 699);
    line(99, 699, 102, 704);
    line(119, 661, 180, 670);
    line(180, 670, 182, 642);
    line(182, 642, 186, 587);
    line(175, 669, 180, 593);
    line(190, 651, 188, 605);
    line(190, 651, 198, 660);
    line(190, 643, 198, 641);
    line(144, 588, 127, 662);
    line(123, 661, 141, 583);
    line(186, 668, 180, 670);
    line(186, 668, 190, 652);
    line(186, 668, 175, 717);
    line(192, 712, 188, 716);
    line(197, 692, 192, 712);
    line(198, 660, 197, 692);
    line(175, 669, 160, 739);
    line(180, 670, 164, 739);
    line(164, 739, 158, 739);
    line(164, 739, 166, 742);
    line(166, 742, 162, 745);
    line(123, 662, 120, 732);
    line(127, 662, 124, 732);
    line(120, 732, 124, 732);
    line(127, 733, 124, 732);
    line(116, 734, 120, 732);
    line(116, 741, 119, 742);
    line(110, 838, 95, 838);
    line(110, 838, 114, 842);
    line(114, 842, 140, 846); 
    line(114, 842, 104, 859);
    line(140, 846, 143, 844);
    line(143, 844, 160, 845);
    line(104, 859, 107, 865);
    line(144, 866, 141, 847);
    line(144, 866, 139, 868);
    line(161, 749, 152, 813);
    line(149, 844, 152, 813);
    line(166, 750, 156, 813);
    line(156, 813, 153, 845);
    line(153, 845, 150, 870);
    line(149, 844, 147, 867);
    line(147, 867, 144, 866);
    line(147, 867, 150, 870);
    line(150, 870, 156, 886);
    line(168, 905, 156, 886);
    line(156, 886, 147, 876);
    line(168, 905, 169, 932);
    line(137, 872, 137, 911);
    line(137, 911, 147, 913);
    line(147, 913, 169, 932);
    line(169, 932, 165, 936);
    line(165, 936, 154, 936);
    line(157, 952, 151, 950);
    line(157, 945, 151, 945);
    line(155, 937, 155, 945);
    line(172, 943, 172, 896);
    line(166, 900, 172, 900);
    line(172, 900, 175, 903);
    line(78, 869, 72, 908);
    line(137, 911, 72, 908);
    line(72, 908, 62, 909);
    line(87, 914, 82, 923);
    line(115, 915, 112, 923);
    line(96, 926, 90, 942);
    line(68, 932, 62, 941);
    line(96, 860, 123, 861);
    line(144, 865, 123, 861);
    line(96, 860, 91, 868);
    line(64, 938, 64, 920);
    line(64, 920, 70, 913);
    line(64, 920, 58, 930);
    line(58, 930, 58, 937);
    line(53, 895, 56, 887);
    line(47, 926, 53, 895);
    line(47, 926, 62, 909);
    line(65, 913, 50, 929);
    line(50, 929, 47, 926);
    line(50, 929, 59, 930);
    line(97, 860, 101, 838);
    line(101, 860, 104, 838);
    line(117, 742, 110, 802);
    line(110, 802, 104, 838);
    line(113, 750, 107, 802);
    line(107, 802, 101, 838);
    line(81, 914, 72, 932);
    line(72, 932, 68, 943);
    line(120, 914, 117, 932);
    line(117, 932, 113, 943);
    line(125, 916, 122, 932);
    line(122, 932, 119, 943);
    line(150, 968, 150, 950);
    line(42, 985, 52, 979);
    line(52, 979, 62, 942);
    line(52, 979, 58, 975);
    line(58, 975, 66, 942);
    line(58, 975, 109, 975);
    line(109, 975, 114, 942);
    line(109, 975, 117, 977);
    line(117, 977, 120, 941);
    line(117, 977, 128, 987);
    line(128, 987, 171, 987);
    line(117, 977, 117, 981);
    line(117, 981, 129, 993);
    //line(129, 992, 172, 992);
    line(117, 981, 57, 981);
    line(109, 975, 109, 981);
    line(57, 981, 58, 975);
    line(57, 981, 50, 980);
    line(50, 980, 44, 993);  
    line(57, 981, 59, 983);
    line(59, 983, 54, 993);
    line(120, 993, 109, 981);
    
    
    line_sym(220, 366, 188, 366);
    line_sym(188, 366, 186, 363);
    line_sym(182, 359, 160, 359);
    line_sym(160, 359, 155, 355);
    line_sym(179, 356, 155, 355);
    line_sym(160, 359, 153, 364);
    line_sym(153, 364, 144, 364);
    line_sym(153, 364, 138, 434);
    line_sym(155, 355, 151, 355);
    line_sym(145, 358, 151, 355);
    line_sym(145, 358, 130, 408);
    line_sym(144, 364, 130, 438);
    line_sym(130, 438, 138, 434);
    line_sym(130, 438, 136, 450);   
    line_sym(138, 434, 142, 441);
    line_sym(142, 441, 136, 450);
    line_sym(142, 441, 216, 460);
    line_sym(136, 450, 214, 473);
    line_sym(216, 460, 214, 473);
    line_sym(214, 473, 222, 473);
    line_sym(216, 460, 222, 456);
    line_sym(140, 380, 158, 380);
    line_sym(133, 418, 158, 418);
    line_sym(162, 459, 150, 547);
    line_sym(197, 468, 185, 554);
    line_sym(159, 485, 165, 492);
    line_sym(187, 495, 165, 492);
    line_sym(187, 495, 194, 489);
    line_sym(151, 456, 143, 524);
    line_sym(143, 524, 141, 552);
    line_sym(141, 552, 140, 580);
    line_sym(140, 580, 145, 580);
    line_sym(144, 454, 136, 517);
    line_sym(136, 517, 133, 556);
    line_sym(133, 556, 134, 593);
    line_sym(134, 593, 140, 580);
    line_sym(134, 593, 133, 597);
    line_sym(139, 467, 160, 470); 
    line_sym(196, 474, 218, 478);
    line_sym(218, 483, 219, 474);
    line_sym(209, 472, 200, 528);
    line_sym(193, 559, 200, 528);
    line_sym(193, 559, 186, 586);
    line_sym(182, 584, 186, 586);
    line_sym(186, 586, 188, 597);
    line_sym(146, 574, 182, 579);
    line_sym(143, 588, 180, 593);
    line_sym(143, 588, 140, 580);
    line_sym(180, 593, 186, 586);
    line_sym(119, 634, 133, 597);
    line_sym(119, 634, 113, 630);
    line_sym(119, 634, 118, 640);
    line_sym(118, 640, 107, 649);
    line_sym(118, 640, 116, 708);
    line_sym(107, 648, 101, 676);
    line_sym(101, 676, 99, 699);
    line_sym(99, 699, 102, 704);
    line_sym(119, 661, 180, 670);
    line_sym(180, 670, 182, 642);
    line_sym(182, 642, 186, 587);
    line_sym(175, 669, 180, 593);
    line_sym(190, 651, 188, 605);
    line_sym(190, 651, 198, 660);
    line_sym(190, 643, 198, 641);
    line_sym(144, 588, 127, 662);
    line_sym(123, 661, 141, 583);
    line_sym(186, 668, 180, 670);
    line_sym(186, 668, 190, 652);
    line_sym(186, 668, 175, 717);
    line_sym(192, 712, 188, 716);
    line_sym(197, 692, 192, 712);
    line_sym(198, 660, 197, 692);
    line_sym(175, 669, 160, 739);
    line_sym(180, 670, 164, 739);
    line_sym(164, 739, 158, 739);
    line_sym(164, 739, 166, 742);
    line_sym(166, 742, 162, 745);
    line_sym(123, 662, 120, 732);
    line_sym(127, 662, 124, 732);
    line_sym(120, 732, 124, 732);
    line_sym(127, 733, 124, 732);
    line_sym(116, 734, 120, 732);
    line_sym(116, 741, 119, 742);
    line_sym(110, 838, 95, 838);
    line_sym(110, 838, 114, 842);
    line_sym(114, 842, 140, 846); 
    line_sym(114, 842, 104, 859);
    line_sym(140, 846, 143, 844);
    line_sym(143, 844, 160, 845);
    line_sym(104, 859, 107, 865);
    line_sym(144, 866, 141, 847);
    line_sym(144, 866, 139, 868);
    line_sym(161, 749, 152, 813);
    line_sym(149, 844, 152, 813);
    line_sym(166, 750, 156, 813);
    line_sym(156, 813, 153, 845);
    line_sym(153, 845, 150, 870);
    line_sym(149, 844, 147, 867);
    line_sym(147, 867, 144, 866);
    line_sym(147, 867, 150, 870);
    line_sym(150, 870, 156, 886);
    line_sym(168, 905, 156, 886);
    line_sym(156, 886, 147, 876);
    line_sym(168, 905, 169, 932);
    line_sym(137, 872, 137, 911);
    line_sym(137, 911, 147, 913);
    line_sym(147, 913, 169, 932);
    line_sym(169, 932, 165, 936);
    line_sym(165, 936, 154, 936);
    line_sym(157, 952, 151, 950);
    line_sym(157, 945, 151, 945);
    line_sym(155, 937, 155, 945);
    line_sym(172, 943, 172, 896);
    line_sym(166, 900, 172, 900);
    line_sym(172, 900, 175, 903);
    line_sym(78, 869, 72, 908);
    line_sym(137, 911, 72, 908);
    line_sym(72, 908, 62, 909);
    line_sym(87, 914, 82, 923);
    line_sym(115, 915, 112, 923);
    line_sym(96, 926, 90, 942);
    line_sym(68, 932, 62, 941);
    line_sym(96, 860, 123, 861);
    line_sym(144, 865, 123, 861);
    line_sym(96, 860, 91, 868);
    line_sym(64, 938, 64, 920);
    line_sym(64, 920, 70, 913);
    line_sym(64, 920, 58, 930);
    line_sym(58, 930, 58, 937);
    line_sym(53, 895, 56, 887);
    line_sym(47, 926, 53, 895);
    line_sym(47, 926, 62, 909);
    line_sym(65, 913, 50, 929);
    line_sym(50, 929, 47, 926);
    line_sym(50, 929, 59, 930);
    line_sym(97, 860, 101, 838);
    line_sym(101, 860, 104, 838);
    line_sym(117, 742, 110, 802);
    line_sym(110, 802, 104, 838);
    line_sym(113, 750, 107, 802);
    line_sym(107, 802, 101, 838);
    line_sym(81, 914, 72, 932);
    line_sym(72, 932, 68, 943);
    line_sym(120, 914, 117, 932);
    line_sym(117, 932, 113, 943);
    line_sym(125, 916, 122, 932);
    line_sym(122, 932, 119, 943);
    line_sym(150, 968, 150, 950);
    line_sym(42, 985, 52, 979);
    line_sym(52, 979, 62, 942);
    line_sym(52, 979, 58, 975);
    line_sym(58, 975, 66, 942);
    line_sym(58, 975, 109, 975);
    line_sym(109, 975, 114, 942);
    line_sym(109, 975, 117, 977);
    line_sym(117, 977, 120, 941);
    line_sym(117, 977, 128, 987);
    line_sym(128, 987, 171, 987);
    line_sym(117, 977, 117, 981);
    line_sym(117, 981, 129, 993);
    //line_sym(129, 992, 172, 992);
    line_sym(117, 981, 57, 981);
    line_sym(109, 975, 109, 981);
    line_sym(57, 981, 58, 975);
    line_sym(57, 981, 50, 980);
    line_sym(50, 980, 44, 993);  
    line_sym(57, 981, 59, 983);
    line_sym(59, 983, 54, 993);
    line_sym(120, 993, 109, 981);
    
//Hands
    
    fill(115, 107, 118);
    beginShape();
        vertex(44, 457);
        vertex(44, 460); 
        vertex(52, 460);
        vertex(52, 462);
        vertex(49, 465);
        //vertex(40, 480);
        vertex(54, 482);
        vertex(56, 484);
        vertex(56, 492);
        vertex(61, 498);
        vertex(61, 502);
        vertex(62, 502);
        vertex(66, 498);
        vertex(69, 506);
        vertex(77, 500);
        vertex(70, 478);
        vertex(70, 473);
        vertex(67, 466);
        vertex(65, 461);
        vertex(62, 458);
        vertex(44, 457);
      endShape();
      
      beginShape();
        vertex(63, 507);
        vertex(67, 513);
        vertex(75, 513);
        vertex(81, 505);
        vertex(87, 491);
        vertex(91, 489);
        vertex(96, 489);
        vertex(97, 494);
        vertex(97, 505);
        vertex(105, 507);
       // vertex(106, 509);
       // vertex(106, 511);
        vertex(102, 515);
        vertex(105, 520);
        vertex(87, 532);
        vertex(84, 531);
        vertex(81, 534);
        vertex(63, 530);
        vertex(63, 527);
        vertex(38, 509);
        vertex(63, 507);
      endShape();
      
      fill(157, 148, 149);
      beginShape();
        vertex(61, 502);
        vertex(62, 507);
        vertex(46, 517);
        vertex(39, 514);
        vertex(36, 507);
        vertex(36, 505);
        vertex(35, 474);
        vertex(39, 459);
        vertex(44, 457);
        vertex(44, 460);
        vertex(52, 460);
        vertex(52, 462);
        vertex(49, 465);
        //vertex(40, 480);
        vertex(54, 482);
        vertex(56, 484);
        vertex(56, 492);
        vertex(61, 498);
        vertex(61, 502);
      endShape();
      
      beginShape();
        vertex(68, 474);
        vertex(58, 478);
        vertex(53, 466);
        vertex(63, 462);
        vertex(68, 474);
      endShape();
      
      beginShape();
        vertex(68, 479);
        vertex(75, 496);
        vertex(67, 499);
        vertex(60, 482);
        vertex(68, 479);
      endShape();
      
      beginShape();
        vertex(69, 504);
        vertex(76, 501);
        vertex(82, 518);
        vertex(77, 521);
        vertex(69, 504);
      endShape();
      
      beginShape();
        vertex(50, 514);
        vertex(56, 512);
        vertex(59, 518);
        vertex(61, 528);
        vertex(54, 524);
        vertex(49, 516);
        vertex(50, 514);
      endShape();
      
      beginShape();
          vertex(63, 528);
          vertex(68, 524);
          vertex(76, 524);
          vertex(81, 526);
          vertex(84, 529);
          vertex(82, 534);
          vertex(71, 534);
          vertex(63, 528);
        endShape();
        
      beginShape();
          vertex(87, 533);
          vertex(90, 533);
          vertex(106, 521);
          vertex(104, 518);
          vertex(99, 518);
          vertex(90, 521);
          vertex(87, 533);
        endShape();
        
      beginShape();
          vertex(100, 516);
          vertex(106, 510);
          vertex(105, 507);
          vertex(100, 507);
          vertex(90, 515);
          vertex(92, 517);
          vertex(100, 516);
        endShape();
        
      beginShape();
          vertex(87, 505);
          vertex(90, 511);
          vertex(96, 506);
          vertex(97, 500);
          vertex(95, 493);
          vertex(93, 493);
          vertex(89, 501);
          vertex(86, 507);
          vertex(87, 505);
        endShape();
        
      

    line_sym(220, 366, 188, 366);
    line_sym(188, 366, 186, 363);
    line_sym(182, 359, 160, 359);
    line_sym(160, 359, 155, 355);
    line_sym(179, 356, 155, 355);
    line_sym(160, 359, 153, 364);
    line_sym(153, 364, 144, 364);
    line_sym(153, 364, 138, 434);
    line_sym(155, 355, 151, 355);
    line_sym(145, 358, 151, 355);
    line_sym(145, 358, 130, 408);
    line_sym(144, 364, 130, 438);
    line_sym(130, 438, 138, 434);
    line_sym(130, 438, 136, 450);   
    line_sym(138, 434, 142, 441);
    line_sym(142, 441, 136, 450);
    line_sym(142, 441, 216, 460);
    line_sym(136, 450, 214, 473);
    line_sym(216, 460, 214, 473);
    line_sym(214, 473, 222, 473);
    line_sym(216, 460, 222, 456);
    line_sym(140, 380, 158, 380);
    line_sym(133, 418, 158, 418);
    line_sym(162, 459, 150, 547);
    line_sym(197, 468, 185, 554);
    line_sym(159, 485, 165, 492);
    line_sym(187, 495, 165, 492);
    line_sym(187, 495, 194, 489);
    line_sym(151, 456, 143, 524);
    line_sym(143, 524, 141, 552);
    line_sym(141, 552, 140, 580);
    line_sym(140, 580, 145, 580);
    line_sym(144, 454, 136, 517);
    line_sym(136, 517, 133, 556);
    line_sym(133, 556, 134, 593);
    line_sym(134, 593, 140, 580);
    line_sym(134, 593, 133, 597);
    line_sym(139, 467, 160, 470); 
    line_sym(196, 474, 218, 478);
    line_sym(218, 483, 219, 474);
    line_sym(209, 472, 200, 528);
    line_sym(193, 559, 200, 528);
    line_sym(193, 559, 186, 586);
    line_sym(182, 584, 186, 586);
    line_sym(186, 586, 188, 597);
    line_sym(146, 574, 182, 579);
    line_sym(143, 588, 180, 593);
    line_sym(143, 588, 140, 580);
    line_sym(180, 593, 186, 586);
    line_sym(119, 634, 133, 597);
    line_sym(119, 634, 113, 630);
    line_sym(119, 634, 118, 640);
    line_sym(118, 640, 107, 649);
    line_sym(118, 640, 116, 708);
    fill(115, 107, 118);
    beginShape();
        vertex_sym(44, 457);
        vertex_sym(44, 460); 
        vertex_sym(52, 460);
        vertex_sym(52, 462);
        vertex_sym(49, 465);
        //vertex_sym(40, 480);
        vertex_sym(54, 482);
        vertex_sym(56, 484);
        vertex_sym(56, 492);
        vertex_sym(61, 498);
        vertex_sym(61, 502);
        vertex_sym(62, 502);
        vertex_sym(66, 498);
        vertex_sym(69, 506);
        vertex_sym(77, 500);
        vertex_sym(70, 478);
        vertex_sym(70, 473);
        vertex_sym(67, 466);
        vertex_sym(65, 461);
        vertex_sym(62, 458);
        vertex_sym(44, 457);
      endShape();
      
      beginShape();
        vertex_sym(63, 507);
        vertex_sym(67, 513);
        vertex_sym(75, 513);
        vertex_sym(81, 505);
        vertex_sym(87, 491);
        vertex_sym(91, 489);
        vertex_sym(96, 489);
        vertex_sym(97, 494);
        vertex_sym(97, 505);
        vertex_sym(105, 507);
       // vertex_sym(106, 509);
       // vertex_sym(106, 511);
        vertex_sym(102, 515);
        vertex_sym(105, 520);
        vertex_sym(87, 532);
        vertex_sym(84, 531);
        vertex_sym(81, 534);
        vertex_sym(63, 530);
        vertex_sym(63, 527);
        vertex_sym(38, 509);
        vertex_sym(63, 507);
      endShape();
      
      fill(157, 148, 149);
      beginShape();
        vertex_sym(61, 502);
        vertex_sym(62, 507);
        vertex_sym(46, 517);
        vertex_sym(39, 514);
        vertex_sym(36, 507);
        vertex_sym(36, 505);
        vertex_sym(35, 474);
        vertex_sym(39, 459);
        vertex_sym(44, 457);
        vertex_sym(44, 460);
        vertex_sym(52, 460);
        vertex_sym(52, 462);
        vertex_sym(49, 465);
        //vertex_sym(40, 480);
        vertex_sym(54, 482);
        vertex_sym(56, 484);
        vertex_sym(56, 492);
        vertex_sym(61, 498);
        vertex_sym(61, 502);
      endShape();
      
      beginShape();
        vertex_sym(68, 474);
        vertex_sym(58, 478);
        vertex_sym(53, 466);
        vertex_sym(63, 462);
        vertex_sym(68, 474);
      endShape();
      
      beginShape();
        vertex_sym(68, 479);
        vertex_sym(75, 496);
        vertex_sym(67, 499);
        vertex_sym(60, 482);
        vertex_sym(68, 479);
      endShape();
      
      beginShape();
        vertex_sym(69, 504);
        vertex_sym(76, 501);
        vertex_sym(82, 518);
        vertex_sym(77, 521);
        vertex_sym(69, 504);
      endShape();
      
      beginShape();
        vertex_sym(50, 514);
        vertex_sym(56, 512);
        vertex_sym(59, 518);
        vertex_sym(61, 528);
        vertex_sym(54, 524);
        vertex_sym(49, 516);
        vertex_sym(50, 514);
      endShape();
      
      beginShape();
          vertex_sym(63, 528);
          vertex_sym(68, 524);
          vertex_sym(76, 524);
          vertex_sym(81, 526);
          vertex_sym(84, 529);
          vertex_sym(82, 534);
          vertex_sym(71, 534);
          vertex_sym(63, 528);
        endShape();
        
      beginShape();
          vertex_sym(87, 533);
          vertex_sym(90, 533);
          vertex_sym(106, 521);
          vertex_sym(104, 518);
          vertex_sym(99, 518);
          vertex_sym(90, 521);
          vertex_sym(87, 533);
        endShape();
        
      beginShape();
          vertex_sym(100, 516);
          vertex_sym(106, 510);
          vertex_sym(105, 507);
          vertex_sym(100, 507);
          vertex_sym(90, 515);
          vertex_sym(92, 517);
          vertex_sym(100, 516);
        endShape();
        
      beginShape();
          vertex_sym(87, 505);
          vertex_sym(90, 511);
          vertex_sym(96, 506);
          vertex_sym(97, 500);
          vertex_sym(95, 493);
          vertex_sym(93, 493);
          vertex_sym(89, 501);
          vertex_sym(86, 507);
          vertex_sym(87, 505);
        endShape();        
        
          
   
          


fill(0,0,255);
if(mousePressed)
{
text("x:"+mouseX+"y:"+mouseY,mouseX,mouseY);
}
 
}

void vertex_sym(float x, float y)
{
vertex(xSym(x),y);
}

void line_sym(float x, float y, float x1, float y1)
{
line(xSym(x),y ,xSym(x1), y1);
}

float xSym(float x)
{
float mirrorX=250; 
float factor=x-mirrorX;
x-=2*factor;
return x;
}

 

