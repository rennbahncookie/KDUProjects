﻿using UnityEngine;
using System.Collections;

public class DirectorScript : MonoBehaviour {

    public GameObject Title;
    public GameObject Drink;
    public GameObject rainbow;
    public GameObject Lights;
    public GameObject sheep;

    float timetoDrink = 2.5f;
    float timetoFly = 1.0f;
    float time;
    float timer2;
    AudioSource holylightsound;
    bool soundplayed = false;

	// Use this for initialization
	void Start () {
        holylightsound = GetComponent<AudioSource>();
	
	}
	
	// Update is called once per frame
	void Update () {

        if(Title.GetComponent<Transform>().localScale.x == 0)
        {
            Drink.SetActive(true);
        }
        if (Drink.activeSelf)
        {
            time += Time.deltaTime;
            if(time>timetoDrink)
            {
                Lights.SetActive(true);
                sheep.GetComponent<PolygonCollider2D>().enabled = true;
                sheep.GetComponent<Animator>().enabled = true;
                playSound();

            }

            if(sheep.GetComponent<MooScript>().clicks >= 100)
            {
                rainbow.SetActive(true);
                sheep.GetComponent<PolygonCollider2D>().enabled = false;
                timer2 += Time.deltaTime;
                if(timer2>timetoFly)
                {
                    sheep.GetComponent<Animator>().Play("State6Fly");
                
                }


            }
        }

        if(sheep.GetComponent<Transform>().localScale.x == 0)
        {
            Application.LoadLevel("Credits");
        }
	
	}
    
    void playSound()
    {
        if(soundplayed == false)
        {
            if (!holylightsound.isPlaying)
            {
                holylightsound.Play();
                soundplayed = true;

            }
        }


    }
}
