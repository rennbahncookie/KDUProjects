﻿using UnityEngine;
using System.Collections;

public class NumberSpawnScript : MonoBehaviour
{
    Vector3 moveup;
    float timer = 3.0f;
    float time;



    // Use this for initialization
    void Start()
    {
        StartCoroutine("fade_out");
        moveup = transform.position;
        
    }

    // Update is called once per frame
    void Update()
    {
        moveup.y -= transform.position.y * Time.deltaTime * 2;
        transform.position = moveup;
        time+= Time.deltaTime;
        if(timer < time)
        {
            Destroy(this.gameObject);
        }
    }

    IEnumerator fade_out()
    {
        for (float alpha = 1.5f; alpha >= 0.0f; alpha -= 0.02f)
        {
            Color c = renderer.material.color;
            c.a = alpha;
            renderer.material.color = c;
            yield return null;
        }
    }
}
