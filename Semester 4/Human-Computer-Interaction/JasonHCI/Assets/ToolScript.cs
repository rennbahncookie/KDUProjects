﻿using UnityEngine;
using System.Collections;

public class ToolScript : MonoBehaviour {
    public bool MouseDown = false;
    public bool ToolEnabled;
    public bool ToolSelected;
    public Sprite enabled;
    public Sprite disabled;
    SpriteRenderer sr;

    public Texture2D cursorChange;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotspot = Vector2.zero;


	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
	
	}
	
	// Update is called once per frame
	void Update () {

        if(ToolEnabled)
        {
            sr.sprite = enabled;
        }
        else
        {
            sr.sprite = disabled;
        }
	
	}

    void OnMouseDown()
    {
        MouseDown = true;
        if(ToolEnabled)
        {
            ToolSelected = true;
            Cursor.SetCursor(cursorChange, hotspot, cursorMode);
 
        }
    }
}




