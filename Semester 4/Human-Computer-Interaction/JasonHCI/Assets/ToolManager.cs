﻿using UnityEngine;
using System.Collections;

public class ToolManager : MonoBehaviour {

    ToolScript _hand;
    ToolScript _glove;
    ToolScript _hammer;
    ToolScript _shaver;
    ToolScript _dynamite;

    AudioSource Sound;

    public AudioClip SoundExplosion;
    public AudioClip SoundPunch;
    public AudioClip SoundSlap;
    public AudioClip SoundHammer;
    public AudioClip SoundShave;

    public GameObject explosion;
    public GameObject slap;
    public GameObject punch;
    public GameObject shave;
    public GameObject hammer;

    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotspot = Vector2.zero;

    Animator _bunnyAnim;

	// Use this for initialization
	void Start () {
        Sound = GetComponent<AudioSource>();
        _dynamite = GameObject.FindGameObjectWithTag("Dynamite").GetComponent<ToolScript>();
        _glove = GameObject.FindGameObjectWithTag("Glove").GetComponent<ToolScript>();
        _hammer = GameObject.FindGameObjectWithTag("Hammer").GetComponent<ToolScript>();
        _shaver = GameObject.FindGameObjectWithTag("Shaver").GetComponent<ToolScript>();
        _hand = GameObject.FindGameObjectWithTag("Hand").GetComponent<ToolScript>();

        _bunnyAnim = GameObject.FindGameObjectWithTag("Bunny").GetComponent<Animator>();

	
	}
	
	// Update is called once per frame
	void Update () {
        if(_hand.ToolSelected)
        {
            _glove.ToolSelected = false;
            _hammer.ToolSelected = false;
            _shaver.ToolSelected = false;
            _dynamite.ToolSelected = false;
        }

        if (_glove.ToolSelected)
        {
            _hand.ToolSelected = false;
            _hammer.ToolSelected = false;
            _shaver.ToolSelected = false;
            _dynamite.ToolSelected = false;
        }

        if (_hammer.ToolSelected)
        {
            _glove.ToolSelected = false;
            _hand.ToolSelected = false;
            _shaver.ToolSelected = false;
            _dynamite.ToolSelected = false;
        }

        if (_shaver.ToolSelected)
        {
            _glove.ToolSelected = false;
            _hammer.ToolSelected = false;
            _hand.ToolSelected = false;
            _dynamite.ToolSelected = false;
        }

        if (_dynamite.ToolSelected)
        {
            _glove.ToolSelected = false;
            _hammer.ToolSelected = false;
            _shaver.ToolSelected = false;
            _hand.ToolSelected = false;
        }
        
        if(_bunnyAnim.GetBool("BunnySlapped"))
        {
            _glove.ToolEnabled = true;
            _hammer.ToolEnabled = false;
            _shaver.ToolEnabled = false;
            _dynamite.ToolEnabled = false;
            _hand.ToolEnabled = false;
        }

        if(_bunnyAnim.GetBool("BunnyPunched"))
        {
            _hammer.ToolEnabled = true;
            _glove.ToolEnabled = false;
           // _hammer.ToolEnabled = false;
            _shaver.ToolEnabled = false;
            _dynamite.ToolEnabled = false;
            _hand.ToolEnabled = false;
        }

        if(_bunnyAnim.GetBool("BunnyHammered"))
        {
            _shaver.ToolEnabled = true;
            _glove.ToolEnabled = false;
            _hammer.ToolEnabled = false;
           // _shaver.ToolEnabled = false;
            _dynamite.ToolEnabled = false;
            _hand.ToolEnabled = false;
        }

        if(_bunnyAnim.GetBool("BunnyShaved"))
        {
            _dynamite.ToolEnabled = true;
            _glove.ToolEnabled = false;
            _hammer.ToolEnabled = false;
            _shaver.ToolEnabled = false;
           // _dynamite.ToolEnabled = false;
            _hand.ToolEnabled = false;
        }

        if(_bunnyAnim.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
        {
            Application.LoadLevel("MainScene");
        }


	
	}

    void OnMouseDown()
    {
        if(_hand.ToolEnabled && _hand.ToolSelected)
        {
            Sound.clip = SoundSlap;
            Sound.Play();
            Instantiate(slap);
            _bunnyAnim.SetBool("BunnySlapped", true);
            _hand.MouseDown = false;
            _hand.ToolSelected = false;
            _hand.ToolEnabled = false;
        }

        if (_shaver.ToolEnabled && _shaver.ToolSelected)
        {
            Sound.clip = SoundShave;
            Sound.Play();
            Instantiate(shave);
            _bunnyAnim.SetBool("BunnyShaved", true);
            _shaver.MouseDown = false;
            _shaver.ToolSelected = false;
            _shaver.ToolEnabled = false;
        }

        if (_hammer.ToolEnabled && _hammer.ToolSelected)
        {
            Sound.clip = SoundHammer;
            Sound.Play();
            Instantiate(hammer);
            _bunnyAnim.SetBool("BunnyHammered", true);
            _hammer.MouseDown = false;
            _hammer.ToolSelected = false;
            _hammer.ToolEnabled = false;

        }

        if (_glove.ToolEnabled && _glove.ToolSelected)
        {
            Sound.clip = SoundPunch;
            Sound.Play();
            Instantiate(punch);
            _bunnyAnim.SetBool("BunnyPunched", true);
            _glove.MouseDown = false;
            _glove.ToolSelected = false;
            _glove.ToolEnabled = false;

        }

        if (_dynamite.ToolEnabled && _dynamite.ToolSelected)
        {
            Sound.clip = SoundExplosion;
            Sound.Play();
            Instantiate(explosion);
            _bunnyAnim.SetBool("BunnyExploded", true);
            _dynamite.MouseDown = false;
            _dynamite.ToolSelected = false;
            _dynamite.ToolEnabled = false;

        }

        Cursor.SetCursor(null, Vector2.zero, cursorMode);
    }
}

//        anim.SetInteger("Clicks", clicks);
//        anim.SetBool("Licking", licking);