﻿using UnityEngine;
using System.Collections;

public class DoorScript : MonoBehaviour 
{

	GameObject player;
	Player playerScript;
	Rect textArea;
	bool collidingWithPlayer=false;
	[SerializeField] string newSceneName;
	// Use this for initialization
	void Start () 
	{
		player=GameObject.Find("/Player");
		playerScript = player.GetComponent<Player>();
		Vector2 screenPos= Camera.main.WorldToScreenPoint(transform.position);
		screenPos.y*=2;
		textArea=new Rect(screenPos.x,screenPos.y,200,200);	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	void OnTriggerStay2D(Collider2D other)
	{
		if(other.tag =="Player")
		{
			collidingWithPlayer=true;
		}
	}
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.tag =="Player")
		{
			collidingWithPlayer=false;
		}
	}
	void OnGUI()
	{
		if(collidingWithPlayer&&playerScript.currentItem==ItemList.Key)
			GUI.Label(textArea,"Enter Door");
	}
	void OnMouseDown()
	{
		if(collidingWithPlayer&&playerScript.currentItem==ItemList.Key)
			Application.LoadLevel(newSceneName);
	}

}
