using UnityEngine;
using System.Collections;
public enum PlayerAction{idle,walking,goingToLadder,climbingLadder,climbingDownLadder}
public enum ItemList{Empty,Key,BlueOrb,PurpleOrb}
public class Player : MonoBehaviour 
{

	public Vector2 position;
	//private int direction=1;
	public float speed=2.5f;
	public float targetX;
	public bool canMove=true;
	public PlayerAction currentAction;
	public ItemList currentItem;
	public bool canFall;

	void moveToPosition(float posX)
	{
		targetX = posX;
	}
	void HandleStates()
	{
		if(currentAction !=PlayerAction.climbingLadder&&currentAction !=PlayerAction.climbingDownLadder)
		{
			if(position.x==targetX)
				currentAction=PlayerAction.idle;
			else if(position.x !=targetX)
			 	currentAction=PlayerAction.walking;
		}

	}
	void HandleGravity()
	{
		if(canFall)
			gameObject.rigidbody2D.gravityScale=1;
		else
			gameObject.rigidbody2D.gravityScale=0;
	}

	void Start () 
	{
		targetX = transform.position.x;
		currentAction = PlayerAction.idle;
		canFall=true;

	}


	void Update () 
	{
	
		SpriteRenderer renderer;
		renderer =(SpriteRenderer)gameObject.renderer;
		position = transform.position;
		Vector3 worldMouse=Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y,0));
		if (Input.GetMouseButton(0))
		{
			if(canMove)
			{
				moveToPosition(worldMouse.x);
			}   
		}
		HandleGravity();
		HandleStates();

		transform.position = Vector2.MoveTowards(new Vector2(transform.position.x,transform.position.y), new Vector2(targetX, position.y),speed*Time.deltaTime);

	}


}
