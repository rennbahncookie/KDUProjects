﻿using UnityEngine;
using System.Collections;

public class LadderScript : MonoBehaviour {
	private GameObject player;

	void setLadderCollision()
	{
		Player playerScript=player.GetComponent<Player>();

		SpriteRenderer ladderRenderer;
		ladderRenderer =(SpriteRenderer)gameObject.renderer;
		
		SpriteRenderer playerRenderer;
		playerRenderer =(SpriteRenderer)player.renderer;

		float playerBoundsY=playerRenderer.bounds.size.y/2;
		float ladderBoundsY=ladderRenderer.bounds.size.y/2;

		if(player.transform.position.y-playerBoundsY >=transform.position.y+ladderBoundsY-0.3f&&playerScript.currentAction!=PlayerAction.climbingDownLadder)
		{

			transform.collider2D.isTrigger=false;
	
		}
		else
		{
			transform.collider2D.isTrigger=true;
		}
	}
	void Start () 
	{
		player = GameObject.Find("/Player");
	}
	
	// Update is called once per frame
	void Update () 
	{
		setLadderCollision();
	}
}
