﻿using UnityEngine;
using System.Collections;

public class NoteScript : MonoBehaviour 
{
	public enum Letter
	{
		A = 0,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z
	}
	
	public Letter letter = Letter.A;
	//temp timer
	public float elapsedTime = 0.0f;

	public float dropSpeed = 1.0f;
	private float dropSpeedChanger = 9.0f;
	public int NoteLane = 1;
	public Vector3 NoteWidthValue;

	//changing sprite
	public Sprite noteTyped;
	public Sprite noteUntyped;
	SpriteRenderer sr;
	private float spriteChangeTime = 9.0f;


	//letter sprites

	public Sprite Untyped_A;
	public Sprite Untyped_B;
	public Sprite Untyped_C;
	public Sprite Untyped_D;
	public Sprite Untyped_E;
	public Sprite Untyped_F;
	public Sprite Untyped_G;
	public Sprite Untyped_H;
	public Sprite Untyped_I;
	public Sprite Untyped_J;
	public Sprite Untyped_K;
	public Sprite Untyped_L;
	public Sprite Untyped_M;
	public Sprite Untyped_N;
	public Sprite Untyped_O;
	public Sprite Untyped_P;
	public Sprite Untyped_Q;
	public Sprite Untyped_R;
	public Sprite Untyped_S;
	public Sprite Untyped_T;
	public Sprite Untyped_U;
	public Sprite Untyped_V;
	public Sprite Untyped_W;
	public Sprite Untyped_X;
	public Sprite Untyped_Y;
	public Sprite Untyped_Z;
	
	public Sprite Typed_A;
	public Sprite Typed_B;
	public Sprite Typed_C;
	public Sprite Typed_D;
	public Sprite Typed_E;
	public Sprite Typed_F;
	public Sprite Typed_G;
	public Sprite Typed_H;
	public Sprite Typed_I;
	public Sprite Typed_J;
	public Sprite Typed_K;
	public Sprite Typed_L;
	public Sprite Typed_M;
	public Sprite Typed_N;
	public Sprite Typed_O;
	public Sprite Typed_P;
	public Sprite Typed_Q;
	public Sprite Typed_R;
	public Sprite Typed_S;
	public Sprite Typed_T;
	public Sprite Typed_U;
	public Sprite Typed_V;
	public Sprite Typed_W;
	public Sprite Typed_X;
	public Sprite Typed_Y;
	public Sprite Typed_Z;

	static T GetRandomEnum<T>()
	{
		System.Array A = System.Enum.GetValues(typeof(T));
		T V = (T)A.GetValue(UnityEngine.Random.Range(0,A.Length));
		//T V = (T)A.GetValue(UnityEngine.Random.Range(0,1)); //used to test single arrow
		return V;
	}

	public void RandomLetter()
	{
		letter = GetRandomEnum<Letter> ();
	}




	// Use this for initialization
	void Start () 
	{
		sr = GetComponent<SpriteRenderer> ();
		ChangeSprite ();
		//ChooseLetter (20);
		//RandomLetter ();
		//ChooseLetter (GameObject.Find);
	}
	
	// Update is called once per frame
	void Update () 
	{
		spriteChangeTime = dropSpeedChanger/dropSpeed;

		elapsedTime += Time.deltaTime;
		transform.Translate(-Vector2.up * dropSpeed * Time.deltaTime);

		//ChangeSprite();

		if(elapsedTime >= spriteChangeTime)
		{
			sr.sprite = noteTyped;
			elapsedTime = 0.0f;
		}

	}

	public void ChooseLane()
	{
		NoteLane = 1;
	}

	public void ChooseLetter(char letterchar) 
	//public void ChooseLetter() //insert enum value
	{
		//call lettermanagerscript function to return sprites
		//letterNumber = 10;

		char a = letterchar;
		int letterNumber =(int)a;
		Debug.Log("letternumber" + letterNumber);

		if(letterNumber == 65)
		{
			letter = Letter.A;
		}

		if(letterNumber == 66)
		{
			letter = Letter.B;
		}

		if(letterNumber == 67)
		{
			letter = Letter.C;
		}

		if(letterNumber == 68)
		{
			letter = Letter.D;
		}

		if(letterNumber == 69)
		{
			letter = Letter.E;
		}

		if(letterNumber == 70)
		{
			letter = Letter.F;
		}

		if(letterNumber == 71)
		{
			letter = Letter.G;
		}

		if(letterNumber == 72)
		{
			letter = Letter.H;
		}

		if(letterNumber == 73)
		{
			letter = Letter.I;
		}

		if(letterNumber == 74)
		{
			letter = Letter.J;
		}

		if(letterNumber == 75)
		{
			letter = Letter.K;
		}

		if(letterNumber == 76)
		{
			letter = Letter.L;
		}

		if(letterNumber == 77)
		{
			letter = Letter.M;
		}

		if(letterNumber == 78)
		{
			letter = Letter.N;
		}

		if(letterNumber == 79)
		{
			letter = Letter.O;
		}

		if(letterNumber == 80)
		{
			letter = Letter.P;
		}

		if(letterNumber == 81)
		{
			letter = Letter.Q;
		}

		if(letterNumber == 82)
		{
			letter = Letter.R;
		}

		if(letterNumber == 83)
		{
			letter = Letter.S;
		}

		if(letterNumber == 84)
		{
			letter = Letter.T;
		}

		if(letterNumber == 85)
		{
			letter = Letter.U;
		}

		if(letterNumber == 86)
		{
			letter = Letter.V;
		}

		if(letterNumber == 87)
		{
			letter = Letter.W;
		}

		if(letterNumber == 88)
		{
			letter = Letter.X;
		}

		if(letterNumber == 89)
		{
			letter = Letter.Y;
		}

		if(letterNumber == 90)
		{
			letter = Letter.Z;
		}

		ChangeSprite();
	}

	public void ChangeSprite()
	{
		if(letter == Letter.A)
		{
			sr.sprite = Untyped_A;
			noteTyped = Typed_A;
		}
		if(letter == Letter.B)
		{
			sr.sprite = Untyped_B;
			noteTyped = Typed_B;
		}
		
		if(letter == Letter.C)
		{
			sr.sprite = Untyped_C;
			noteTyped = Typed_C;
		}
		
		if(letter == Letter.D)
		{
			sr.sprite = Untyped_D;
			noteTyped = Typed_D;
		}
		
		if(letter == Letter.E)
		{
			sr.sprite = Untyped_E;
			noteTyped = Typed_E;
		}
		
		if(letter == Letter.F)
		{
			sr.sprite = Untyped_F;
			noteTyped = Typed_F;
		}
		
		if(letter == Letter.G)
		{
			sr.sprite = Untyped_G;
			noteTyped = Typed_G;
		}
		
		if(letter == Letter.H)
		{
			sr.sprite = Untyped_H;
			noteTyped = Typed_H;
		}
		
		if(letter == Letter.I)
		{
			sr.sprite = Untyped_I;
			noteTyped = Typed_I;
		}
		
		if(letter == Letter.J)
		{
			sr.sprite = Untyped_J;
			noteTyped = Typed_J;
		}
		
		if(letter == Letter.K)
		{
			sr.sprite = Untyped_K;
			noteTyped = Typed_K;
		}
		
		if(letter == Letter.L)
		{
			sr.sprite = Untyped_L;
			noteTyped = Typed_L;
		}
		
		if(letter == Letter.M)
		{
			sr.sprite = Untyped_M;
			noteTyped = Typed_M;
		}
		
		if(letter == Letter.N)
		{
			sr.sprite = Untyped_N;
			noteTyped = Typed_N;
		}
		
		if(letter == Letter.O)
		{
			sr.sprite = Untyped_O;
			noteTyped = Typed_O;
		}
		
		if(letter == Letter.P)
		{
			sr.sprite = Untyped_P;
			noteTyped = Typed_P;
		}
		
		if(letter == Letter.Q)
		{
			sr.sprite = Untyped_Q;
			noteTyped = Typed_Q;
		}
		
		if(letter == Letter.R)
		{
			sr.sprite = Untyped_R;
			noteTyped = Typed_R;
		}
		
		if(letter == Letter.S)
		{
			sr.sprite = Untyped_S;
			noteTyped = Typed_S;
		}
		
		if(letter == Letter.T)
		{
			sr.sprite = Untyped_T;
			noteTyped = Typed_T;
		}
		
		if(letter == Letter.U)
		{
			sr.sprite = Untyped_U;
			noteTyped = Typed_U;
		}
		
		if(letter == Letter.V)
		{
			sr.sprite = Untyped_V;
			noteTyped = Typed_V;
		}
		
		if(letter == Letter.W)
		{
			sr.sprite = Untyped_W;
			noteTyped = Typed_W;
		}
		
		if(letter == Letter.X)
		{
			sr.sprite = Untyped_X;
			noteTyped = Typed_X;
		}
		
		if(letter == Letter.Y)
		{
			sr.sprite = Untyped_Y;
			noteTyped = Typed_Y;
		}
		
		if(letter == Letter.Z)
		{
			sr.sprite = Untyped_Z;
			noteTyped = Typed_Z;
		}
	}

	public void NoteWidth()
	{
		NoteWidthValue = renderer.bounds.size;
	}

	public void Kill()
	{
		GameObject.Find ("Spawner").GetComponent<SpawnerScript> ().spawnedNotes--;
		Destroy(gameObject);
	}
}
