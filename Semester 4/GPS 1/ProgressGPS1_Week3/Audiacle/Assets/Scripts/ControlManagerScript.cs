﻿using UnityEngine;
using System.Collections;

public class ControlManagerScript : MonoBehaviour {

	//Keys Pressed?
	public bool IsPressedA = false;
	public bool IsPressedB = false;
	public bool IsPressedC = false;
	public bool IsPressedD = false;
	public bool IsPressedE = false;
	public bool IsPressedF = false;
	public bool IsPressedG = false;
	public bool IsPressedH = false;
	public bool IsPressedI = false;
	public bool IsPressedJ = false;
	public bool IsPressedK = false;
	public bool IsPressedL = false;
	public bool IsPressedM = false;
	public bool IsPressedN = false;
	public bool IsPressedO = false;
	public bool IsPressedP = false;
	public bool IsPressedQ = false;
	public bool IsPressedR = false;
	public bool IsPressedS = false;
	public bool IsPressedT = false;
	public bool IsPressedU = false;
	public bool IsPressedV = false;
	public bool IsPressedW = false;
	public bool IsPressedX = false;
	public bool IsPressedY = false;
	public bool IsPressedZ = false;

	public float inputTimer = 0.05f;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		InputCollection ();

	
	}
	//collection of intput
	void InputCollection()
	{
		InKeyA();
		InKeyB();
		InKeyC();
		InKeyD();
		InKeyE();
		InKeyF();
		InKeyG();
		InKeyH();
		InKeyI();
		InKeyJ();
		InKeyK();
		InKeyL();
		InKeyM();
		InKeyN();
		InKeyO();
		InKeyP();
		InKeyQ();
		InKeyR();
		InKeyS();
		InKeyT();
		InKeyU();
		InKeyV();
		InKeyW();
		InKeyX();
		InKeyY();
		InKeyZ();
	}

	//input keys functions
	void InKeyA()
	{
		if(Input.GetKeyDown(KeyCode.A))
		{
			IsPressedA = true;
			Debug.Log ("A pressed");
		}
		if(IsPressedA)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedA = false;
			}
		}
	}
	
	void InKeyB()
	{
		if(Input.GetKeyDown(KeyCode.B))
		{
			IsPressedB = true;
			Debug.Log ("B pressed");
		}
		if(IsPressedB)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedB = false;
			}
		}
	}
	
	void InKeyC()
	{
		if(Input.GetKeyDown(KeyCode.C))
		{
			IsPressedC = true;
			Debug.Log ("C pressed");
		}
		if(IsPressedC)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedC = false;
			}
		}
	}
	
	void InKeyD()
	{
		if(Input.GetKeyDown(KeyCode.D))
		{
			IsPressedD = true;
			Debug.Log ("D pressed");
		}
		if(IsPressedD)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedD = false;
			}
		}
	}
	
	void InKeyE()
	{
		if(Input.GetKeyDown(KeyCode.E))
		{
			IsPressedE = true;
			Debug.Log ("E pressed");
			//GameObject.Find("Spawner").GetComponent<SpawnerScript>().Spawn(SpawnerScript.Lane.Lane_3);
		}
		if(IsPressedE)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedE = false;
			}
		}
	}
	
	void InKeyF()
	{
		if(Input.GetKeyDown(KeyCode.F))
		{
			IsPressedF = true;
			Debug.Log ("F pressed");
		}
		if(IsPressedF)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedF = false;
			}
		}
	}
	
	void InKeyG()
	{
		if(Input.GetKeyDown(KeyCode.G))
		{
			IsPressedG = true;
			Debug.Log ("G pressed");
		}
		if(IsPressedG)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedG = false;
			}
		}
	}
	
	void InKeyH()
	{
		if(Input.GetKeyDown(KeyCode.H))
		{
			IsPressedH = true;
			Debug.Log ("h pressed");
		}
		if(IsPressedH)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedH = false;
			}
		}
	}
	
	void InKeyI()
	{
		if(Input.GetKeyDown(KeyCode.I))
		{
			IsPressedI = true;
			Debug.Log ("I pressed");
		}
		if(IsPressedI)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedI = false;
			}
		}
	}
	
	void InKeyJ()
	{
		if(Input.GetKeyDown(KeyCode.J))
		{
			IsPressedJ = true;
			Debug.Log ("J pressed");
		}
		if(IsPressedJ)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedJ = false;
			}
		}
	}
	
	void InKeyK()
	{
		if(Input.GetKeyDown(KeyCode.K))
		{
			IsPressedK = true;
			Debug.Log ("K pressed");
		}
		if(IsPressedK)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedK = false;
			}
		}
	}
	
	void InKeyL()
	{
		if(Input.GetKeyDown(KeyCode.L))
		{
			IsPressedL = true;
			Debug.Log ("L pressed");
		}
		if(IsPressedL)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedL = false;
			}
		}
	}
	
	void InKeyM()
	{
		if(Input.GetKeyDown(KeyCode.M))
		{
			IsPressedM = true;
			Debug.Log ("M pressed");
		}
		if(IsPressedM)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedM = false;
			}
		}
	}
	
	void InKeyN()
	{
		if(Input.GetKeyDown(KeyCode.N))
		{
			IsPressedN = true;
			Debug.Log ("N pressed");
		}
		if(IsPressedN)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedN = false;
			}
		}
	}
	
	void InKeyO()
	{
		if(Input.GetKeyDown(KeyCode.O))
		{
			IsPressedO = true;
			Debug.Log ("O pressed");
		}
		if(IsPressedO)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedO = false;
			}
		}
	}
	
	void InKeyP()
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			IsPressedP = true;
			Debug.Log ("P pressed");
		}
		if(IsPressedP)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedP = false;
			}
		}
	}
	
	void InKeyQ()
	{
		if(Input.GetKeyDown(KeyCode.Q))
		{
			IsPressedQ = true;
			Debug.Log ("Q pressed");
			GameObject.Find("Spawner").GetComponent<SpawnerScript>().Spawn(2);
			GameObject.Find("Note(Clone)").GetComponent<NoteScript>().ChooseLetter('L');
		}
		if(IsPressedQ)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedQ = false;
			}
		}
	}
	
	void InKeyR()
	{
		if(Input.GetKeyDown(KeyCode.R))
		{
			IsPressedR = true;
			Debug.Log ("R pressed");
			//GameObject.Find("Spawner").GetComponent<SpawnerScript>().Spawn(SpawnerScript.Lane.Lane_4);
		}
		if(IsPressedR)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedR = false;
			}
		}
	}
	
	void InKeyS()
	{
		if(Input.GetKeyDown(KeyCode.S))
		{
			IsPressedS = true;
			Debug.Log ("S pressed");
		}
		if(IsPressedS)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedS = false;
			}
		}
	}
	
	void InKeyT()
	{
		if(Input.GetKeyDown(KeyCode.T))
		{
			IsPressedT = true;
			Debug.Log ("T pressed");
			//GameObject.Find("Spawner").GetComponent<SpawnerScript>().Spawn(SpawnerScript.Lane.Lane_5);
		}
		if(IsPressedT)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedT = false;
			}
		}
	}
	
	void InKeyU()
	{
		if(Input.GetKeyDown(KeyCode.U))
		{
			IsPressedU = true;
			Debug.Log ("U pressed");
		}
		if(IsPressedU)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedU = false;
			}
		}
	}
	
	void InKeyV()
	{
		if(Input.GetKeyDown(KeyCode.V))
		{
			IsPressedV = true;
			Debug.Log ("V pressed");
		}
		if(IsPressedV)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedV = false;
			}
		}
	}
	
	void InKeyW()
	{
		if(Input.GetKeyDown(KeyCode.W))
		{
			IsPressedW = true;
			Debug.Log ("W pressed");
			//GameObject.Find("Spawner").GetComponent<SpawnerScript>().Spawn(SpawnerScript.Lane.Lane_2);
		}
		if(IsPressedW)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedW = false;
			}
		}
	}
	
	void InKeyX()
	{
		if(Input.GetKeyDown(KeyCode.X))
		{
			IsPressedX = true;
			Debug.Log ("X pressed");
			if(GameObject.Find("Spawner").GetComponent<SpawnerScript>().spawnedNotes >= 1)
			{
				GameObject.Find("Note(Clone)").GetComponent<NoteScript>().Kill();
			}

		}
		if(IsPressedX)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedX = false;
			}
		}
	}
	
	void InKeyY()
	{
		if(Input.GetKeyDown(KeyCode.Y))
		{
			IsPressedY = true;
			Debug.Log ("Y pressed");
			//GameObject.Find("Spawner").GetComponent<SpawnerScript>().Spawn(SpawnerScript.Lane.Lane_6);
		}
		if(IsPressedY)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedY = false;
			}
		}
	}
	
	void InKeyZ()
	{
		if(Input.GetKeyDown(KeyCode.Z))
		{
			IsPressedZ = true;
			Debug.Log ("Z pressed");
		}
		if(IsPressedZ)
		{
			inputTimer -= Time.deltaTime;
			
			if(inputTimer < 0)
			{
				inputTimer = 0.05f;
				IsPressedZ = false;
			}
		}
	}


}
