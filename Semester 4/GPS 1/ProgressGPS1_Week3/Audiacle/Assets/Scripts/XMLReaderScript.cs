﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Collections.Generic;

//[XmlRoot]("note-set")]

public class XMLReaderScript : MonoBehaviour 
{
	public float globalTimer = 0.0f;
	public float checkTimer = 0.0f;
	public float resetCheckTimer = 2.0f;

	public string laneNumberString;
	public int laneNumber;

	public string LetterString;
	public char Letter;

	public string TimeString;
	public int NoteSpawnTime;
	public int FirstNoteSpawnTime;
	public int noteCount = 0;

	public string NoteTimer;



	public bool timeCounter;

	//public TextAsset notefile;
	[XmlRoot("NoteCollection")]
	public class NoteCollection{
		[XmlAttribute("NoteTime")]
		public float NoteTime;
		[XmlAttribute("Letter")]
		public string Letter;
		[XmlAttribute("Lane")]
		public int Lane;
		[XmlElementAttribute("Hold")]
		public bool Hold;

		public static NoteCollection Load(string path)
		{
			var serializer = new XmlSerializer(typeof(NoteCollection));
			using (var stream = new FileStream(path, FileMode.Open))
			{
				return serializer.Deserialize(stream) as NoteCollection;
			}
		}
	}



	//public var noteCollection;


	//TextAsset noteData = Resources.Load ("Misc/note-set") as TextAsset;

	/*
	void getXML()
	{
		FileStream ReadFileStream = new FileStream(@"C:\Users\Sean-Li\Desktop\Audiacle\Assets\Misc\note-set.xml", FileMode.Open, FileAccess.Read, FileShare.Read);

	}
	*/

	// Use this for initialization
	void Start () 
	{

		//GameObject.Find ("Spawner").GetComponent<SpawnerScript>().Spawn(laneList);
		//Debug.Log(spawnTime);

	}


	
	// Update is called once per frame
	void Update () 
	{

		globalTimer += Time.deltaTime;
		checkTimer += Time.deltaTime;

		var noteCollection = NoteCollection.Load(Path.Combine(Application.dataPath, "noteDoc.xml"));

		XmlDocument noteDocument = new XmlDocument();
		noteDocument.Load(Path.Combine(Application.dataPath, "noteDoc.xml"));
		XmlNodeList letterList = noteDocument.GetElementsByTagName("Letter");
		XmlNodeList laneList = noteDocument.GetElementsByTagName("Lane");
		XmlNodeList holdList = noteDocument.GetElementsByTagName("Hold");
		XmlNodeList noteTimeList = noteDocument.GetElementsByTagName("NoteTime");


		//NoteTimer = float.Parse (noteDocument.InnerText);

		NoteTimer = noteDocument.SelectSingleNode ("NoteCollection/Note[1]/Letter").Value;
		Debug.LogError (NoteTimer);

		//timing
		TimeString = noteTimeList[0].InnerXml;
		FirstNoteSpawnTime = XmlConvert.ToInt32(TimeString);
		//first note
		if(checkTimer >= FirstNoteSpawnTime)
		{
			//timeCounter = true;
			if (checkTimer >= NoteSpawnTime)
				{
				//spawn stuff
					for (int i=1 ; i < laneList.Count; i++)
					{
						laneNumberString = laneList[i].InnerXml;
						laneNumber = XmlConvert.ToInt32(laneNumberString);
						
						GameObject.Find ("Spawner").GetComponent<SpawnerScript>().Spawn(laneNumber);
						
						for (int j=1 ; j < letterList.Count; j++)
						{
							LetterString = letterList[i].InnerXml;
							Letter = XmlConvert.ToChar(LetterString);
							
							GameObject.Find ("Note(Clone)").GetComponent<NoteScript>().ChooseLetter(Letter);
							Debug.Log ("LetterOutput: " + letterList[i].InnerXml);
						}
						
						Debug.Log ("Laneoutput: " + laneList[i].InnerXml);
						//checkTimer = 0.0f;
						timeCounter = true;
					}
				}
			}

		if(timeCounter)
		{
			for(noteCount=1; noteCount<noteTimeList.Count; noteCount++)
			{
				TimeString = noteTimeList[noteCount].InnerXml;
				NoteSpawnTime = NoteSpawnTime + XmlConvert.ToInt32(TimeString);
				timeCounter = false;
			}
			
		}
	}
	
		///if(1.5f == 1.5f);
		//{
			//GameObject.Find ("Spawner").GetComponent<SpawnerScript>().Spawn(3);
			//GameObject.Find ("Note").GetComponent<NoteScript>().ChooseLetter(noteCollection.Letter);
			//Debug.LogError(noteCollection.Letter);
			//noteCollection.Letter
		//}
	
}

