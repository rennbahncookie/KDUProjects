﻿using UnityEngine;
using System.Collections;

public class HealthManagerScript : MonoBehaviour {
    public int HPTokenID;
    PlayerDataScript hpscript;
    public Sprite dead;
    public Sprite alive;
    private SpriteRenderer sr;
	// Use this for initialization
	void Start () {
        hpscript = GameObject.Find("PlayerData").GetComponent<PlayerDataScript>();
        sr = GetComponent<SpriteRenderer>();

       // GameObject.FindGameObjectWithTag("InputBox").GetComponent<SendInputOnSpace>().LoadDictionary();
	
	}
	
	// Update is called once per frame
	void Update () {
       
        if((hpscript.MaxLife/HPTokenID) >= hpscript.Life+1)
        //if((hpscript.MaxLife)/5 *HPTokenID >= hpscript.Life+1)
        {
            sr.sprite = dead;
        }
        else
        {
            sr.sprite = alive;
        }
	
	}
}
