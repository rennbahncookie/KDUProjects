﻿using UnityEngine;
using System.Collections;

public class ShowMenu : MonoBehaviour {
    public GameObject menu;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseOver()
    {
        menu.SetActive(true);
    }

    void OnMouseExit()
    {
        menu.SetActive(false);
    }
}
