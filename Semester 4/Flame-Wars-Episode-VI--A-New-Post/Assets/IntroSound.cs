﻿using UnityEngine;
using System.Collections;

public class IntroSound : MonoBehaviour {

    AudioSource ads;
    public Camera mainCamera;
    public AudioClip sounds;
    public AudioClip door;
    public AudioClip song;
    Animator anim;

    float time = 2.0f;
    float timer;

	// Use this for initialization
	void Start () {
        //mainCamera = GameObject.Find("mainCamera");
        ads = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();

	
	}
	
	// Update is called once per frame
	void Update () {
	
         if (anim.GetCurrentAnimatorStateInfo(0).IsName("DOne"))
         {
             timer += Time.deltaTime;

             if(timer > time)
             {
                 Application.LoadLevel("MainMenu");

             }

             
         }
	}   

    void PlayNotification()
    {
        AudioSource.PlayClipAtPoint(sounds, transform.position);
    }

    void PlayDoor()
    {
        AudioSource.PlayClipAtPoint(door, transform.position);
    }

    void PlaySong()
    {
        AudioSource.PlayClipAtPoint(song, transform.position);

    }
}
