﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyScoreScript : MonoBehaviour {
    public GameObject inOut;
    public int score;
    Text text;
	// Use this for initialization
	void Start () {
        score = inOut.GetComponent<SendInputOnSpace>().enemyScore;
        text = GetComponent<Text>();
	
	}
	
	// Update is called once per frame
	void Update () {
        score = inOut.GetComponent<SendInputOnSpace>().enemyScore;
        text.text = "Previous score to beat: " + score.ToString();
	
	}
}
