﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHealthScript : MonoBehaviour {
    public GameObject inOut;
    PlayerDataScript datascript;
    int score;
    Text text;
	// Use this for initialization
	void Start () {
        datascript = GameObject.Find("PlayerData").GetComponent<PlayerDataScript>();
        text = GetComponent<Text>();
	
	}
	
	// Update is called once per frame
	void Update () {
        score = datascript.Life;
        text.text = "Your HP: " + score.ToString();
	
	}
}
