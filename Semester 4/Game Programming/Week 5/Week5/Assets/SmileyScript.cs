﻿using UnityEngine;
using System.Collections;

public class SmileyScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown()
	{
		Debug.Log("OnMouseDown");
	}

	void OnMouseUp()
	{
		Debug.Log("OnMouseUp");
	}

	void OnMouseDrag()
	{
		Vector3 mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePos.z = 0.0f;

		transform.position = mousePos;

		Debug.Log("OnMouseDrag");
	}
}
