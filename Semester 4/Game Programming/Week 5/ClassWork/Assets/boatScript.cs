﻿using UnityEngine;
using System.Collections;

public class boatScript : MonoBehaviour {

	private float m_smileyHalfW;
	private float m_smileyHalfH;

	private SpriteRenderer m_spriteRenderer;
	private Sprite mySprite;

	float dirX = 0.1f;
	float dirY = 0.1f;

	public float speed = 0.1f;
	public float negativespeed = -0.1f;

	// Use this for initialization
	void Start () {

		m_spriteRenderer = gameObject.renderer as SpriteRenderer;
		mySprite = m_spriteRenderer.sprite;

		m_smileyHalfW = mySprite.bounds.size.x * 0.5f;
		m_smileyHalfH = mySprite.bounds.size.y * 0.5f;
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 WorldSize = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height,0.0f));
		float halfWidth = WorldSize.x;
		float halfHeight = WorldSize.y;

		Vector3 myPosition = transform.position;

		myPosition.x += dirX;

		if (myPosition.x >= halfWidth - m_smileyHalfW)
		{
			//set speed negative
			dirX = negativespeed;
			myPosition.x += dirX;
		}
		
		if (myPosition.y >= halfHeight - m_smileyHalfH)
		{
			//set speed negative 
			dirY = negativespeed;
			myPosition.y += dirY;
		}
		
		if (myPosition.y <= -halfHeight + m_smileyHalfH)
		{
			dirY = speed;
			myPosition.y += dirY;
		}
		if (myPosition.x <= -halfWidth + m_smileyHalfW)
		{
			dirX = speed;
			myPosition.x += dirX;
		}

		transform.position = myPosition;


	
	}

	public void changeDirection()
	{
		dirX = -dirX;
	}
}
