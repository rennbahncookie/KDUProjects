﻿using UnityEngine;
using System.Collections;

public class ClearScript : MonoBehaviour {

    public bool clear = false;
    SpriteRenderer sr;

    // Use this for initialization
    void Start()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        if (clear)
        {
            sr.color = Color.red;
        }
        else
        {
            sr.color = Color.white;
        }

    }

    void OnMouseDown()
    {
        clear = true;
    }
}
