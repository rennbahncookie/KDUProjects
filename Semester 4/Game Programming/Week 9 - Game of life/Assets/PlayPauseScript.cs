﻿using UnityEngine;
using System.Collections;

public class PlayPauseScript : MonoBehaviour {

    public bool play = false;
    SpriteRenderer sr;

	// Use this for initialization
	void Start () {
        sr = gameObject.GetComponent<SpriteRenderer>();
	
	}
	
	// Update is called once per frame
	void Update () {
        if(play)
        {
            
            sr.color = Color.red;
        }
        else
        {
            sr.color = Color.white;
        }
	
	}

    void OnMouseDown()
    {
        play = !play;
    }
}
