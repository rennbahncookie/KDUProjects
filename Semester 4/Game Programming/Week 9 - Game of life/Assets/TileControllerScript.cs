﻿using System.Collections;
using UnityEngine;

public class TileControllerScript : MonoBehaviour 
{
		
    public GameObject m_tilePrefab;
	private GameObject[,] m_tiles;
	private bool[,] m_tilesNext;

    private int m_playerX;
    private int m_playerY;
    private int m_cellNumX;
    private int m_cellNumY;
    private float m_cellSize;

    //Ints for States on all sides
    private bool stateUp;
    private bool stateDown;
    private bool stateRight;
    private bool stateLeft;
    private bool stateUpRight;
    private bool stateUpLeft;
    private bool stateDownRight;
    private bool stateDownLeft;
	

    TileScript playerScript;

	private float updateTimer;

    float timer;
    GameObject btnPlayPause;
    bool play = false;

    bool clear = false;
    GameObject btnClear;

    GameObject btnSwitchTime;
    bool toggle = false;
    float toggleTime = 0.03f;

    float posOffsetX = 3f;

    public bool sizeToggle;
    GameObject btnToggle;
    bool currentSize = false;

    void Start()
    {
        btnPlayPause = GameObject.Find("PlayPause");
        btnClear = GameObject.Find("Clear");
        btnSwitchTime = GameObject.Find("SwitchTime");
        btnToggle = GameObject.Find("Toggle");

        DrawArray(false);


    }

    void DrawArray(bool toggle)
    {

        if(toggle)
        {
            m_cellNumX = 80;
            m_cellNumY = 50;
            m_cellSize = 0.17f;

            float mapWidth = m_cellNumX * m_cellSize;
            float mapHeight = m_cellNumY * m_cellSize;

            m_tiles = new GameObject[m_cellNumX, m_cellNumY];
            m_tilesNext = new bool[m_cellNumX, m_cellNumY];


            int i, j;
            for (i = 0; i < m_cellNumX; i++)
            {
                for (j = 0; j < m_cellNumY; j++)
                {
                    Vector3 pos = new Vector3(i * m_cellSize - mapWidth * 0.5f, j * m_cellSize - mapWidth * 0.5f + posOffsetX, 0);
                    GameObject theObject = Instantiate(m_tilePrefab, pos, Quaternion.identity) as GameObject;
                    theObject.transform.parent = transform;

                    TileScript tileScript = (TileScript)theObject.GetComponent(typeof(TileScript));


                    m_tiles[i, j] = theObject;
                    m_tilesNext[i, j] = false;
                    tileScript.setState(m_tilesNext[i, j]);
                }
                timer = 0.0f;
            }
        }
        else
        {
            m_cellNumX = 30;
            m_cellNumY = 30;
            m_cellSize = 0.17f;

            float mapWidth = m_cellNumX * m_cellSize;
            float mapHeight = m_cellNumY * m_cellSize;

            m_tiles = new GameObject[m_cellNumX, m_cellNumY];
            m_tilesNext = new bool[m_cellNumX, m_cellNumY];


            int i, j;
            for (i = 0; i < m_cellNumX; i++)
            {
                for (j = 0; j < m_cellNumY; j++)
                {
                    Vector3 pos = new Vector3(i * m_cellSize - mapWidth * 0.5f, j * m_cellSize - mapWidth * 0.5f, 0);
                    GameObject theObject = Instantiate(m_tilePrefab, pos, Quaternion.identity) as GameObject;
                    theObject.transform.parent = transform;

                    TileScript tileScript = (TileScript)theObject.GetComponent(typeof(TileScript));


                    m_tiles[i, j] = theObject;
                    m_tilesNext[i, j] = false;
                    tileScript.setState(m_tilesNext[i, j]);
                }
                timer = 0.0f;
            }
        }

    }

    public void ToggleSize()
    {
        for (int i = 0; i < m_cellNumX; i++)
        {
            for (int j = 0; j < m_cellNumY; j++)
            {
                GameObject theObject = m_tiles[i, j];
                if(theObject != null)
                {
                    TileScript tileScript = (TileScript)theObject.GetComponent(typeof(TileScript));
                    tileScript.Kill();
                }
            }
        }
        currentSize = !currentSize;
        DrawArray(currentSize);
    }

	public void Clear()
	{
		for (int i = 0; i < m_cellNumX; i++)
        {
            for (int j = 0; j < m_cellNumY; j++)
            {
                m_tilesNext[i, j] = false;
                playerScript = (TileScript)m_tiles[i, j].GetComponent(typeof(TileScript)); 
                playerScript.setState(m_tilesNext[i, j]);           
            }
                   
        }
        clear = false;
        btnClear.GetComponent<ClearScript>().clear = false;
        SetNextState();	
	}




    public void Update()
    {
        clear = btnClear.GetComponent<ClearScript>().clear;
        if (clear)
        {
            Clear();
        }

        toggle = btnSwitchTime.GetComponent<SwitchTimeScript>().toggle;
        if (toggle)
        {
            toggleTime = 0.2f;
        }
        else
        {
            toggleTime = 0.03f;
        }

        sizeToggle = btnToggle.GetComponent<ToggleScript>().toggleSize;
        if(sizeToggle)
        {
            ToggleSize();
            btnToggle.GetComponent<ToggleScript>().toggleSize = false;
        }

        play = btnPlayPause.GetComponent<PlayPauseScript>().play;

        if (play)
        {
            timer += Time.deltaTime;
            if (timer >= toggleTime)
            {
                timer = 0.0f;

                // Loop through every cell on the grid.
                for (int i = 0; i < m_cellNumX; i++)
                {
                    for (int j = 0; j < m_cellNumY; j++)
                    {
                        bool living = m_tiles[i, j].GetComponent<TileScript>().getState();
                        int count = CheckSurroundings(i, j);
                        bool result = false;
                    //     Debug.Log("I'm here!");
                        if (living && count < 2)
                            result = false;
                        if (living && (count == 2 || count == 3))
                            result = true;
                        if (living && count > 3)
                            result = false;
                        if (!living && count == 3)
                            result = true;

                        m_tilesNext[i, j] = result;

                    }
                }
                SetNextState();
            }
        }
    }

    public int CheckSurroundings(int x, int y)
    {
        int count = 0;

		// Check cell on the right.
		if (x != m_cellNumX - 1)
			if (m_tiles[x + 1, y].GetComponent<TileScript>().getState())
				count++;

		// Check cell on the bottomw right.
		if (x != m_cellNumX - 1 && y != m_cellNumY - 1)
			if (m_tiles[x + 1, y + 1].GetComponent<TileScript>().getState())
				count++;

		// Check cell on the bottom.
		if (y != m_cellNumY - 1)
			if (m_tiles[x, y + 1].GetComponent<TileScript>().getState())
				count++;

		// Check cell on the bottom left.
		if (x != 0 && y != m_cellNumY - 1)
			if (m_tiles[x - 1, y + 1].GetComponent<TileScript>().getState())
				count++;

		// Check cell on the left.
		if (x != 0)
			if (m_tiles[x - 1, y].GetComponent<TileScript>().getState())
				count++;

		// Check cell on the top left.
		if (x != 0 && y != 0)
			if (m_tiles[x - 1, y - 1].GetComponent<TileScript>().getState())
				count++;

		// Check cell on the top.
		if (y != 0)
			if (m_tiles[x, y - 1].GetComponent<TileScript>().getState())
				count++;

		// Check cell on the top right.
		if (x != m_cellNumX - 1 && y != 0)
			if (m_tiles[x + 1, y - 1].GetComponent<TileScript>().getState())
				count++;

		return count;


    //     #endregion

 
            
    }   

	public void SetNextState()
	{
		for (int i = 0; i < m_cellNumX; i++)
        {
            for (int j = 0; j < m_cellNumY; j++)
            {
                m_tiles[i, j].GetComponent<TileScript>().setState(m_tilesNext[i, j]);
            }

        }

	}
	}
