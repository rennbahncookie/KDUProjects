﻿using UnityEngine;
using System.Collections;

public class SquareExplosion : MonoBehaviour {

	private ParticleSystem m_particleSystem = null;
	
	// Use this for initialization
	void Start () {
		m_particleSystem = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(m_particleSystem.IsAlive() == false)
		{
			Destroy(gameObject);
		}
	}
}
