﻿using UnityEngine;
using System.Collections;

public class levelGenerator : MonoBehaviour {

	public GameObject m_spikePrefab;
	public GameObject m_squarePrefab;

	private PlayerController m_playerScript;
	private float m_squareDefaultY = -2.0f;
	private float m_spikeDefaultY = -2.02f;

	private float m_currentDistance = 0.0f;
	private float m_nextSpawnDistance = 0.0f;

	// Use this for initialization
	void Start () {
	
		GameObject player = GameObject.Find("/player");
		m_playerScript = (PlayerController) player.GetComponent(typeof(PlayerController));
	}
	
	// Update is called once per frame
	void Update ()
	{
		m_currentDistance += m_playerScript.m_speed * Time.deltaTime;

		if (m_currentDistance >= m_nextSpawnDistance)
		{
			float dice = Random.value;
			// 1 spike
			if(dice <= 0.6f)
			{
				Vector3 pos = new Vector3(8.0f, m_spikeDefaultY, 0.0f);
				GameObject newObject = Instantiate(m_spikePrefab, pos, Quaternion.identity) as GameObject;

				m_nextSpawnDistance = m_currentDistance + Random.value*8.0f + 1.2f;
			}
			// 2 spikes
			else
			{
				Vector3 pos = new Vector3(8.0f, m_spikeDefaultY, 0.0f);
				GameObject newObject1 = Instantiate(m_spikePrefab, pos, Quaternion.identity) as GameObject;

				pos.x += 0.6f;
				GameObject newObject2 = Instantiate(m_spikePrefab, pos, Quaternion.identity) as GameObject;

				m_nextSpawnDistance = m_currentDistance + Random.value*8.0f + 2.2f;
			}
		

		}

		//Debug.Log ("current distance : " + m_currentDistance);
	}
}
