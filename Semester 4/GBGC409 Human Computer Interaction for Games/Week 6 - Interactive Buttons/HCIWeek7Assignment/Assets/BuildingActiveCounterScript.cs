﻿using UnityEngine;
using System.Collections;

public class BuildingActiveCounterScript : MonoBehaviour {

	public GameObject Background;
	ReloadScript reloadScript;


	// Use this for initialization
	void Start () {
		Background = GameObject.Find("Background");
		reloadScript = Background.GetComponent<ReloadScript>();
		
		reloadScript.numberOfActive++;

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
