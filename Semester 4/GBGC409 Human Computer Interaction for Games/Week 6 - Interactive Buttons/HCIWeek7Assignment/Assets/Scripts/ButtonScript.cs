﻿using UnityEngine;
using System.Collections;

public class ButtonScript : MonoBehaviour {

	public Sprite Up;
	public Sprite Pressed;
	public Sprite Hover;
	public Sprite Disabled;
	public Sprite Active;

	bool hover = false;
	bool pressed = false;
	bool up = false;
	bool activebutton = false;

	public float cooldowntime = 2.0f;
	public float currenttime = 0.0f;

	SpriteRenderer sr;
	Animator animator;

	public AudioSource[] sounds;
	public AudioSource noise1;
	public AudioSource noise2;
	public AudioSource noise3;
	bool hasDoorPlayed;

	float doordelaycurrent;
	public float doordelay = 0;

	public enum ButtonState
	{
		Disabled = 0,
		Enabled,
		Active
	}

	public ButtonState Button = ButtonState.Enabled;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		animator.enabled = !animator.enabled;
		Button = ButtonState.Enabled;

		bool hasDoorPlayed = false;

		sr = GetComponent<SpriteRenderer> ();

		sounds = GetComponents<AudioSource>();
		noise1 = sounds[0];
		noise2 = sounds[1];
		noise3 = sounds[2];
	
	}
	
	// Update is called once per frame
	void Update () {
		if(activebutton)
		{
			currenttime += Time.deltaTime;
			if(currenttime >= cooldowntime)
			{
				Disable ();
				//activebutton = false;
				sr.sprite = Disabled;
				activebutton = false;
				currenttime = 0.0f;
			}

		}


		if(Button != ButtonState.Active && Button != ButtonState.Disabled)
		{
			if(Button == ButtonState.Enabled)
			{
				if(up)
				{
					noise1.Stop();
					sr.sprite = Up;
				}
				if (hover)
				{
					sr.sprite = Hover;

					if(!noise1.isPlaying)
					{
						noise1.Play();
					}

					

					
					
					if(pressed)
					{
						sr.sprite = Pressed;
						noise1.Stop();
						
						
						ActivateButton();
					}
				}
			}
		}



		if(Button == ButtonState.Disabled)
		{
			doordelaycurrent += Time.deltaTime;

			if(doordelay <= doordelaycurrent)
			{
				if(!hasDoorPlayed)
				{
					if(!noise3.isPlaying)
					{
						noise3.Play();
					}
					hasDoorPlayed = true;
				}
			}



			sr.sprite = Disabled;
			animator.enabled = false;
			Button = ButtonState.Disabled;
			//Button = ButtonState.Enabled;
			//GameObject.Find("smiley").GetComponent<SwitchScript>().ToggleOff();

		}


	}

	void OnMouseOver()
	{
		up = false;
		hover = true;
	}

	void OnMouseDown()
	{
		if(Button != ButtonState.Disabled)
		{
			Debug.Log ("MouseDownButton");
			up = false;
			pressed = true;
			hover = false;
		}

	}

	 void OnMouseUp()
	{
		if(Button != ButtonState.Disabled)
		{
			up = false;
			hover = false;
			pressed = true;
			activebutton = true;
		}
	}

	void OnMouseExit()
	{
		up = true;
		hover = false;
		pressed = false;
	}

	public void Enable()
	{
		Button = ButtonState.Enabled;
		up = true;
		hover = false;
		pressed = false;
	}

	public void Disable()
	{
		Button = ButtonState.Disabled;
		sr.sprite = Disabled;
		noise2.Stop();

	}

	void ActivateButton()
	{
		if(activebutton)
		{

			currenttime = 0.0f;
			Button = ButtonState.Active;
			//sr.sprite = Active;		
			animator.enabled = true;
			if(!noise2.isPlaying)
			{
				noise2.Play();
			}

		}
	}
	
	
}
