﻿using UnityEngine;
using System.Collections;



public class CameraScript : MonoBehaviour {

	float currentTime;
	float waitTime;
	float waitTime2;
	
	public Vector3 target;
	Vector3 firstPos;
	Vector2 velocity;
	Vector3 direction;
	int numberOfActiveHere;

	public float smoothTime = 0.3f;
	// Use this for initialization
	void Start () {
		target = new Vector3(0f, -4.44f, -19f);
		firstPos = transform.position;

		direction = (target - firstPos);

		waitTime = 2f;
		waitTime2 = 6f;
	
	}
	
	// Update is called once per frame
	void Update () {

		GameObject reload = GameObject.Find("Background");

		numberOfActiveHere = reload.GetComponent<ReloadScript>().numberOfActive;

		if(numberOfActiveHere >= 3)
		{
			currentTime += Time.deltaTime;
			if(currentTime >= waitTime)
			{
				if(transform.position.y > -4.44f)
				{
					transform.Translate(direction * 0.65f * Time.deltaTime);
				}
				
				if(transform.position.y <= -4.44f)
				{
					transform.position = target;
				}
				
				if(camera.orthographicSize > 0.97f)
				{
					camera.orthographicSize += -Time.deltaTime * 3f;
				}
				
				if(camera.orthographicSize <= 0.97f)
				{
					camera.orthographicSize = 0.97f;
				}
				
				if(camera.orthographicSize <= 0.97f && transform.position.y <= -4.44f)
				{
					GetComponent<AudioSource>().enabled = true;
					if(currentTime >= waitTime2)
					{
						Application.LoadLevel(Application.loadedLevel);
					}

				}
			}
		
		}


	
	}
}
