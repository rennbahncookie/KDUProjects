#pragma once
#include <string>
using namespace std;

class AItems
{
	string name;

public:
	AItems(string n) : name(n)
	{

	}
	string GetName()
	{
		return name;
	}
};

class Chainsaw : public AItems
{
public:
	Chainsaw() : AItems("Chainsaw"){}
};

class Shotgun : public AItems
{
public:
	Shotgun() : AItems("Shotgun"){}
};

class Baseballbat : public AItems
{
public:
	Baseballbat() : AItems("Baseballbat"){}
};

class LeatherGloves: public AItems
{
public:
	LeatherGloves() : AItems("LeatherGloves"){}
};

class GunHolster : public AItems
{
public:
	GunHolster() : AItems("GunHolster"){}
};
