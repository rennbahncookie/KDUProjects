#pragma once 
#include "Employee.h"

Manager::Manager()
{
	CreateAndAddBenefit(Car());
	CreateAndAddBenefit(Computer());
	CreateAndAddBenefit(Salary());
	CreateAndAddBenefit(CellPhone());
	CreateAndAddBenefit(Allowance());


}

Engineer::Engineer()
{
	CreateAndAddBenefit(Car());
	CreateAndAddBenefit(Computer());
	CreateAndAddBenefit(Salary());
	CreateAndAddBenefit(CellPhone());
	CreateAndAddBenefit(Allowance());
	CreateAndAddBenefit(Laptop());
}