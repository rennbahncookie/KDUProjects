#ifndef _MUSICBOX_H
#define _MUSICBOX_H

#include "Observer.h"
#include "Trigger.h"
#include <iostream>

using namespace std;

class MusicBox : public Observer
{
protected:
	//! Updated function called when 
	//! subject has notification event
	void update(Subject* subject)
	{
		bool triggerIsOn = ((Trigger*)subject)->getState();
		if (triggerIsOn)
			cout << "Music Box is playing music!" << endl;
		else
			cout << "Music Box is not player music!" << endl;

		Trigger* trigger = (Trigger*)subject;
		trigger->setState(!trigger->getState());
	}
};

#endif

