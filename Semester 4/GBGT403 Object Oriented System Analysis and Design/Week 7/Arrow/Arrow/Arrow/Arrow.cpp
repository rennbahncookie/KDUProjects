// Arrow.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <windowsx.h>

using namespace std;

class Arrow
{
protected:
	string description;
	int weight;
public:
	Arrow(string name, int weight)
		:description(name), weight(weight){}

	virtual void printArrow(int level) = 0;
};

class StandardArrow : public Arrow
{
public:
	StandardArrow(string name, int weight)
		:Arrow(name, weight){}
	void printArrow(int level)
	{
		for (int j = 0; j < level; j++)
			cout << "\t";
		cout << description << ", weight : " << weight << " units" << endl;
	}
};

class FireArrow :public Arrow
{
private:
	vector<Arrow*> children;
public:
	FireArrow(string descrption, int weight)
		:Arrow(description, weight)
	{}

	void add(Arrow *cmp)
	{
		children.push_back(cmp);
	}

	void printArrow(int level)
	{
		for (int j = 0; j < level; ++j) cout << "\t";
		cout << "FireArrow : " << this->description << "weight: " << weight << endl;

		if (!children.empty())
		{
			for (int x = 0; x < level; ++x)
				cout << "\t";
				cout << "Subarrows of " << description << " : " << endl;
			++level;
			for (int i = 0; i < children.size(); i++)
			{
				children[i]->printArrow(level);
			}
		}
	}
};

class ArrowBundle : public Arrow
{

};


int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}

