// Observer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MusicBox.h"
#include "DualSwitchLight.h"
#include <iostream>
#include <windows.h>
using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	Trigger trigger1;
	Trigger trigger2;

	MusicBox mBox;

	DualSwitchLight mLight();
	
	

	trigger1.AddObserver(&mBox);


	bool isExit = true;

	do
	{
		char input;
		cin >> input;

		if (input == '1')
		{
			trigger1.setState(true);
			isExit = true;
		}
		else if (input == '2')
		{
			trigger1.setState(false);
			isExit = true;
		}
		else if (input == '3')
		{
			//mLight();
			isExit = true;
		}
		else if (input == '4')
		{
			//mLight();
			isExit = true;
		}

	} while (!isExit);


	system("PAUSE");
	return 0;
}

