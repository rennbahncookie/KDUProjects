#pragma once
#include <vector>
#include <iostream>
#include "AItems.h"

using namespace std;

class AEmployee
{
public:
	vector<AItems> benefits;
	enum EMP_TYPE {ENGINEER , MANAGER };
	void CreateAndAddBenifit(AItems ben);
	void PrintBenefits();
	static AEmployee * CreateEmployee(EMP_TYPE empType);


};
