#pragma once
#include "AEmployee.h"
#include "AItems.h"
#include "Employee.h"

AEmployee* AEmployee::CreateEmployee(EMP_TYPE empTYP)
{
	if (empTYP == EMP_TYPE::ENGINEER)
	{
		return new  Engineer();

	}
	else if (empTYP == EMP_TYPE::MANAGER)
	{

		return new Manager();
	}

}

void AEmployee::CreateAndAddBenifit(AItems ben)
{
	benefits.push_back(ben);

}
void AEmployee::PrintBenefits()
{
	for (vector<AItems>::iterator iter = benefits.begin(); iter != benefits.end(); ++iter)
	{
		cout << (*iter).GetName() << endl;

	}

}
