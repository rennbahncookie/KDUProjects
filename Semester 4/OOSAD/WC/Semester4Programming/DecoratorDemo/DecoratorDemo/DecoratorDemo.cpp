// DecoratorDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Decorator.h"

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	Sandwich *sandwich = new CheeseDecorator(new SubDecorator(new ItalianBread()));

	cout << "Your sandwich is " << sandwich->getDesc() << endl;
	cout << "Price is RM " << sandwich->getCost() << endl;

	sandwich = new VegeDecorator(new SubDecorator(sandwich));
	sandwich = new SauceDecorator(new SubDecorator(sandwich));

	cout << "Your sandwich is " << sandwich->getDesc() << endl;
	cout << "Price is RM " << sandwich->getCost() << endl;


	

	system("PAUSE");
	return 0;
}

