// VisitorDemo.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Node.h"
#include <iostream>
#include <Windows.h>

using namespace std;

class PrintHierarchyVisitor : public ActorVisitor
{
	void visit(Actor* actor)
	{
		Node* parent = actor->mOwner;
		int indentCount = 0;
		while (parent)
		{
			++indentCount;
			parent = parent->getParent();
		}
		for (int i = 0; i < indentCount; i++)
		{
			cout << " ";
		}
		cout << actor->mName << endl;
	}
	void GetRoot()
	{

	}
	void GetTotalElement()
	{

	}
};

class PrintVertHierarchy : public ActorVisitor
{
	void visit(Actor* actor)
	{

	}

};

int _tmain(int argc, _TCHAR* argv[])
{
	//!X(Root)
	//!- A (dog)
	//!X(Child)
	//! -A (Bone)
	//!	-A (Hat)
	//!X(Child2)
	//!	-A (Cat)
	Node root, child, child2, child3, child4;
	Actor dog("Dog"), bone("Bone"), hat("Hat"), cat("Cat"), fish("Fish"), wolf("Wolf");

	root.appendActor(&dog);
	root.addChild(&child);
	root.addChild(&child2);
	child.appendActor(&bone);
	child.appendActor(&hat);
	child2.appendActor(&cat);

	child2.addChild(&child3);
	child3.appendActor(&fish);
	
	child.addChild(&child4);
	child4.appendActor(&wolf);


	PrintHierarchyVisitor visitor;
	root.accept(&visitor);
	system("pause");
	return 0;
}

