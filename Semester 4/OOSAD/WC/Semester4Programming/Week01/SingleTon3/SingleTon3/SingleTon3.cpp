// SingleTon3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "windows.h"

using namespace std;

class SingleTon
{
private:
	static bool instanceFlag;
	static SingleTon *single;
	SingleTon()
	{
		//!private constructor

	}
public:
	static SingleTon *getInstance();
	void method();
	~SingleTon()
	{
		instanceFlag = false;
		
	}

	
};

bool SingleTon::instanceFlag = false;
SingleTon *SingleTon::single = NULL;
SingleTon *SingleTon::getInstance()
{
	if (!instanceFlag)
	{
		single = new SingleTon();
		instanceFlag = true;
		return single;
	}
	else
	{
		return single;
	}
	

}

void SingleTon::method()
{
	cout << "Method of the singleTon class" << endl;
}



int _tmain(int argc, _TCHAR* argv[])
{
	SingleTon *s1, *s2;
	s1 = SingleTon::getInstance();
	s1->method();
	s2 = SingleTon::getInstance();    
	s2->method();
	

	system("PAUSE");
	return 0;
}

