// SingleTon2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "windows.h"

using namespace std;

//!Singleton
class GlobalClass
{
	int value;
	//! static instance variable

	static GlobalClass * instance;
	//! private constructor
	GlobalClass(int v = 0)
	{
		value = v;

	}
public:
	int getValue()
	{
		return value;

	}
	void setValue(int v)
	{
		value = v;
	}
	static GlobalClass *getInstance()
	{
		if (!instance)
		{
			instance = new GlobalClass;
		}
		return instance;

	}
};

GlobalClass *GlobalClass::instance = 0;

void foo(void)
{
	GlobalClass::getInstance()->setValue(1);
	cout << "foo: instance value is " << GlobalClass::getInstance()->getValue() << endl;

}
void boo(void)
{
	GlobalClass::getInstance()->setValue(2);
	cout << "boo: instance value is " << GlobalClass::getInstance()->getValue()<<endl;

}
int _tmain(int argc, _TCHAR* argv[])
{
	
	cout << "main : instance value is " << GlobalClass::getInstance() -> getValue() << endl;
	foo();
	boo();
	
	system("PAUSE");
	
	return 0;
		
}

