#pragma once
#include <string>
#include <iostream>
#include "windows.h"

using namespace std;

class ArmorList
{
	string name;
public:
	ArmorList(string j) :name(j)
	{

	}

	string GetName()
	{
		return name;
	}
};

class Shield : public ArmorList

{

public:
	Shield() : ArmorList("Shield"){}


};

class Armor : public ArmorList

{

public:
	Armor() : ArmorList("Armor"){}


};

class Goggle : public ArmorList

{

public:
	Goggle() : ArmorList("Night-Vision Goggle"){}


};

class Dog : public ArmorList

{

public:
	Dog() : ArmorList("Dog"){}


};
