#pragma once
#include <iostream>
#include <vector>
#include "ItemList.h"
#include "ArmorList.h"

using namespace std;

class PlayerList
{
public:
	vector<ItemList>weapon;
	vector<ArmorList>armor;
	enum PLAYER{ HUMAN, SOLDIER };
	void CreateWeapon(ItemList iList);
	void CreateArmor(ArmorList aList);
	void PrintWeapon();
	void PrintArmor();
	static PlayerList * CreatePlayer(PLAYER choosePlayer);


};