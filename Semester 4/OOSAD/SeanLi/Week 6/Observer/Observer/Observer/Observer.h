#ifndef _OBSERVER_H
#define _OBSERVER_H

class Observer
{
	friend class Subject;
protected:
	virtual void update(Subject* subject) = 0;
};

#endif