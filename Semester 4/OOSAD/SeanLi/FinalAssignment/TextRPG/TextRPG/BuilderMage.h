#ifndef BUILDERMAGE_H
#define BUILDERMAGE_H

#include "PlayerClass.h"
#include "PlayerBuilder.h"

using namespace std;

class BuilderMage : public PlayerBuilder
{
public:
	void bStrength()
	{
		player->setStrength(0);
	}
	void bDexerity()
	{
		player->setDexterity(0);
	}
	void bIntelligence()
	{
		player->setIntelligence(0);
	}
	void bHealth()
	{
		player->setHealth(100);
	}
	void bMana()
	{
		player->setMana(100);
	}
	void bDmgPhysical()
	{
		player->setDmgPhysical(10);
	}
	void bDmgMagical()
	{
		player->setDmgMagical(70);
	}
	void bEvadeChance()
	{
		player->setEvadeChance(10);
	}
	void bDefense()
	{
		player->setDefense(30);
	}
	void bExperience()
	{
		player->setExperience(0);
	}
	void bLevel()
	{
		player->setLevel(0);
	}
	void bName()
	{
		player->setName("Mage");
	}
	void bSkillPoints()
	{
		player->setSkillPoints(10);
	}
};

#endif