#ifndef EQUIPMENTDECORATOR_H
#define EQUIPMENTDECORATOR_H

#include <iostream>
#include <string>
#include "windows.h"
#include "WeaponFactory.h"

using namespace std;

class EQ
{
public:
	virtual double getWeight() = 0;
	virtual string getDesc() = 0;
	virtual int getDef() = 0;
	virtual int getHealth() = 0;
};

class Shoes : public EQ
{
public:
	string getDesc()
	{
		return "Shoes";
	}
	double getWeight()
	{
		return 3;
	}
	int getDef()
	{
		return 5;
	}
	int getHealth()
	{
		return 10;
	}
};

class Pants : public EQ
{
public:
	string getDesc()
	{
		return "Pants";
	}
	double getWeight()
	{
		return 5;
	}
	int getDef()
	{
		return 7;
	}
	int getHealth()
	{
		return 15;
	}
};

class Shirt : public EQ
{
public:
	string getDesc()
	{
		return "Shirt";
	}
	double getWeight()
	{
		return 5;
	}
	int getDef()
	{
		return 7;
	}
	int getHealth()
	{
		return 10;
	}
};

class Headgear : public EQ
{
public:
	string getDesc()
	{
		return "Headgear";
	}
	double getWeight()
	{
		return 2;
	}
	int getDef()
	{
		return 3;
	}
	int getHealth()
	{
		return 5;
	}
};


class SubDecorator : public EQ
{
	EQ *eq;
public:
	SubDecorator(EQ *eqRef)
	{
		eq = eqRef;
	}
	string getDesc()
	{
		return eq->getDesc();
	}
	double getWeight()
	{
		return eq->getWeight();
	}
	int getHealth()
	{
		return eq->getHealth();
	}
	int getDef()
	{
		return eq->getDef();
	}
};

class HealthDecorator : public SubDecorator
{
private:
	int extraHP;
	string extraHP_desc()
	{
		return "+ HP Crystal ";
	}
public:
	HealthDecorator(EQ *eq): SubDecorator(eq)
	{
		extraHP = 20;
	}
	string getDesc()
	{
		return SubDecorator::getDesc().append(extraHP_desc());
	}
	int getHealth()
	{
		return SubDecorator::getHealth() + extraHP;
	}
	int getDef()
	{
		return SubDecorator::getDef();
	}
	double getWeight()
	{
		return SubDecorator::getWeight();
	}
};

class DefDecorator : public SubDecorator
{
private:
	int extraDef;
	string extraDef_desc()
	{
		return "+ DEF Crystal ";
	}
public:
	DefDecorator(EQ *eq) : SubDecorator(eq)
	{
		extraDef = 20;
	}
	string getDesc()
	{
		return SubDecorator::getDesc().append(extraDef_desc());
	}
	int getDef()
	{
		return SubDecorator::getDef() + extraDef;
	}
	int getHealth()
	{
		return SubDecorator::getHealth();
	}
	double getWeight()
	{
		return SubDecorator::getWeight();
	}
};

#endif