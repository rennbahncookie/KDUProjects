// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "AEmployee.h"
#include <Windows.h>

int _tmain(int argc, _TCHAR* argv[])
{
	AEmployee *emp =
		AEmployee::CreateEmployee(AEmployee::EMP_TYPE::MANAGER);
	cout << "Receivable for Manager" << endl;
	emp->PrintBenefits();
	cout << endl;
	delete emp;

	emp =
		AEmployee::CreateEmployee(AEmployee::EMP_TYPE::ENGINEER);
	cout << "Receivable for Engineer" << endl;
	emp->PrintBenefits();
	cout << endl;
	system("PAUSE");
	return 0;
}
