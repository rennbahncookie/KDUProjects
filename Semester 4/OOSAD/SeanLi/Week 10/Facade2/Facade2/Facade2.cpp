// Facade2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <Windows.h>

using namespace std;

class Customer
{
private:
	string name;
public: Customer(string cname) : name(cname)
{}
		string& getName()
		{
			return name;
		}
};

class Bank
{
public:
	bool hasSufficientSavings(Customer c, int amount)
	{
		cout << "Check bank for" << c.getName() << endl;
		return true;
	}
};

class Credit
{
public:
	bool hasGoodCredit(Customer c, int amount)
	{
		cout << "Check credit for" << c.getName() << endl;
		return true;
	}
};

class Loan
{
public:
	bool hasGoodCredit(Customer c, int amount)
	{
		cout << "Check loan for" << c.getName() << endl;
		return true;
	}

};

// Facade
class Mortgage
{
private:
	Bank myBank;
	Loan myLoan;
	Credit myCredit;
public:
	bool isEligible(Customer cust, int amount)
	{
		cout << cust.getName() << " applies for a loan for RM" << amount << endl;
		bool eligible = true;
		eligible = myBank.hasSufficientSavings(cust, amount);
		if (eligible)
			eligible = myLoan.hasGoodCredit(cust, amount);
		if (eligible)
			eligible = myCredit.hasGoodCredit(cust, amount);

		return eligible;
	}
};


int _tmain(int argc, _TCHAR* argv[])
{
	Mortgage mortgage;
	Customer customer("Sam Kong");
	bool eligible = mortgage.isEligible(customer, 100000);
	cout << "\n" << customer.getName() << " has been " << (eligible?"Approved" : "Rejected") << endl;
	system("PAUSE");
	return 0;
}

