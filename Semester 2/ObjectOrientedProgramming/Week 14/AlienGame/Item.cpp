#include "DefaultHeaderFiles.h"
Item::Item(ITEM_TYPE type)
{
    int randomNumber = rand();
    int randX = (randomNumber % 21) + 5;
    x = randX;
    y = 5;
    row = 3;
    col = 3;
    speedX = 1;
    speedY = 1;
    this->type = type;
    char symbol = ' ';
    if(type == HEALTH_PACK)
    {
        color = LIGHTRED;
        symbol = 'H';
    }
    else if(type == DUAL_PACK)
    {
        color = LIGHTGREEN;
        symbol = 'D';
    }
    else if(type == TRIPLE_PACK)
    {
        color = LIGHTMAGENTA;
        symbol = 'T';
    }
    updateDelayTimer = 0;
    updateDelayDuration = 120;
    char tempSprite[3][3] =
    {
        {(char)201,(char)203,(char)187},
        {(char)199,symbol,(char)182},
        {(char)200,(char)202,(char)188},
    };
    sprite = new char*[row];
    for (int i = 0; i< row; i++)
    {
        sprite[i] = new char [col];
        for(int j = 0; j<col; j++)
        {
            sprite[i][j] = tempSprite[i][j];
        }
    }
}

void Item::Update(int elapsedTime)
{
    updateDelayTimer += elapsedTime;
    if(updateDelayTimer < updateDelayDuration)
    {
        return;
    }
    updateDelayTimer %= updateDelayDuration;

    MoveDown();

    if(CheckOverBoundary())
    {
        isToBeDestroyed = true;
    }
}
