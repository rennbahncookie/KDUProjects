#include "DefaultHeaderFiles.h"
//! highscore

//!define several game variables
int startTime = 0;
int currentTime = 0;
int elapsedTime = 0;
int lastUpdateTime = 0;
bool isExit = false;

//! define a player object
Player player;
vector<Enemy*> enemyList;
vector<SpawnInfo> spawnList;
vector<Bullet> playerBulletList;
vector<Bullet> enemyBulletList;

vector<Item> itemList;
vector<SpawnInfo> spawnItemList;

void DrawHealth()
{
    if(player.health<7)
    {
        textcolor(LIGHTRED);
    }
    else if(player.health >=7 && player.health<=13)
    {
        textcolor(YELLOW);
    }
    else if(player.health>13)
    {
        textcolor(LIGHTGREEN);
    }
    if(player.health<=0 && player.lives>0)
    {
        player.health = 20;
        player.lives --;
    }
    for(int i=0; i<player.health; i++)
    {
        cout << char(210);
    }
    for(int i=0; player.health+i<20; i++)
    {
        cout << " ";
    }
    textcolor(WHITE);
}

void DrawBorder()
{
    for(int i = BOUNDARY_LEFT; i < BOUNDARY_RIGHT; i++)
    {
        gotoxy(i, BOUNDARY_TOP);
        cout << char(205);
        gotoxy(i, BOUNDARY_BOTTOM);
        cout << char(205);
    }
    for(int j = BOUNDARY_TOP; j < BOUNDARY_BOTTOM; j++)
    {
        gotoxy(BOUNDARY_LEFT, j);
        cout << char(186);
        gotoxy(BOUNDARY_RIGHT,j);
        cout << char(186);
    }
    //! corners
    gotoxy(BOUNDARY_LEFT,BOUNDARY_TOP);
    cout << char(201);
    gotoxy(BOUNDARY_LEFT,BOUNDARY_BOTTOM);
    cout << char(200);
    gotoxy(BOUNDARY_RIGHT,BOUNDARY_TOP);
    cout << char(187);
    gotoxy(BOUNDARY_RIGHT,BOUNDARY_BOTTOM);
    cout << char(188);

}
void DrawHUD()
{
    gotoxy(BOUNDARY_LEFT, BOUNDARY_TOP - 1);
    cout << "                             ";
    gotoxy(BOUNDARY_LEFT, BOUNDARY_TOP - 2);
    cout << "Score : " << player.score;
    gotoxy(BOUNDARY_LEFT + 30, BOUNDARY_TOP - 2);
    cout << "                             ";
   // gotoxy(BOUNDARY_LEFT + 50, BOUNDARY_TOP - 2);
 //   cout << "Bullet : " << playerBulletList.size();

    gotoxy(BOUNDARY_LEFT + 35, BOUNDARY_TOP - 2);
    cout << "Health : " ,DrawHealth();
}
void SpawnEnemy(ENEMY_TYPE type)
{
    Enemy* tempEnemy;
    if(type == ENEMY_01)
    {
        tempEnemy = new Enemy01();
    }
    else if (type == ENEMY_02)
    {
        tempEnemy = new Enemy02();
    }
    else if (type == ENEMY_03)
    {
        tempEnemy = new Enemy03();
    }
    enemyList.push_back(tempEnemy);
}

void SpawnItem(ITEM_TYPE type)
{
    if(type == HEALTH_PACK)
    {
        Item tempItem(HEALTH_PACK);
        itemList.push_back(tempItem);
    }
    else if(type == DUAL_PACK)
    {
        Item tempItem(DUAL_PACK);
        itemList.push_back(tempItem);
    }
    else if(type == TRIPLE_PACK)
    {
        Item tempItem(TRIPLE_PACK);
        itemList.push_back(tempItem);

    }
}

void AddItem(ITEM_TYPE type, int time)
{
    SpawnInfo tempSpawnInfo;
    tempSpawnInfo.itemType = type;
    tempSpawnInfo.time = time;
    tempSpawnInfo.isSpawned = false;
    spawnItemList.push_back(tempSpawnInfo);
}

//! set the type of enemy and the time
void AddEnemy(ENEMY_TYPE type, int time)
{
    SpawnInfo tempSpawnInfo;
    tempSpawnInfo.type = type;
    tempSpawnInfo.time = time;
    tempSpawnInfo.isSpawned = false;
    spawnList.push_back(tempSpawnInfo);
}


void AddEnemyPattern01(int timeOffset)
{
    AddEnemy(ENEMY_01,timeOffset +600);
    AddEnemy(ENEMY_02,timeOffset +600);
    AddEnemy(ENEMY_03,timeOffset +600);
    AddItem(HEALTH_PACK,timeOffset+600);

}

void AddEnemyPattern02(int timeOffset)
{
    AddEnemy(ENEMY_01,timeOffset +600);
    AddEnemy(ENEMY_02,timeOffset +600);
    AddEnemy(ENEMY_03,timeOffset +600);

}

void AddEnemyPattern03(int timeOffset)
{
    AddEnemy(ENEMY_01,timeOffset +300);
    AddEnemy(ENEMY_01,timeOffset +600);
    AddEnemy(ENEMY_01,timeOffset +900);
    AddItem(DUAL_PACK,timeOffset +300);
}
void AddEnemyPattern04(int timeOffset)
{
    AddEnemy(ENEMY_03,timeOffset +300);
    AddEnemy(ENEMY_03,timeOffset +600);
    AddEnemy(ENEMY_03,timeOffset +900);
}
void AddEnemyPattern05(int timeOffset)
{
    AddEnemy(ENEMY_01,timeOffset +500);
    AddEnemy(ENEMY_02,timeOffset +500);
    AddEnemy(ENEMY_01,timeOffset +650);
    AddItem(TRIPLE_PACK,timeOffset + 550);
}

void AddEnemyPattern06(int timeOffset)
{
    AddEnemy(ENEMY_03,timeOffset +500);
    AddEnemy(ENEMY_01,timeOffset +500);
    AddEnemy(ENEMY_03,timeOffset +650);
    AddItem(HEALTH_PACK,timeOffset +700);
}

void AddEnemyPattern07(int timeOffset)
{
    AddEnemy(ENEMY_03,timeOffset +500);
    AddEnemy(ENEMY_02,timeOffset +600);
    AddEnemy(ENEMY_03,timeOffset +600);
}

void AddEnemyPattern08(int timeOffset)
{
    AddEnemy(ENEMY_01,timeOffset +300);
    AddEnemy(ENEMY_03,timeOffset +800);
    AddEnemy(ENEMY_01,timeOffset +500);
}

void AddEnemyPattern09(int timeOffset)
{
    AddEnemy(ENEMY_02,timeOffset +300);
    AddEnemy(ENEMY_01,timeOffset +800);
    AddEnemy(ENEMY_02,timeOffset +500);
}

void AddEnemyPattern10(int timeOffset)
{
    AddEnemy(ENEMY_01,timeOffset +950);
    AddEnemy(ENEMY_02,timeOffset +950);
    AddEnemy(ENEMY_03,timeOffset +500);

}

void InitializeSpawnList()
{
    for(int i=0; i<200; i++)
    {
        int randType = rand()%10;
        if(randType == 0)
        {
            AddEnemyPattern01(1500*i);
        }
        if(randType == 1)
        {
            AddEnemyPattern02(1500*i);
        }
        if(randType == 2)
        {
            AddEnemyPattern03(1500*i);
        }
        if(randType == 3)
        {
            AddEnemyPattern04(1500*i);
        }
        if(randType == 4)
        {
            AddEnemyPattern05(1500*i);
        }
        if(randType == 5)
        {
            AddEnemyPattern06(1500*i);
        }
        if(randType == 6)
        {
            AddEnemyPattern07(1500*i);
        }
        if(randType == 7)
        {
            AddEnemyPattern08(1500*i);
        }
        if(randType == 8)
        {
            AddEnemyPattern09(1500*i);
        }
        if(randType == 9)
        {
            AddEnemyPattern10(1500*i);
        }
    }

}

void UpdateSpawn()
{
    for(int i=0; i<spawnList.size(); i++)
    {
        if(spawnList[i].isSpawned == true)
        {
            continue;
        }
        if(currentTime - startTime > spawnList[i].time)
        {
            SpawnEnemy(spawnList[i].type);
            spawnList[i].isSpawned = true;
        }
    }
}

void UpdateItemSpawn()
{
    for(int i=0; i<spawnItemList.size(); i++)
    {
        if(spawnItemList[i].isSpawned == true)
        {
            continue;
        }
        if(currentTime - startTime > spawnItemList[i].time)
        {
            SpawnItem(spawnItemList[i].itemType);
            spawnItemList[i].isSpawned = true;
        }
    }
}

void Shoot(bool isPlayer, GameObject& object)
{
    BULLET_TYPE tempBulletType;
    vector<Bullet>* tempBulletList;
    bool tempIsReverseDirection = false;

    if(isPlayer)
    {
        tempBulletType = ((Player*)&object)->bulletType;
        tempBulletList = &playerBulletList;
        player.enableShooting = false;
    }
    else
    {
        tempBulletType = ((Enemy*)&object)->bulletType;
        tempBulletList = &enemyBulletList;
        ((Enemy*)&object)->enableShooting = false;
        tempIsReverseDirection = true;
    }
    if(tempBulletType == STRAIGHT)
    {
        Bullet tempBullet;
        tempBullet.x = object.x + 1;
        tempBullet.y = object.y;
        tempBullet.direction = NORTH;
        tempBullet.isReverseDirection = tempIsReverseDirection;
        tempBulletList->push_back(tempBullet);
    }
    else if(tempBulletType == DUAL)
    {
        Bullet tempBullet;
        tempBullet.x = object.x + 2;
        tempBullet.y = object.y;
        tempBullet.direction = NORTH;
        tempBullet.isReverseDirection = tempIsReverseDirection;
        tempBulletList->push_back(tempBullet);

        Bullet tempBullet02;
        tempBullet02.x = object.x -2;
        tempBullet02.y = object.y;
        tempBullet02.direction = NORTH;
        tempBullet02.isReverseDirection = tempIsReverseDirection;
        tempBulletList->push_back(tempBullet02);
    }
    else if(tempBulletType == TRIPLE)
    {
        Bullet tempBullet;
        tempBullet.x = object.x + 1;
        tempBullet.y = object.y;
        tempBullet.direction = NORTH;
        tempBullet.isReverseDirection = tempIsReverseDirection;
        tempBulletList->push_back(tempBullet);

        Bullet tempBullet02;
        tempBullet02.x = object.x -2;
        tempBullet02.y = object.y;
        tempBullet02.direction = NORTH;
        tempBullet02.isReverseDirection = tempIsReverseDirection;
        tempBulletList->push_back(tempBullet02);

        Bullet tempBullet03;
        tempBullet03.x = object.x + 2;
        tempBullet03.y = object.y;
        tempBullet03.direction = NORTH;
        tempBullet03.isReverseDirection = tempIsReverseDirection;
        tempBulletList->push_back(tempBullet03);
    }

    //player.enableShooting = false;
}

bool CheckBoundingBoxCollision(GameObject a, GameObject b)
{
    if(a.x + a.col < b.x ||
       a.x > b.x + b.col ||
       a.y + a.row < b.y ||
       a.y > b.y + b.row)
    {
        return false;
    }
}
void CheckCollision()
    return true;
{
    //! enemy vs bullet
    for(int i=1; i<enemyList.size(); i++)
    {
        for(int j=0; j<playerBulletList.size(); j++)
        {
            if(CheckBoundingBoxCollision(*enemyList[i],playerBulletList[j]))
            {
                enemyList[i]->isToBeDestroyed = true;
                playerBulletList[j].isToBeDestroyed = true;
                player.score += 10;
            }
        }
    }
    //! enemy vs player

    //! enemy bullet vs player

    //! player vs items
}
int main()
{
    //! console window size.
    system("mode con: cols=80 lines=30");
    srand(time(NULL)); //! time for random generator
    DrawBorder();
    InitializeSpawnList();

    //! set the start time to the current clock tick
    startTime = clock();

    //! set the current time to current clock tick
    do{
        currentTime = clock();
        UpdateSpawn();
        UpdateItemSpawn();

        player.Update(elapsedTime);

        if(player.enableShooting)
        {
            Shoot(true, player);
        }

        for(int i=0; i< enemyList.size(); i++)
        {
            enemyList[i]->Update(elapsedTime);
            if(enemyList[i]->enableShooting)
            {
                Shoot(false, *enemyList[i]);
            }
        }
        for (int i=0; i<itemList.size(); i++)
        {
            itemList[i].Update(elapsedTime);
        }
        for(int i=0; i<playerBulletList.size(); i++)
        {
            playerBulletList[i].Update(elapsedTime);
        }
        for(int i=0; i<enemyBulletList.size(); i++)
        {
            enemyBulletList[i].Update(elapsedTime);
        }

        CheckCollision();
        player.Draw();
        //DrawHUD();

        for(int i=0; i<enemyList.size(); i++)
        {
            if(enemyList[i]-> isToBeDestroyed)
            {
                enemyList[i]->DrawTrail();
                enemyList.erase(enemyList.begin()+i);
                i--;
            }
            else
            {
                enemyList[i]->Draw();
            }
        }
        for(int i=0; i<playerBulletList.size(); i++)
        {
            if(playerBulletList[i].isToBeDestroyed)
            {
                playerBulletList[i].DrawTrail();
                playerBulletList.erase(playerBulletList.begin() +i);
                i--;
            }
            else
            {
                playerBulletList[i].Draw();
            }
        }
        for(int i=0; i<enemyBulletList.size(); i++)
        {
            if(enemyBulletList[i].isToBeDestroyed)
            {
                enemyBulletList[i].DrawTrail();
                enemyBulletList.erase(enemyBulletList.begin() +i);
                i--;
            }
            else
            {
                enemyBulletList[i].Draw();
            }
        }

        for(int i=0; i<itemList.size(); i++)
        {
            if(itemList[i].isToBeDestroyed)
            {
                itemList[i].DrawTrail();
                itemList.erase(itemList.begin() +i);
                i--;
            }
            else
            {
                itemList[i].Draw();
            }
        }
        //! exit
        if(GetAsyncKeyState(VK_ESCAPE))
        {
            isExit = true;
        }

        DrawHUD();
        elapsedTime = (clock() - lastUpdateTime)*1000 /
        CLOCKS_PER_SEC;
        lastUpdateTime = clock();
        Sleep(25);


    } while(!isExit);
    cout << endl;

    system("PAUSE");
    return 0;
}
