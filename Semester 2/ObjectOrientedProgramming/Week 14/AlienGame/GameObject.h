#include "DefaultHeaderFiles.h"
//! function header & class declaration here
//! super class

class GameObject
{
    public:
    //!attribute or properties
    int x, y, xOld, yOld, row, col, speedX, speedY, color;
    //! represents the 2D sprite array
    //!     x
    //!     x->
    //!     x

    //! 2D sprite array
    char** sprite;
    //! check visibility
    bool isToBeDestroyed;
    //! timer
    int updateDelayTimer;
    int updateDelayDuration;

    int health;
    int lives;

    //!constructor
    GameObject();
    //!function

    void MoveLeft();
    void MoveRight();
    void MoveUp();
    void MoveDown();

    void DrawTrail();
    void Draw();

    //! boundry collision detection
    bool CheckOverBoundary();
    //! pure virtual function
    //! -> no need to include the function body

    virtual void Update(int elapsedTime);
};
