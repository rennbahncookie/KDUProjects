#include "DefaultHeaderFiles.h"

class Item: public GameObject
{
public:
    bool isInTheScreen;
    ITEM_TYPE type;
    Item(ITEM_TYPE type);
    void Update(int elapsedTime);
};
