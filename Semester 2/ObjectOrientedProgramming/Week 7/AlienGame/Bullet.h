#include "DefaultHeaderFiles.h"

class Bullet : public GameObject
{
    public:

    DIRECTION direction;

    //! constructor
    Bullet();

    void Update(int elapsedTime);
};
