#include "DefaultHeaderFiles.h"
//! highscore

//!define several game variables
int startTime = 0;
int currentTime = 0;
int elapsedTime = 0;
int lastUpdateTime = 0;
bool isExit = false;

//! define a player object

Player player;

void DrawBorder()
{
    for(int i = BOUNDARY_LEFT; i < BOUNDARY_RIGHT; i++)
    {
        gotoxy(i, BOUNDARY_TOP);
        cout << char(205);
        gotoxy(i, BOUNDARY_BOTTOM);
        cout << char(205);
    }
    for(int j = BOUNDARY_TOP; j < BOUNDARY_BOTTOM; j++)
    {
        gotoxy(BOUNDARY_LEFT, j);
        cout << char(186);
        gotoxy(BOUNDARY_RIGHT,j);
        cout << char(186);
    }
    //! corners
    gotoxy(BOUNDARY_LEFT,BOUNDARY_TOP);
    cout << char(201);
    gotoxy(BOUNDARY_LEFT,BOUNDARY_BOTTOM);
    cout << char(200);
    gotoxy(BOUNDARY_RIGHT,BOUNDARY_TOP);
    cout << char(187);
    gotoxy(BOUNDARY_RIGHT,BOUNDARY_BOTTOM);
    cout << char(188);


}

int main()
{
    //! console window size.
    system("mode con: cols=80 lines=30");
    srand(time(NULL)); //! time for random generator
    DrawBorder();

    //! set the start time to the current clock tick
    startTime = clock();

    //! set the current time to current clock tick
    do{
        currentTime = clock();

        player.Update(elapsedTime);
        player.Draw();
        //DrawHUD();

        elapsedTime = (clock() - lastUpdateTime)*1000 /
        CLOCKS_PER_SEC;
        lastUpdateTime = clock();
        Sleep(75);

    } while(!isExit);
    cout << endl;

    system("PAUSE");
    return 0;
}
