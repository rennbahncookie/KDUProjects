#include "random.h"
#include <string>
#include <iostream>

using namespace std;

class Player
{
private:
    int mTotal;
    float mMoney;
    float mWager;
    string mName;
    int mRetract;
    //! Static attributes
    static int wins;
    static int nGame;
    static int nPush;
public:
    Player();
    void start();
    int deal(int);
    int game(int);
    void rest();
    //! static
    static int gamesWin(int);
    static int numberGames(int);
    static int numberPush(int);
    //! setter and getter
    float getMoney();
    void setMoney(float);
    float getWager();
    void setWager(float);
    int getTotal();
    void setTotal(int);
    void viewStats();
    int getRetract();
    void setRetract(int);
    int gamesWin();
    int numberGames();
    int numberPush();

};

