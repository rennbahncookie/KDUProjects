#include "DefaultHeaderFiles.h"

class Player :  public GameObject
{
    public:
    bool enableShooting;
    bool disableShooting;
    int score;

    int shootDelayTimer;
    int shootDelayDuration;

    BULLET_TYPE bulletType;

    int weaponTemp;
    int weaponTempMax;
    int cooldownTimer;
    int cooldownDuration;
    int cooldownDelayTimer;
    int cooldownDelayDuration;

    //! Constructor
    Player();

    //! update function prototype
    void Update(int elapsedTime);
};
