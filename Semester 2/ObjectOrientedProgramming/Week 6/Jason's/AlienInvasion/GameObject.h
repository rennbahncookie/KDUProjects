//! header file -> handle the class declaration

class GameObject
{
    //! attribute or properties
    int x, y, xOld, yOld, row, col, speedX, speedY, colour;
    //! represent the 2-D sprite array
    char** sprite;
    //! to check the visibility of each object
    bool isToBeDestroy;
    //! timer
    int updateDelayTimer;
    int updateDelayDuration;

    //! constructor
    GameObject();
    //! function

    void MoveLeft();
    void MoveRight();
    void MoveUp();
    void MoveDown();

    void DrawTrail();
    void Draw();
    //! check the boundary collision detection
    bool CheckOverBoundary();

    virtual void Update(int elapsedTimed)= 0;

};
