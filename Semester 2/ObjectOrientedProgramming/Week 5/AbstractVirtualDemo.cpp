#include <iostream>
#include <string>
#include "math.h"

using namespace std;
//! super class with virtual function
class GeometricObject
{
private:
    string color;
public:
    GeometricObject(string c):
        color(c)
    {}
    //! pure virtual function
    virtual double calArea() = 0;

};

class Rectangle : public GeometricObject
{
private:
    int height;
    int width;
public:
    Rectangle(string c, int h , int w) :
        GeometricObject(c) , height(h), width(w)
        {}
        //! implement the virtual method
        double calArea()
        {
            return height*width;
        }
};

class Circle : public GeometricObject
{
private:
    int radius;
public:
    Circle(string c, int r) :
        GeometricObject(c) , radius(r)
        {}
        double calArea()
        {
            return radius*radius*M_PI;
        }
};

class Cylinder : public GeometricObject
{
private:
    int radius;
    int height;
public:
    Cylinder(string c, int h, int r) :
        GeometricObject(c) , radius(r), height(h)
        {}
        double calArea()
        {
            return radius*radius*M_PI*height;
        }
};


int main ()
{
    //! Geomertic * g1 = new GeometricObject("yellow); <- virtual = doesn't work

    Rectangle * r1 = new Rectangle("yellow", 10, 5);
    double resultr1 = r1 -> calArea();
    cout << "Rectangle Area is: \t" << resultr1 << "cm" << endl;

    Circle * c1 = new Circle ("blue", 10);
    double resultc1 = c1 -> calArea();
    cout << "Circle Area is: \t" << resultc1 << "cm^2" << endl;

    Cylinder* y1 = new Cylinder ("blue", 5, 10);
    double resulty1 = y1 -> calArea();
    cout << "Cylinder Volume is: \t" << resulty1 << "cm^3" << endl;

//! my code
    cout << endl;
    int arr [3] = {resultr1, resultc1, resulty1};
    int n, result = 0;

    for (int i = 3 - 1; i>= 0; i--)
    {
        cout << "Area/Volume is " << arr[i] << endl;
    }

//! jason's code
    cout << endl << endl;
    GeometricObject* gArray [3];
    gArray[0] = r1;
    gArray[1] = c1;
    gArray[2] = y1;

    for (int i = 0; i < 3; i++)
    {
        cout << "Area/Volume is " << gArray[i] -> calArea() << endl;
    }

    return 0;
}
