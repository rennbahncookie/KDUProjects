#include "player.h"
#include "random.h"
#include <iostream>
#include <cstdlib>
#include <string>

using namespace std;

Player::Player()
{
    mTotal = 0;
    mMoney = 100.0f;
    mWager = 0.0f;
    mName = "Player";
    mRetract = 0;
}

void Player::start()
{
    cout<<"Black Jack"<<endl;
    cout<<"=========="<<endl;
    cout<<"Please enter your name: ";
    getline(cin, mName);
    cout<<"You start with RM"<< mMoney<<" !"<<endl;
}
int Player::deal(int value)
{
    int cards[2];
    int choice;
    cards[0] = Random(1,14);
    cards[1] = Random(1,14);
    value = cards[0] + cards[1];
    if(cards[0] == 12)
    {
        value -=2;
    }
    if(cards[1] == 12)
    {
        value -=2;
    }
    if(cards[0] == 13)
    {
        value -=3;
    }
    if(cards[1] == 13)
    {
        value -=3;
    }
    if(cards[0] == 14)
    {
        value -=4;
    }
    if(cards[1] == 14)
    {
        value -=4;
    }
    cout<<"\nCards: "<< cards[0]<<", "<<cards[1]<<endl;
    cout<<"Total: "<< value <<endl;
    if(cards[0]==11 || cards[1]== 11)
    {
        bool ok = false;
        do
        {
            cout<<"\nAce 1 or 11? ";
            cin>>choice;
            if(choice == 1)
            {
                value -= 10;
                ok = true;
            }
            else if(choice == 11)
            {
                ok = true;
            }
        }while(!ok);
    }
    return value;
}
int Player::game(int value)
{
    int card =0;
    int choice = 1;
    char redouble = 'n';
    if(value == 9 || value == 10 || value == 11)
    {
       cout<<"\nRedouble the bet? Y/N: ";
       cin>>redouble;
       redouble = toupper(redouble);
       if(redouble == 'Y')
       {
           setWager(getWager()*2);
           card = Random(1,11);
           if(card == 11)
           {
               bool ok = false;
               do
               {
                   cout<<"\n1 or 11?: ";
                   cin>>card;
                   if(card == 1 || card == 11)
                   {
                       ok = true;
                   }
               }while(!ok);
           }
           value += card;
           cout<<"\nYour card is: "<< card<<endl;
           cout<<"Your card Total is: "<< value<<endl;
           if(value >21)
           {
               cout<<"You bust!"<<endl;
               return value;
           }
       }
       else
       {
            do
            {
                cout<<"\nWould you like : "<<endl;
                cout<<"1) Hit"<<endl;
                cout<<"2) Stay"<<endl;
                cin>>choice;
                switch(choice)
                {
                case 1:
                    card = Random(1,14);
                    if(card == 11)
                    {
                       bool ok = false;
                       do
                       {
                           cout<<"\n1 or 11?: ";
                           cin>>card;
                           if(card == 1 || card == 11)
                           {
                               ok = true;
                           }
                       }while(!ok);
                    }
                    if(card == 12)
                    {
                        value -=2;
                    }
                    else if(card == 13)
                    {
                        value -=3;
                    }
                    else if(card == 14)
                    {
                        value -=4;
                    }
                    value += card;
                   cout<<"\nYour card is: "<< card<<endl;
                   cout<<"Your card Total is: "<< value<<endl;
                   if(value >21)
                   {
                       cout<<"You bust!"<<endl;
                       return value;
                   }
                   break;
                case 2:
                    break;
                default:
                    break;
                }
            }while(choice == 1);
       }
    }
    else
    {
        do
            {
                cout<<"\nWould you like : "<<endl;
                cout<<"1) Hit"<<endl;
                cout<<"2) Stay"<<endl;
                cin>>choice;
                switch(choice)
                {
                case 1:
                    card = Random(1,14);
                    if(card == 11)
                    {
                       bool ok = false;
                       do
                       {
                           cout<<"\n1 or 11?: ";
                           cin>>card;
                           if(card == 1 || card == 11)
                           {
                               ok = true;
                           }
                       }while(!ok);
                    }
                    if(card == 12)
                    {
                        value -=2;
                    }
                    else if(card == 13)
                    {
                        value -=3;
                    }
                    else if(card == 14)
                    {
                        value -=4;
                    }
                    value += card;
                   cout<<"\nYour card is: "<< card<<endl;
                   cout<<"Your card Total is: "<< value<<endl;
                   if(value >21)
                   {
                       cout<<"You bust!"<<endl;
                       return value;
                   }
                   break;
                case 2:
                    break;
                default:
                    break;
                }
            }while(choice == 1);
    }
    return value;
}
void Player::rest()
{
    cout<<"Resting ...."<<endl;
    mMoney = 100.0f;
}

int Player::gamesWin()
{
    return wins;
}
int Player::numberGames()
{
    return nGame;
}
int Player::numberPush()
{
    return nPush;
}

int Player::wins = 0;
int Player::nGame = 0;
int Player::nPush = 0;

    //!Set and get
float Player::getMoney()
{
    return mMoney;
}
void Player::setMoney(float money)
{
    mMoney += money;
}
float Player::getWager()
{
    return mWager;
}
void Player::setWager(float wager)
{
    mWager = wager;
}
int Player::getTotal()
{
    return mTotal;
}
void Player::setTotal(int total)
{
    mTotal = total;
}
void Player::viewStats()
{
    cout<<endl;
    cout<<"Player STATS"<<endl;
    cout<<"============"<<endl;
    cout<<"Name = "<< mName<<endl;
    cout<<"Games = " <<numberGames()<<endl;
    cout<<"Wins = "<< gamesWin()<<endl;
    cout<<"Lost = "<< numberGames() - gamesWin() - numberPush()<<endl;
    cout<<"Push = "<< numberPush()<<endl;
    cout<<"Money = "<< mMoney<<endl;
    cout<<"END OF PLAYER STATS"<<endl;
    cout<<"============"<<endl;
}
int Player::getRetract()
{
    return mRetract;
}

void Player::setRetract(int retract)
{
    mRetract = retract;
}
int Player::gamesWin(int n)
{
    wins += n;
    return wins;
}
int Player::numberGames(int n)
{
    nGame += n;
    return nGame;
}
int Player::numberPush(int n)
{
    nPush += n;
    return nPush;
}
