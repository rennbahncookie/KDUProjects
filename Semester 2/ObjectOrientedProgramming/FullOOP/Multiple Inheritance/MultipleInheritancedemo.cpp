#include <string>

using namespace std;

class Person
{
private:
    string name;
    int age;
    bool isMale;
public:
    Person(string n, int a, bool gender)
        : name(n), age(a), isMale(gender)
        {}
    string GetName(){ return name;}
    int GetAge(){ return age;}
    bool GetGender(){ return isMale;}
};

class Employee
{
private:
    string employer;
    double salary;
public:
    Employee(string emp, double s)
        : employer(emp), salary(s)
        {}
    string GetEmployer(){return employer;}
    double GetSalary(){return salary;}
};


//! Multiple Inheritance
class Teacher : public Person , public Employee
{
private:
    int grade;
public:
    Teacher(string n, int a, bool gender,string emp, double s,int g)
        :Person(n,a,gender), Employee(emp,s), grade(g)
        {}
};
