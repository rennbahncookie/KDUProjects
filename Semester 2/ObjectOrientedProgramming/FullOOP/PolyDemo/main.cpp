#include <iostream>\
#include "Employee.h"
#include "Manager.h"

using namespace std;

int main()
{
    Employee emp1("John",30.0);

    cout << "Name : "<<emp1.getName() << endl;
    cout << "Pay  : "<<emp1.pay(40.0) << endl;
    cout << endl;
    //! Name :  John
    //! Pay  :  1200.00
    Manager mng("David",5000.00, true);
    cout << "Name : "<<mng.getName() << endl;
    cout << "Pay  : "<<mng.pay(100.0) << endl;
    cout << endl;
    //! Name :  David
    //! Pay  :  500000.00

    //! array -> employee -> Manager
    //!                   -> Supervisor
    //!                   -> Worker
    Manager m1("John", 4000.00, true);
    Manager m2("David", 100.00, false);
    Manager m3("Sharon", 6000.00, true);
    //Super class pointer array
    Employee* employeeArray[3];
    employeeArray[0] = &m1;
    employeeArray[1] = &m2;
    employeeArray[2] = &m3;
    float hours = 0.0;
    for(int i=0; i<3; i++)
    {
        cout<<"Name : "<<employeeArray[i]->getName()<<endl;
        cout<<"Please enter your hours worked : ";
        cin>>hours;
        cout<<"Pay : "<<employeeArray[i]->pay(hours)<<endl;

    }
    //! Name :  John
    //! Please enter your hours worked : 100
    //! Pay  :  4000.00

    //! Name :  David
    //! Please enter your hours worked : 100
    //! Pay  :  10000.00

    //! Name :  Sharon
    //! Please enter your hours worked : 100
    //! Pay  :  6000.00
    return 0;
}
