#include <iostream>
#include <string>
#include <cmath>

using namespace std;
//!super class with virtual function
class GeometricObject
{
private:
    string color;
public:
    GeometricObject(const string c):
        color(c)
    {}
    //!pure virtual function
    virtual double calArea()=0;
};

class Rectangle : public GeometricObject
{
private:
    int height;
    int width;
public:

    Rectangle(const string c, int h, int w) :
        GeometricObject(c), height(h), width(w)
    {}
    //!implement the virtual method
    double calArea()
    {
        return height*width;
    }
};

class Circle : public GeometricObject
{
private:
    int radius;
public:
    Circle(const string c, int r) :
        GeometricObject(c), radius(r)
    {}

    double calArea()
    {
        return M_PI*radius*radius;
    }
};

int main()
{
   // Rectangle r1;
    Rectangle *r1 = new Rectangle("yellow",4,5);
   // double result = r1->calArea();
   // cout<<"Rectangle Area is " <<result<<endl;
    Circle *c1 = new Circle("Red",5);

   // cout<<"Circle Area is " <<c1->calArea()<<endl;
//! ---------------
//!   r1  |  c1
//! --------------
    GeometricObject* gArray [2];
    gArray[0] = r1;
    gArray[1] = c1;

    for (int i=0; i< 2; i++)
    {
        cout<<"Area is " << gArray[i]->calArea()<<endl;
    }
    return 0;
}
