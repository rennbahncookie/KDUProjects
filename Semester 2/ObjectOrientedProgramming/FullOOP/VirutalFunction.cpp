//! abstract class

#include <iostream>
using namespace std;

class Vehicle
{
public:
    int speed;
    //! virtual functions
    virtual void Move()
    {
        cout<<"Move at "<<speed<<" KMH"<<endl;
    }
};
//! car inherite from abstract Vehicle class
//! car class required to implement all the virutal function
//! from the super class
class Car : public Vehicle
{
private:
    int gear;
public:
    //! Override the parent's implementations -> move()
    void Move()
    {
        cout<<"Drive at "<<gear<<" KMH"<<endl;
    }
    void SetGear(int g)
    {
        gear = g;
    }
};

class Heli : public Vehicle
{
private:
    int fanSpeed;
public:
   /* void Move()
    {
        cout<<"Fly at "<<fanSpeed<<" KMH"<<endl;
    }*/
    void SetFanSpeed(int fs)
    {
        fanSpeed = fs;
    }
};

int main()
{
    Vehicle myVehicle;
    myVehicle.speed = 6;
    myVehicle.Move();

    Car myCar;
    myCar.SetGear(4);
    myCar.Move();

    Heli myHeli;
    myHeli.SetFanSpeed(5);
   // myHeli.Move();

    return 0;
}
