#include "DefaultHeaderFiles.h"

Enemy01::Enemy01()
{
    x = 15;
    y= 5;
    row = 3;
    col = 3;
    speedX = 1;
    speedY = 1;
    colour = GREEN;
    isInTheScreen = false;

    enableShooting = false;
    bulletType = STRAIGHT;

    updateDelayTimer = 0;
    updateDelayDuration = 60;

    char tempSprite[3][3] = {
                                {'x','1','x'},
                                {'\\','1','/'},
                                {' ','v',' '}
                            };
    sprite = new char*[row];
    for(int i=0; i<row; i++)
    {
        sprite[i] = new char[col];
        for(int j=0; j<col; j++)
        {
            sprite[i][j] = tempSprite[i][j];
        }
    }
}
void Enemy01::Update(int elapsedTime)
{
    updateDelayTimer += elapsedTime;
    shootDelayTimer += elapsedTime;

    if(updateDelayTimer < updateDelayDuration)
    {
        return;
    }
    updateDelayTimer %= updateDelayDuration;


    MoveRight();
    MoveDown();

    if(shootDelayTimer > shootDelayDuration)
    {
        enableShooting = true;
        shootDelayTimer = 0;
    }
}

