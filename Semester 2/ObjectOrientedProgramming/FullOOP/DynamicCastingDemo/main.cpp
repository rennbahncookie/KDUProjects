#include <iostream>
#include <exception>

using namespace std;

class BaseClass
{
    virtual void dummy(){}
};
class DerivedClass : public BaseClass
{
    int a;
};

int main()
{
    try
    {
        BaseClass *ptrA = new DerivedClass;
        BaseClass *ptrB = new BaseClass;
        DerivedClass *ptrC;
        //! type casting
        //! double x = 9.4;
        //! int y = (int)x;
        //! dynamic_cast
        ptrC = dynamic_cast<DerivedClass *>(ptrA);
        if(ptrC == 0)
        {
            cout<<"Null pointer on first cast-type"<<endl;
        }

        ptrC = dynamic_cast<DerivedClass *>(ptrB);
        if(ptrC == 0)
        {
            cout<<"Null pointer on second cast-type"<<endl;
        }
    }catch(exception& my_ex)
    {
        cout<<"Exception: "<<my_ex.what();
    }

    cout << "Hello world!" << endl;
    return 0;
}
