#include <iostream>
#include "windows.h"

using namespace std;

//! class declaration and definition
class Hero
{
    private:
    //! attributes or properties
    int hp;
    int mp;
    int attack;

    //! Constructor (Special function
    //! Constructor's name == Class name
    public:
    Hero()
    {
        hp=100;
        mp=200;
        attack=20;
        cout<<"Fire shall be your death!"<<endl;
    }

    //! Destructor -> a special funtion
    //! Destructor's name == class name
    ~Hero()
    {
        cout<<"Blurgh!"<<endl;
    }
    //! class function
    //! set or update the attribute's value
    void SetHP(int hp)
    {
        if(hp>100)
        {
            this->hp = 100;
        }
        else
        {
            this->hp = hp;
        }
    }
    //! to return the updated value
    int GetHP()
    {
        return hp;
    }

    void SetMP(int mp);
    int GetMP();

    void SetAttack(int attack)
    {
        this->attack = attack;
    }
    int GetAttack()
    {
        return attack;
    }
            //! myHero2
    void Attack(Hero *hero, int attackValue)
    {
        //!hero.hp = hero.hp - attackValue;
        hero->hp -= attackValue;
        cout<<hero->hp<<endl;
    }
};

void Hero::SetMP(int mp)
{
    this->mp = mp;
}
int Hero::GetMP()
{
    return mp;
}

int main()
{
    //! Construct Hero Objects
    //Hero myHero;
    //Hero myHero2;
    //! object pointer
    Hero *myHero = new Hero();
    Hero *myHero2 = new Hero();
    cout<<"Original HP for myHero: "<<myHero->GetHP()<<endl;
    //myHero.pp = 200;
    myHero->SetHP(60);
    cout<<"Updated HP for myHero: "<<myHero->GetHP()<<endl;
    cout<<"Original HP for myHero2: "<<myHero2->GetHP()<<endl;

    myHero->Attack(myHero2,myHero->GetAttack());
    myHero2->Attack(myHero,myHero->GetAttack());
    cout<<myHero->GetHP()<<endl;
    cout<<myHero2->GetHP()<<endl;

}
