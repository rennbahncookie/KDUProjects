#include <iostream>

using namespace std;

class Polygon
{
protected:
    int width, height;
public:
    void set_Value(int a, int b)
    {
        width = a;
        height= b;
    }
};

class Rectangle : public Polygon
{
public:
    int area()
    {
        return width*height;
    }
};

class Triangle : public Polygon
{
public:
    int area()
    {
        return (width*height) /2;
    }
};

int main()
{
    Rectangle rect;
    Triangle tri;
    //! declare parent class pointer
    Polygon *p1 = &rect;
    Polygon *p2 = &tri;

    p1 -> set_Value(4,5);
    p2 -> set_Value(2,3);

    cout << "Rectangle area: \t" << rect.area() << endl;
    cout << "Triangle area: \t\t" << tri.area() << endl;
    return 0;
}
