#include <string>
using namespace std;

class Employee
{
protected:
    string name;
    float payRate;
public:
    Employee(string n, float rate);
    string getName() const;
    float getPayRate() const;

    //!poly
    float pay(float hoursWork)const;

};
