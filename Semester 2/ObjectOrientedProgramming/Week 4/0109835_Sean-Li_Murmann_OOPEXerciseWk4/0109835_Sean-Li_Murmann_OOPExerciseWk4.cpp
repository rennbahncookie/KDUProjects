//============================================================================
// Name        : 0109835_Sean-Li_Murmann_OOPExerciseWk4.cpp
// Author      : Sean-Li Murmann
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
// IDE         : Eclipse
//============================================================================

#include <iostream>
#include "windows.h"
using namespace std;

class GameCharacter
{
protected:
	int totalhealth;
	int meleeDamage;

public:
	GameCharacter();
	void attack() const;
	void health() const;
};

GameCharacter::GameCharacter() :
		totalhealth(100),
		meleeDamage(10)
		{}

void GameCharacter::attack() const
{
	cout << "Basic attack deals " << meleeDamage << " damage!" << endl;
}

void GameCharacter::health() const
{
	cout << "This character has " << totalhealth <<" total health." << endl;
}

class Hero : public GameCharacter
{
public:
	float heroSpecialAttackMultiplier;
	float heroHealthMultiplier;

	Hero();
	void heroSpecialAttack();
	void heroHealth();
	int specialAttackValue();
};
Hero::Hero():

		heroSpecialAttackMultiplier(2),
		heroHealthMultiplier(2.5)
{}
void Hero::heroSpecialAttack()
{
	cout << "Melee attack deals " << (heroSpecialAttackMultiplier*meleeDamage);
	cout << " damage!" << endl;
}
void Hero::heroHealth()
{
	cout << "Hero has " << (totalhealth*heroHealthMultiplier) << " total health.";
	cout << endl;
}

class Human : public GameCharacter
{
public:
	float humanSpecialAttackMultiplier;
	float humanHealthMultiplier;

	Human();
	void humanSpecialAttack();
	void humanHealth();
	int specialAttackValue();
};
Human::Human():
		humanSpecialAttackMultiplier(0.5),
		humanHealthMultiplier(0.5)
{}
void Human::humanSpecialAttack()
{
	cout << "Human poke deals " << (humanSpecialAttackMultiplier*meleeDamage);
	cout << " damage!" << endl;
}
void Human::humanHealth()
{
	cout << "Human has " << (totalhealth*humanHealthMultiplier) << " total health.";
}

class Enemy : public GameCharacter
{
protected:
	int mDamage;

public:
	Enemy();
	void enemyAttack() const;
	int enemyAttackValue() const;
};

Enemy::Enemy() :
		mDamage(25)
{}

void Enemy::enemyAttack() const
{
	cout<<"Attack deals " <<mDamage<< " damage!"<<endl;
}

class Boss : public Enemy
{
public:
	int mDamageMultiplier;

	Boss();
	void SpecialAttack();
	int SpecialAttackValue();
};

Boss::Boss():
	mDamageMultiplier(3)
{}
void Boss::SpecialAttack()
{
	cout<<"Special Attack inflicts "<<(mDamageMultiplier*mDamage);
	cout<<" damage points!"<<endl;
}
int Boss::SpecialAttackValue()
{
	return mDamageMultiplier*mDamage;
}
class FinalBoss : public Boss
{
public:
	int mSpecialAttackMultiplier;
	FinalBoss();
	void MegaAttack();
};


FinalBoss::FinalBoss():
	mSpecialAttackMultiplier(10)
{}
void FinalBoss::MegaAttack()
{
	cout<<"Mega Attack inflicts "<<mSpecialAttackMultiplier*SpecialAttackValue();
	cout<<" damage points!"<<endl;
}


int main()
{
	cout << "Class test: GameCharacter" << endl;
	GameCharacter gameCharacter1;
	gameCharacter1.attack();
	gameCharacter1.health();
	cout << endl;

	cout << "Spawning Hero" << endl;
	Hero hero1;
	hero1.attack();
	hero1.heroSpecialAttack();
	hero1.heroHealth();
	cout << endl;

	cout <<"Spawning Human" << endl;
	Human human1;
	human1.attack();
	human1.humanSpecialAttack();
	human1.humanHealth();
	cout << endl;

	cout<<"Creating an Enemy"<< endl;
	Enemy enemy1;
	enemy1.attack();
	enemy1.enemyAttack();
	cout << endl;

	cout << "Creating a Boss" << endl;
	Boss boss1;
	boss1.attack();
	boss1.enemyAttack();
	boss1.SpecialAttack();
	cout << endl;

	cout << "Creating a FinalBoss" << endl;
	FinalBoss finalboss1;
	finalboss1.attack();
	finalboss1.enemyAttack();
	finalboss1.SpecialAttack();
	finalboss1.MegaAttack();
	cout << endl;

    system("PAUSE");
return 0;
}
