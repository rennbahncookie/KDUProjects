#include <iostream>

using namespace std;

class Enemy
{
private:
    int mDamage;
public:
    Enemy(int damage = 10);
    //! make virtual to be overriden
    void virtual Taunt () const;
    void virtual Attack() const;
};

Enemy::Enemy(int damage) :
    mDamage(damage)
{}

void Enemy::Taunt() const
{
    cout<<"The enemy says he will fight you!"<< endl;
}
void Enemy::Attack() const
{
    cout<<"Attack! Inflics "<<mDamage << " damage points."<<endl;
}
//! making the Boss Class inherit from Enemy super class
class Boss : public Enemy
{
public:
    Boss(int damage = 30);
    //! overriding the super class functions
    void Taunt () const;
    void Attack () const;
};
//! sub class constructor need to invoke the super class constructor
//! pass the damage value to the super class.
Boss::Boss(int damage) :
    Enemy(damage)
{}
//! overriding the super class virtual functions
void Boss::Taunt()const
{
    cout<<"The boss says he will end your pitiful life!"<<endl;
}
void Boss::Attack()const
{
    Enemy::Attack();
    cout<< "And laughs at you!"<<endl;
}


int main()
{
    cout << "Enemy Object" << endl;
    //!int x = 5
    //!creating /instantiate an object
    Enemy anEnemy;
    anEnemy.Taunt();
    anEnemy.Attack();

    cout << "Boss Object" << endl;
    Boss aBoss;
    aBoss.Taunt();
    aBoss.Attack();

    return 0;
}
