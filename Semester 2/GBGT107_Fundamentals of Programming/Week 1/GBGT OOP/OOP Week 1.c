#include <iostream>
#include "windows.h"

using namespace std;


//! Object Oriented-> class

class Enemy
{
//properties of the enemy > data member
    private:
    int power;
    int hp;

    public:

    Enemies(int h, int p)
    {
        hp = h;
        power = p;

    }


    ~Enemy()
    {

    }
    //class member functions

    int getHP()
    {
        return hp;
    }
    void setHp(int h)
    {
        hp = h;
}

int getPower()
{

    return power;

}
void setPower(int p)
{
    power = p;
}
};

int main()
{
    //! function to create enemy object
     int x;
    *Enemy e1, e2, e3;
    int h1, h2,p1, p2,h3 ,p3 ;
        cout<<"Please enter power and hp for Enemy 1"<<endl;
        cin>>p1>>h1;
        e1.setPower(p1);

        e1.setHP(h1); //sets only for E1 object

        cout<<"Please enter power and hp for Enemy 2"<<endl;
        cin>>p2>>h2;
        e2.setPower(p2);
        e2.setHp(h2);
        cout<<"Please enter power and hp for Enemy 3"<<endl;
        cin>>e3.power>>e3.hp;
        e3.setPower(p3);
        e3.setHp(h3);

        cout<<"Enemy 1 power is: "<<e1.getPower()<<endl;

        cout<<"Enemy 1 hp is: "<<e1.getHp()<<endl;

        cout<<"Enemy 2 power is: "<<e2.getPower()r<<endl;()

        cout<<"Enemy 2 hp is: "<<e2.getPower()<<endl;

          cout<<"Enemy 3 power is: "<<e3.power<<endl;

        cout<<"Enemy 3 hp is: "<<e3.hp<<endl;
}

*/


    int h4,p4;

        cout<<"Please enter power and hp for Enemy 4"<<endl;
        cin>>p4>>h4;
        Enemy *e4 = new Enemy(h4,p4);  //the asterisk means that e4 is an address, the pointers "->" below is to direct the address to the function
       cout<<"Enemy 4 power is: "<< e4->getPower()<<endl;
       cout<<"Enemy 4 hp is: "<< e4->getHp()<<endl;
