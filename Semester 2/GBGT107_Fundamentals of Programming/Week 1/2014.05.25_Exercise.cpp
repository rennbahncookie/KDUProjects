#include <iostream>

using namespace std;

int main()
{
    //Numbers to work with
  int number1, number2, number3;
  int smallest;
  int largest;

//Outputs + Calculations
  cout<<"Input three different integers: ";
  cin>> number1;
  cin>> number2;
  cin>> number3;

  smallest = number1;
  if ( number2 < smallest )
    smallest = number2;

  if ( number3 < smallest)
    smallest = number3;

  largest = number1;
  if ( number2 > largest )
    largest = number2;

  if ( number3 > largest)
    largest = number3;

  cin.ignore();
  cout<<"Sum is "<< (number1 + number2 + number3) <<"\n";
  cout<<"Average is "<< ((number1 + number2 + number3)/3) <<"\n";
  cout<<"Product is "<< (number1*number2*number3) <<"\n";
  cout<<"Smallest is "<< smallest <<"\n";
  cout<<"Largest is "<< largest <<"\n";

  cin.get();

//Square
for(int a=1;a<=10;a++)
        {
            cout<<"*";
        }
            cout<<endl;
for(int b=1;b<=5;b++)
    {
    for(int c=1;c<=1;c++)
        {
                cout<<"*";
        }
        for(int d=1;d<=8;d++)
              {
                cout<<" ";
              }
                cout<<"*"<<endl;
       }
for(int e=1;e<=10;e++)
 {
 cout<<"*";
 }
 cout<< " \n";


//DiamondShape

    int size;

  size = 4;

    int z=1;
  for ( int i=0; i<=size; i++)
  {
    for (int j=size; j>i; j--)
    {
      cout<<" "; // printing space here
    }

    cout<<"*";  // printing asterisk here

    if ( i>0)
    {
      for ( int k=1; k<=z; k++)
      {
        cout<<" ";
      }
      z+=2;
      cout<<"*";
    }
    cout<<endl; // end line similar  to \n
  }

  z-=4;

  for (int i=0; i<=size-1; i++)
  {
    for (int j=0; j<=i; j++)
    {
      cout<<" ";
    }

    cout<<"*";

    for (int k=1; k<=z; k++)
    {
      cout<<" ";
    }
    z-=2;

    if (i!=size-1)
    {
      cout<<"*";
    }
    cout<<endl;
  }
  cout<<"\n";


//Arrow

 int e=1;


  for(int a=1;a<=5;a++)
 {
    for(int b=4;b>=a;b--)
   {
    cout<<" ";  // Printing Space Here
   }
      for(int c=0;c<e;c++)
     {
       cout<<"*";  // Printing asterisk here
     }
      cout<<endl;   // new line
      e=e+2;
 }
 cout<<"    *\n    *\n    *\n    *\n    *\n    *\n\n\n";

 //Circle

int circle_radius = 6; // or whatever you want
    float console_ratio = 4.0/3.0;
    float a = console_ratio*circle_radius;
    float b = circle_radius;

    for (int y = -circle_radius; y <= circle_radius; y++)
    {
        for (int x = -console_ratio*circle_radius; x <= console_ratio*circle_radius; x++)
        {
            float d = (x/a)*(x/a) + (y/b)*(y/b);
            if (d > 0.90 && d < 1.1)
            {
                cout << "*";
            }
            else
            {
                 cout << " ";
            }
        }
        cout << endl;
    }

//Q3

  cout << "integer\t\tsquare\t\tcube\n";
  cout << 0 << "\t\t" << 0*0 << "\t\t" << 0*0*0 << "\n";
  cout << 1 << "\t\t" << 1*1 << "\t\t" << 1*1*1 << "\n";
  cout << 2 << "\t\t" << 2*2 << "\t\t" << 2*2*2 << "\n";
  cout << 3 << "\t\t" << 3*3 << "\t\t" << 3*3*3 << "\n";
  cout << 4 << "\t\t" << 4*4 << "\t\t" << 4*4*4 << "\n";
  cout << 5 << "\t\t" << 5*5 << "\t\t" << 5*5*5 << "\n";
  cout << 6 << "\t\t" << 6*6 << "\t\t" << 6*6*6 << "\n";
  cout << 7 << "\t\t" << 7*7 << "\t\t" << 7*7*7 << "\n";
  cout << 8 << "\t\t" << 8*8 << "\t\t" << 8*8*8 << "\n";
  cout << 9 << "\t\t" << 9*9 << "\t\t" << 9*9*9 << "\n";
  cout << 10 << "\t\t" << 10*10 << "\t\t" << 10*10*10 << "\n\n\n";


  int weight;
  float height,BMI;

    cout<<"Please enter your weight in kilograms: ";
    cin>>weight;
    cout<<"\nPlease enter your height in centimeters: ";
    cin>>height;

    BMI =weight*703/(height*height);
    cout<<"\nYour body mass Index is: "<<BMI;

    if (BMI>25)
    cout<<"\n\nYou are somewhat overweight.";
    else if (BMI<18.5)
    cout<<"\n\nYou are somewhat underweight.";
    else if (BMI>18.5 && BMI <25)
    cout<<"\n\nCongratulations! You are within a healthy weight range.";

    cin.get ();
    cin.get ();
    return 0;

 return 0;
}


