/*
 * CardDeck.h
 *
 *  Created on: Jul 12, 2014
 *      Author: Sean-Li Murmann
 */

//#ifndef CARDDECK_H_
//#define CARDDECK_H_

#include <iostream>
#include <string>
#include <vector>
#include "windows.h"
using namespace std;

//! card structure definition
struct Card {
	string face;
	string suit;
};

class CardDeck {
public:
	static const int numberOfCards = 52;
	static const int faces = 13;
	static const int suits = 4;
	CardDeck();
	void shuffle();
	void deal() const;
private:
	vector<Card> deck;
};

//#endif /* CARDDECK_H_ */
