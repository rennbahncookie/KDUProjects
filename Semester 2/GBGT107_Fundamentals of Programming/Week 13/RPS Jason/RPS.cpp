#include <iostream>
#include "windows.h"
using namespace std;

enum objectType{ROCK, PAPER, SCISSORS};

void displayRules();
objectType retrievePlay(char selection);
bool validSelection(char selection);
void convertEnum(objectType object);
objectType winningObject (objectType play1, objectType play2);
void gameResult(objectType play1, objectType Play2, int& winner);
void displayResults(int gCount, int wCount1, int wCount2);

int main()
{
 int gameCount;
 int winCount1;
 int winCount2;
 int gamewinner;
 char response;

 char selection1;
 char selection2;
 objectType play1;
 objectType play2;

 gameCount =0;
 winCount1=0;
 winCount2=0;

 displayRules();

 cout<<"Enter Y/y to play the game: ";
 cin>>response;
 cout<<endl;
 while(response == 'Y' || response == 'y')
 {
       cout<<"player 1 enter your choice: ";
       cin>>selection1;
       cout<<endl;

       cout<<"player 2 enter your choice: ";
       cin>>selection2;
       cout<<endl;

       if(validSelection(selection1) &&
              validSelection(selection2))
              {
               play1 = retrievePlay(selection1);
               play2 = retrievePlay(selection2);
               gameCount++;
               gameResult(play1, play2, gamewinner);

               if(gamewinner == 1)
                   winCount1++;
               else if (gamewinner == 2)
                    winCount2++;
              }
        cout<<"Enter Y/y to play the game: ";
        cin>>response;
        cout<<endl;
 }
 displayResults(gameCount, winCount1, winCount2);
 system("PAUSE");
 return 0;
}

void displayRules()
{
 cout<<" Welcome to the game of Rock, Paper, and Scissors."<<endl;
 cout<<"  This is a game for two players. For each game, each players selscts one of the object Rocks,"<<endl;
 cout<<" Paper, or Sciccors."<<endl;
 cout<<"The game is played according to the following rules:"<<endl;
 cout<<" -If both players choose the same object, this play is a tie."<<endl;
 cout<<" -If one player chooses rock and the other chooses scissors, the player choosing the rock wins this play because the rock breaks the scissors. "<<endl;
 cout<<" -If one player chooses rock and the other chooses paper, the player choosing the papers wins this play because the paper covers the rock. "<<endl;
 cout<<" -If one player chooses scissors and the other choose paper, the player choosing the scissors wins this play because the scissors cut the paper. "<<endl;
}

bool validSelection(char selection)
{
 switch(selection)
 {
   case 'R':
   case 'r':
   case 'P':
   case 'p':
   case 'S':
   case 's':
        return true;
   default:
        return false;
 }
}

objectType retrievePlay(char selection)
{
 objectType object;
 switch(object)
 {
   case 'R':
   case 'r':
        object = ROCK;
        break;
   case 'P':
   case 'p':
        object = PAPER;
        break;
   case 'S':
   case 's':
        object = SCISSORS;
        break;

 }
 return object;
}
void gameResult(objectType play1, objectType play2, int& winner)
{
 objectType winnerObject;
 cout << "Play1: " << play1;
 cout << "Play2: " << play2;
 if(play1 == play2)
 {
  winner = 0;
  cout<<"Both playes selected ";
  convertEnum(play1);
  cout<<". This game is a tie."<<endl;
 }
 else
 {
  winnerObject = winningObject(play1, play2);
  cout<<"PLAYER 1 selected ";
  convertEnum(play1);
  cout<<" and PLAYER 2 selected ";
  convertEnum(play2);

  if(play1 == winnerObject)
           winner =1;
  else if(play2 == winnerObject)
       winner = 2;

  cout<<"Player " << winner << " win the this game."<<endl;
 }
}

void convertEnum(objectType object)
{
 switch(object)
 {
  case ROCK:
       cout<<"Rock";
       break;
  case PAPER:
       cout<<"Paper";
       break;
  case SCISSORS:
       cout<<"Scissors";
       break;
 }
}
objectType winningObject(objectType play1, objectType play2)
{
 if((play1 == ROCK && play2 == SCISSORS) ||
           (play2 == ROCK && play1 == SCISSORS))
           return ROCK;
 else if((play1 == ROCK && play2 == PAPER) ||
           (play2 == ROCK && play1 == PAPER))
           return PAPER;
 else
     return SCISSORS;
}

void displayResults(int gCount, int wCount1, int wCount2)
{
 cout<<"The total number of plays: "<<gCount<<endl;
 cout<<"The number of plays won by player 1: "<< wCount1<<endl;
 cout<<"The number of plays won by player 2: "<< wCount2<<endl;
}
