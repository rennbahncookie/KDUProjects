#include <iostream>
#include <string>
using namespace std;

void printInfor(string[], int);

int main()
{
    const int sizeArray = 5;
    string npc[sizeArray] = {"Helper","Navigator","Translator","Police","Master"};

    printInfor(npc,sizeArray);
}

void printInfor(string npcName[], int sizeArray)
{
    npcName[3] = "Human";
    for(int i=0; i<sizeArray; i++)
    {
        cout << i+1 << " Category : " << npcName[i] << endl;
    }
}
