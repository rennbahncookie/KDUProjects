#include <iostream>
#include "windows.h"

using namespace std;

int main (){

    //! array size
    int num[10];
    //! assign value to array
    for (int i=0; i<10 ; i++)
    {
        num[i] = i + 1;

    }

    cout << "Position (index) \t"  << "Value" << endl;

    //! loop to display array items
        for (int i=0; i<10 ; i++)
    {
        cout << i << "\t\t\t" << num[i] << endl;
    }
}
