#include <iostream>
#include "windows.h"
#include <array>


using namespace std;


int main (){
    //! initalize num array
    //! array size
    int const arraySize = 10;
    int num[arraySize] = {12,95,23,54,65,76,45,78,49,18};
    int total = 0;
    cout << "Position (index) \t"  << "Value" << endl;

    //! loop to display array items
        for (int i=0; i<10 ; i++)
    {
        cout << i << "\t\t\t" << num[i] << endl;
        total += num[i];
    }
    cout << endl;
    cout << total << endl;
    cout << total/arraySize();
}
