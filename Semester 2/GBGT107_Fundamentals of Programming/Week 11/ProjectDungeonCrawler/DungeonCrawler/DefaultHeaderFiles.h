#ifndef _DEFAULT_HEADER_FILES_H_
#define _DEFAULT_HEADER_FILES_H_

#include <iostream>
#include <windows.h>
#include <vector>
#include <ctime>

using namespace std;

#define BOUNDARY_LEFT 4
#define BOUNDARY_RIGHT 77
#define BOUNDARY_TOP 4
#define BOUNDARY_BOTTOM 28

enum DIRECTION
{
    NORTH = 0,
    EAST,
    SOUTH,
    WEST,
    NORTH_WEST,
    NORTH_EAST,
    SOUTH_WEST,
    SOUTH_EAST
};

#include "Border.h"
#include "Enemy.h"
#include "GameObject.h"
#include "Player.h"

#endif // _DEFAULT_HEADER_FILES_H_
