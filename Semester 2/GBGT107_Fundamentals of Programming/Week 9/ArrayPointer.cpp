#include <iostream>
#include "windows.h"

using namespace std;

int main()
{
    int number1 = 10;
    int number2 = 20;
    int number3 = 30;
    int number4 = 40;
    int number5 = 50;
    int number6 = 60;

    int numbers[6] = {10, 20, 30, 40, 50, 60};

    for(int i = 0; i <5; i++)
    {
        cout << "Number "<< (i) << " : " <<numbers[i] << endl;
    }
    cout << endl;

    //! *ptr is storing the address of numbers(int array)
    int* ptr = numbers;
    cout << "Numbers: " << numbers << endl;
    cout << "*Numbers: " << *numbers << endl;

    cout << "ptr: " << ptr << endl;
    cout << "*ptr: " << *ptr << endl;
    ptr++;

    cout << "ptr: " << ptr << endl;
    cout << "*ptr: " << *ptr << endl;
    ptr++;

    cout << "ptr: " << ptr << endl;
    cout << "*ptr: " << *ptr << endl;

    cout << endl;

    cout <<"&Number[1]: " << &numbers[1] << endl;
    cout <<"Number[1]: " << numbers[1] << endl;

    system("PAUSE");
    return 0;
}
