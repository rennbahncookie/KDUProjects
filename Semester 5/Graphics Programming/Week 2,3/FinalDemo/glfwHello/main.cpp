#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <windows.h>
#include <GL/GLU.h>

#include <iostream>

#include "camera.h"
#include "matrix.h"
#include "vector.h"

#include "wave_demo.h"
#include "lighting_demo.h"
#include "triangle_demo.h"
#include "third_vertex_array_demo.h"
#include "wave2.h"
#include "wave3.h"
//#include "blending_demo.h"

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600


void onWindowResized(GLFWwindow* window, int width, int height)
{
	if (height == 0) height = 1;						// Prevent A Divide By Zero By making Height Equal One

	glViewport(0, 0, width, height);					// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	// Calculate The Aspect Ratio Of The Window
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

// Simple camera controller. (MOUSE)
Camera gCamera;
void onMouseMove(GLFWwindow* window, double x, double y)
{
	static int lastX = -1, lastY = -1;
	if (lastX == -1 && lastY == -1)
	{
		lastX = x;
		lastY = y;
		return;
	}

	int offsetX = x - lastX;
	int offsetY = y - lastY;
	lastX = x; lastY = y;

	gCamera.rotate(offsetX * 0.1f, Vector(0.0f, 1.0f, 0.0f));
	gCamera.rotateLocal(offsetY * 0.1f, Vector(1.0f, 0.0f, 0.0f));
}

int main()
{
	int running = GL_TRUE;

    GLFWwindow* window;

    /* Initialize the GLFW library */
    if (!glfwInit())
        return -1;

	// Open an OpenGL window
	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "OpenGL", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return -1;
    }

	// Hook window resize.
	glfwSetWindowSizeCallback(window, onWindowResized);
	   
	/* Make the window's context current */
    glfwMakeContextCurrent(window);

	onWindowResized(window, WINDOW_WIDTH, WINDOW_HEIGHT);

	// hook mouse move callback and lock/hide mouse cursor.
	glfwSetCursorPosCallback(window, onMouseMove);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// initialize OpenGL.
	glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
	glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black Background
	glClearDepth(1.0f);									// Depth Buffer Setup
	glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
	glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations

	// initialize camera.
	gCamera.translate(0.0f, 10.0f, 10.0f);
	Matrix matrix(Matrix::makeIdentityMatrix());
	Matrix rotateMatrix(Matrix::makeRotateMatrix(1.0f, Vector(0.0f, 0.0f, 30.0f)));
	gCamera.rotate(45.0f, Vector(30.0f, 0.0f, 0.0f));

	// initialize demo.
	DemoBase* demo = new WaveDemo();
	DemoBase* demo1 = new TriangleDemo();
	DemoBase* demo2 = new LightingDemo();
	DemoBase* demo3 = new ThirdVertexArrayDemo();
	DemoBase* demo4 = new WaveDemo2();
	DemoBase* demo5 = new WaveDemo3();
	//DemoBase* demo = new LightingDemo();
	demo->init();
	demo1->init();
	demo2->init();
	demo3->init();
	demo4->init();
	demo5->init();


	float timer = 0;
	// Main loop
	while (!glfwWindowShouldClose(window))
	{
		timer+= 0.01;
		std::cout << timer << std::endl;
		// OpenGL rendering goes here...
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Simple camera controller. (KEYBOARD)
		float camMoveOffsetX = 0.0f, camMoveOffsetZ = 0.0f;
		if(glfwGetKey(window, 'A')) camMoveOffsetX -= 0.01f;
		if(glfwGetKey(window, 'D')) camMoveOffsetX += 0.01f;
		if(glfwGetKey(window, 'W')) camMoveOffsetZ -= 0.01f;
		if(glfwGetKey(window, 'S')) camMoveOffsetZ += 0.01f;
		gCamera.translateLocal(camMoveOffsetX, 0.0f, camMoveOffsetZ);

		// Check if ESC key was pressed
		if(glfwGetKey(window, GLFW_KEY_ESCAPE))
			break;
		if (timer < 5.0f)
		{
			demo->draw(gCamera.getViewMatrix());
		}
		else if (timer > 5.0f && timer < 6.0f)
		{
			demo->deinit();
			demo1->draw(gCamera.getViewMatrix());
			
		}
		else if (timer > 7.0f && timer < 15.0f)
		{
			demo1->deinit();
			demo2->draw(gCamera.getViewMatrix());
		}
		else if (timer > 15.0f && timer < 50.0f)
		{
			demo3->draw(gCamera.getViewMatrix());
		}
		else if (timer > 50.0f && timer < 55.0f)
		{
			demo4->draw(gCamera.getViewMatrix());
		}
		else if (timer > 55.0f && timer < 60.0f)
		{
			demo5->draw(gCamera.getViewMatrix());
		}
		else if (timer > 60.0f && timer < 65.0f)
		{
			demo4->draw(gCamera.getViewMatrix());
		}
		else if (timer > 65.0f && timer < 70.0f)
		{
			demo5->draw(gCamera.getViewMatrix());
		}

		// Swap front and back rendering buffers
		glfwSwapBuffers(window);

		glfwPollEvents();
	}

	demo->deinit();
	delete demo;

	// Close window and terminate GLFW
	glfwTerminate();
}
