#ifndef _SECOND_VERTEX_ARRAY_DEMO_H
#define _SECOND_VERTEX_ARRAY_DEMO_H

#include "demo_base.h"
#include "bitmap.h"
#define TEXTURE_COUNT 1

class SecondVertexArrayDemo : public DemoBase
{
private:
	GLuint mtextureID[TEXTURE_COUNT];
	void loadTexture(const char* path, GLuint textureID)
	{
		CBitmap bitmap(path);

		// Create Linear FIltered Texture
		glBindTexture(GL_TEXTURE_2D, textureID);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//aooky texture wrapping along horizontal part.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);//aooky texture wrapping along vertical part.

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); //near filtering (For when texgure needs to scale up on screen)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);


		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.GetWidth(), bitmap.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.GetBits());
	}
public:
	void init()
	{
		glGenTextures(TEXTURE_COUNT, mtextureID);
		loadTexture("../media/rocks.bmp", mtextureID[0]);
	}

	void deinit()
	{

	}
	void draw(const Matrix& viewMatrix)
	{
		//drawaxis
		glLoadMatrixf((GLfloat*)viewMatrix.mVal);
		glBegin(GL_LINES);
		glColor3f(1.0f, 0.3f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(1.0f, 0.0f, 0.0f);

		glColor3f(0.3f, 1.0f, 0.3f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);

		glColor3f(0.3f, 0.3f, 1.0f);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, 1.0f);
		glEnd();

		Matrix rotate1 = Matrix::makeRotateMatrix(0, Vector(0.0f, 0.0f, 1.0f));

		Matrix modelMatrix = rotate1;

		Matrix viewSpaceMatrix = viewMatrix;// *modelMatrix;


		// Rectangle
		glLoadMatrixf((GLfloat*)viewSpaceMatrix.mVal);
		//drawPyramid();
		//drawCube();
		//glColor3f(1.0f, 1.0f, 1.0f);

		static GLfloat vertices[] =
		{
			/*		-1.0f, -1.0f, 1.0f,
					1.0f, -1.0f, 1.0f,
					1.0f, 1.0f, 1.0f,
					1.0f, 1.0f, 1.0f,
					-1.0f, 1.0f, 1.0f,
					-1.0f, -1.0f, 1.0f
					*/
			0.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,

			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,

			1.0f, 1.0f, 0.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			1.0f, 0.0f, 0.0f,

			0.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			1.0f, 0.0f, 0.0f,

			1.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			0.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f,
			0.0f, 0.0f, 1.0f,

			0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 1.0f,
		};

		static GLfloat texCoords[] =
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,

			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 0.0f,
			0.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,

			0.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,

			0.0f, 1.0f,
			1.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,

			0.0f, 1.0f,
			1.0f, 1.0f,
			0.0f, 0.0f,
			1.0f, 0.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,

			1.0f, 0.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,
			1.0f, 1.0f,
			0.0f, 0.0f,
			0.0f, 1.0f
		};
		static GLubyte colors[] = 
		{
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,

			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255,
			rand() % 255, rand() % 255, rand() % 255
			//26, 199, 60,
			//82, 200, 106,
			//106, 112, 168,

			//65, 233, 64,
			//240, 223, 67,
			//252, 143, 227,

			//60, 193, 89,
			//172, 238, 79,
			//150, 5, 98,

			//150, 160, 208,
			//74, 154, 151,
			//229, 105, 249,

			//166, 102, 16,
			//221, 232, 177,
			//62, 45, 220,

			//115, 195, 43,
			//142, 5, 206,
			//195, 113, 192,

			//98, 98, 142,
			//14, 104, 233,
			//104, 124, 198,

			//238, 248, 246,
			//233, 226, 162,
			//67, 197, 56,

			//214, 64, 108,
			//36, 131, 110,
			//32, 60, 3,

			//230, 200, 148,
			//119, 77, 49,
			//195, 165, 179,

			//70, 253, 181,
			//233, 182, 64,
			//108, 144, 231,

			//29, 2, 111,
			//217, 156, 211,
			//191, 246, 91,

			/*	255, 255, 255,
				255, 0, 0,
				0, 255, 0,
				0, 255, 0,
				255, 0, 0,
				255, 255, 255,

				255, 255, 255,
				255, 0, 0,
				0, 255, 0,
				0, 255, 0,
				255, 0, 0,
				255, 255, 255,

				255, 255, 255,
				255, 0, 0,
				0, 255, 0,
				0, 255, 0,
				255, 0, 0,
				255, 255, 255,

				255, 255, 255,
				255, 0, 0,
				0, 255, 0,
				0, 255, 0,
				255, 0, 0,
				255, 255, 255,

				255, 255, 255,
				255, 0, 0,
				0, 255, 0,
				0, 255, 0,
				255, 0, 0,
				255, 255, 255,

				255, 255, 255,
				255, 0, 0,
				0, 255, 0,
				0, 255, 0,
				255, 0, 0,
				255, 255, 255*/
		};
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_1D, mtextureID[0]);
		// activate and specify pointer to vertext array
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		//tell opengl which vertex array is going to be used (set active)
		// in this case, vertices is the name of the vertices array going to be used
		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
		glColorPointer(3, GL_UNSIGNED_BYTE, 0, colors);

		int numberOfVertices = 3 * 2 * 6;
		// draw rectangle
		glDrawArrays(GL_TRIANGLES, 0, numberOfVertices);

		//deactivate vertex arrats after drawing
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		glDisable(GL_TEXTURE_2D);

	}
};


#endif