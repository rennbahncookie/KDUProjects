﻿using UnityEngine;
using System.Collections.Generic;

public class MultitouchTestOld : MonoBehaviour
{
	public GameObject mTouchSpritePrefab;
	Dictionary<int, Transform> mTouchSprites = new Dictionary<int, Transform>();

	void OnEnable()
	{
		TouchManager.instance.EvtOnTouchDown += OnTouchDown;
		TouchManager.instance.EvtOnTouchDrag += OnTouchDrag;
		TouchManager.instance.EvtOnTouchUp += OnTouchUp;
	}
	
	void OnDisable()
	{
		TouchManager.instance.EvtOnTouchDown -= OnTouchDown;
		TouchManager.instance.EvtOnTouchDrag -= OnTouchDrag;
		TouchManager.instance.EvtOnTouchUp -= OnTouchUp;
	}

	void OnTouchDown(TouchManager.State state, ref bool processed)
	{
		GameObject sprite = (GameObject)Instantiate(mTouchSpritePrefab);
		Transform spriteTrans = sprite.transform;
	
		Vector3 screenPos = state.position;
		Vector3 thePos = Camera.main.ScreenToWorldPoint(screenPos);
		thePos.z = 0.0f;
		spriteTrans.position = thePos;

		mTouchSprites[state.fingerID] = spriteTrans;
	}
	
	void OnTouchDrag(TouchManager.State state, ref bool processed)
	{
		Transform spriteTrans = mTouchSprites[state.fingerID];
		Vector3 screenPos = state.position;
		Vector3 thePos = Camera.main.ScreenToWorldPoint(screenPos);
		thePos.z = 0.0f;
		spriteTrans.position = thePos;
	}
	
	void OnTouchUp(TouchManager.State state, ref bool processed)
	{
		Transform spriteTrans = mTouchSprites[state.fingerID];
		Destroy(spriteTrans.gameObject);
		mTouchSprites.Remove(state.fingerID);
	}
}
