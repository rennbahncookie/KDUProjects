﻿using UnityEngine;
using System.Collections;

public class SphereScript : MonoBehaviour {

    public float m_LifeTime;
    private float m_Life;
	// Use this for initialization
	void Start () {

        m_Life = m_LifeTime;
	
	}

    void OnEnable ()
    {
        m_Life = m_LifeTime;
    }
	
	// Update is called once per frame
	void Update () {

        m_Life -= Time.deltaTime;
        if(m_Life < 0.0f)
        {
            // Destroy(this.gameObject);
            this.gameObject.SetActive(false);
        }
	
	}
}
