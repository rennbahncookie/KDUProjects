﻿using UnityEngine;
using System.Collections;

public class mainScript1 : MonoBehaviour {

    Camera camera;
    public GameObject m_projectilePrefab;
    GameObject m_pickingObject;
    int count = 0;
    bool hold =false;
	// Use this for initialization
	void Start () {
        camera = GetComponent<Camera>();
        
	
	}
	
	// Update is called once per frame
	void Update () {
        if(Input.touchCount>0)
        {
            Touch myTouch = Input.GetTouch(0);
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hitInfo;
            // foreach(Touch touch in Input.touches)
            if (myTouch.phase == TouchPhase.Began)
            {


                if(Physics.Raycast(ray.origin, ray.direction, out hitInfo))
                {
                    if (hitInfo.collider.gameObject.tag == "Pickup")
                    {
                        Debug.Log("Pickup");
                        m_pickingObject = hitInfo.collider.gameObject;
                        //Vector3 test = hitInfo.collider.gameObject.transform.position - ray.origin;
                        //Rigidbody newObjectRigidBody = hitInfo.collider.gameObject.GetComponent<Rigidbody>();
                        //newObjectRigidBody.velocity = test.normalized * -1.0f;
                    }
        
                }


                //GameObject newObject = Instantiate(m_projectilePrefab, ray.origin, Quaternion.identity) as GameObject;
                //count++;
                //Debug.Log(count);
                //Rigidbody newObjectRigidBody = newObject.GetComponent<Rigidbody>();
                //newObjectRigidBody.velocity = ray.direction * 15.0f;

            }
            if (myTouch.phase == TouchPhase.Stationary || myTouch.phase == TouchPhase.Moved)
            {
                if (m_pickingObject != null)
                {
                    Debug.Log("Drag");
                    // move the object
                    Rigidbody newObjectRigidBody = m_pickingObject.GetComponent<Rigidbody>();
                    Ray fingerToObject = camera.ScreenPointToRay(myTouch.position);
                    Vector3 pos = (fingerToObject.origin + fingerToObject.direction) * 1f;

                    newObjectRigidBody.velocity = pos;
                }
                //Vector3 myCameraPosition = new Vector3(myTouch.position.x, myTouch.position.y, 0);
                //hitInfo.collider.gameObject.transform.position += myCameraPosition;
            }
            if (myTouch.phase == TouchPhase.Ended)
            {
                if (m_pickingObject != null)
                {
                    // shoot th e object
                    Rigidbody newObjectRigidBody = m_pickingObject.GetComponent<Rigidbody>();
                    newObjectRigidBody.velocity = ray.direction * 25.0f;
                    m_pickingObject = null;
                }
            }
        }
	
	}
}
