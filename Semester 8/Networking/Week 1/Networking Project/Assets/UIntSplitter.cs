﻿using UnityEngine;
using System.Collections;

public class UIntSplitter{
    public byte[] m_bytes;

    public void storeUInt(uint value)
    {
        m_bytes = new byte[4];

        for(int i = 0; i < m_bytes.Length; i++)
        {
            m_bytes[i] = (byte)((value >> i * 8)& 0xff);
        }
    }

    public uint getInt()
    {
        uint returnInt = new uint();

        for(int i = 0; i < m_bytes.Length; i++)
        {
            returnInt = returnInt | (uint)(m_bytes[i] << i*8);
        }
        Debug.Log(returnInt);
        // built in bit converter
        // returnInt = (uint)System.BitConverter.ToInt32(m_bytes, 0);
        
        return returnInt;
    }
}
