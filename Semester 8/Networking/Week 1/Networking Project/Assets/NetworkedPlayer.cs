﻿using UnityEngine;
using System.Collections;

public class NetworkedPlayer : Photon.MonoBehaviour
{

    float timer = 0;
    float timeReset = 0.1f;

    public float forwardVel = 12;
    public float rotateVel = 100;

    Quaternion targetRotation;
    Rigidbody rBody;
    float forwardInput, sideBySideInput;



    void Start()
    {
        targetRotation = transform.rotation;
        rBody = GetComponent<Rigidbody>();
        newPos = transform.position;
    }

    void GetInput()
    {
        forwardInput = Input.GetAxis("Vertical");
        sideBySideInput = Input.GetAxis("Horizontal");
    }

    void Update()
    {
        if (photonView.isMine)
            GetInput();
        else
            PredictPos();



        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            photonView.RPC("SendPosition", PhotonTargets.Others, photonView.ownerId, transform.position.x,
                transform.position.y, rBody.velocity.x, rBody.velocity.y);
            timer = timeReset;
        }
    }

    void FixedUpdate()
    {
        Run();
        Turn();

    }

    void Run()
    {
        if (forwardInput != 0)
            rBody.velocity = transform.forward * forwardInput * forwardVel;
        else
            rBody.velocity = Vector3.zero;
    }

    void Turn()
    {
        targetRotation *= Quaternion.AngleAxis(rotateVel * sideBySideInput * Time.deltaTime, Vector3.up);
        transform.rotation = targetRotation;
    }
    Vector3 newPos;
    void PredictPos()
    {
        transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime);
    }


    [PunRPC]
    void SendPosition(int playerID, float posX, float posY, float velX, float velY)
    {
        newPos = new Vector3(posX, posY);
    }


    [PunRPC]
    void onObjectMove(int dir, bool up)
    {
        Debug.Log("Move Object");
        var pos = transform.position;

        if (!up)
            pos.x = pos.x + dir;// * Time.deltaTime * 3;
        else
            pos.z = pos.z + dir;// * Time.deltaTime * 3;


        transform.position = pos;
    }

    [PunRPC]
    void ChatMessage(string message)
    {
        Debug.Log("Chat says:" + message);
    }
}
