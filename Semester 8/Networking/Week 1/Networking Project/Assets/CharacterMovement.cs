﻿using UnityEngine;
using System.Collections;


public class CharacterMovement : Photon.MonoBehaviour
{
    Vector3 position;
    Vector3 velocity;
    Vector3 acceleration;

    public float accelerationModifier = 0.1f;
    public float velocityModifier = 0.1f;
    public float friction = 0.1f;

    public float interpolationWeight = 0.9f;

    float timer = 0;
    public float timeReset = 0.0f;

    Vector3 predictedPos;

    public bool linearInterpolation = true;

    void Start()
    {
        position = velocity = acceleration = Vector3.zero;
    }

    void Update()
    {
        Movement();

        if (photonView.isMine)
        {
            NetworkUpdate();
        }
    }

    void NetworkUpdate()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            photonView.RPC("SendPosition", PhotonTargets.Others, photonView.ownerId, position.x,
                position.y, position.z, velocity.x, velocity.y, velocity.z);
            timer = timeReset;
        }
    }

    [PunRPC]
    void SendPosition(int playerID, float posX, float posY, float posZ, float velX, float velY, float velZ)
    {
        predictedPos.x = posX;
        predictedPos.y = posY;
        predictedPos.z = posZ;

        velocity.x = velX;
        velocity.y = velY;
        velocity.z = velZ;
    }


    void Movement()
    {
        // if input then change acceleration.x = 1
        if (photonView.isMine)
        {
            if (Input.GetKey(KeyCode.W))
            {
                acceleration.z = 1;
            }

            if (Input.GetKey(KeyCode.S))
            {
                acceleration.z = -1;
            }

            if (Input.GetKey(KeyCode.A))
            {
                acceleration.x = -1;
            }

            if (Input.GetKey(KeyCode.D))
            {
                acceleration.x = 1;
            }
        }


        velocity = velocity * velocityModifier + (acceleration * accelerationModifier);


        //friction
        velocity = velocity * friction;

        if (photonView.isMine)
            position = position + velocity;
        else
        {
            if (linearInterpolation)
                position = Vector3.Lerp(position, predictedPos, (100f - interpolationWeight*10) * Time.deltaTime);
            else
                position = interpolationWeight * position + (1.0f - interpolationWeight) * predictedPos;
        }

        //reset accel (or dont if you do = 0 on not button press)
        acceleration = Vector3.zero;

        transform.position = position;
    }

}
