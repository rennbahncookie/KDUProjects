#include "SeanEngine.h"

SeanEngine::SeanEngine()
{
	std::cout << "engine constructor" << std::endl;
}

//~ destructor
SeanEngine::~SeanEngine()
{
	std::cout << "engine constructor" << std::endl;
}

void SeanEngine::run(void)
{
#ifdef NDEBUG
	std::cout << "run in release" << std::endl;
#else
	std::cout << "run in debug" << std::endl;
#endif

	std::cout << "run engine" << std::endl;
}


