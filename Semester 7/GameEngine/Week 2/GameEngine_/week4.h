
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osg/ShapeDrawable>
#include <osg/Array>
#include <osg/MatrixTransform>
#include <osg/Geode>
#include <osg/StateSet>
#include <osg/Shape>
#include <osg/PositionAttitudeTransform>
#include <osg/Node>
#include <osg/AnimationPath>
#include <iostream>

#include <osg/Notify>
#include <osg/MatrixTransform>
#include <osg/PositionAttitudeTransform>
#include <osg/Geometry>
#include <osg/Geode>
#include <osg/ShapeDrawable>
#include <osg/Texture2D>
#include <osg/Material>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/LightModel>
#include <osg/Billboard>
#include <osg/LineWidth>
#include <osg/TexEnv>
#include <osg/TexEnvCombine>
#include <osg/ClearNode>


#include <osgUtil/Optimizer>

#include <osgDB/Registry>
#include <osgDB/ReadFile>
#include <osgDB/WriteFile>


#include <osgGA/NodeTrackerManipulator>
#include <osgGA/TrackballManipulator>
#include <osgGA/FlightManipulator>
#include <osgGA/DriveManipulator>
#include <osgGA/KeySwitchMatrixManipulator>

#include <osgViewer/Viewer>
#include "Windows.h"

#include <iostream>


void Week4();
void Code1();
void Code2();
void Code3STL();

class Manager
{
public:
	virtual ~Manager() {}
};

class SoundManager : public Manager
{
public:
	virtual ~SoundManager() {}
};

class ControlManager : public Manager
{
public:
	virtual ~ControlManager() {}
};


class SoloObject
{
public:
	virtual ~SoloObject() {}
};

template<class T>
class CircularBuffer
{
private:
	int _start;
	int tail_;
	int head_;
	int contents_size_;

	int array_size_;

	T* _buffer;// _buffer;

	void increment_tail()
	{
		++contents_size_;
		tail_ = next_tail();
	}

	void decrement_tail()
	{
		++contents_size_;
		tail_ = (tail_ - 1 == -1) ? array_size_ : tail_ - 1;
	}

	int next_tail()
	{
		return (tail_ + 1 == array_size_) ? 0 : tail_ + 1;
	}

	int next_head()
	{
		return (head_ + 1 == array_size_) ? 0 : head_ + 1;
	}
	void increment_head()
	{
		// precondition: !empty()
		++head_;
		--contents_size_;
		if (head_ == array_size_) head_ = 0;
	}

	void pop(int location)
	{
		if (_buffer[location] == NULL)
		{
			if (location > array_size_)
				throw std::out_of_range("the value you're trying to get is out of range");
			return;
		}
		_buffer[location] = 0;
	}


public:
	CircularBuffer(const int size)
	{
		if (size <= 0)
			array_size_ = 10;
		else
			array_size_ = size;

		_start = -1;
		tail_ = 0;
		head_ = 0;
		contents_size_ = 0;
		_buffer = new T[size];

	}

	//add item into array
	void push(T const &obj) {
		_buffer[head_] = obj;
		if (_start == -1)
			_start = head_;
		head_ = (head_ + 1) % array_size_;



		if (contents_size_ + 1 > array_size_)
			increment_tail();
		else
			contents_size_++;
	}


	void popFront() {
		int destroyPos = head_;
		--contents_size_;
		if (head_ - 1 == -1)
			head_ = array_size_ - 1;
		else
			head_--;

		pop(head_);

	}
	void popBack() {

		int destroyPos = tail_;
		--contents_size_;

		if (tail_ + 1 >= array_size_)
			tail_ = 0;
		else
			tail_++;
		pop(destroyPos);

	}

	int getSize() {}
	T getValue(int location)
	{
		if (location > array_size_)
			throw std::out_of_range("the value you're trying to get is out of range");
		else
			return _buffer[location];
	}

	T getBufferValue(int location)
	{
		location++;
		if (location > array_size_)
			throw std::out_of_range("the value you're trying to get is out of range");
		else
		{
			int bufferlocation = head_;

			if (head_ - location < 0)
				return _buffer[array_size_ + (head_ - location)];
			else
				return _buffer[array_size_ - location];

		}
	}

	void DisplayArrayList()
	{
		for (int i = 0; i < array_size_; i++)
		{
			std::cout << i + 1 << ". " << getValue(i) << std::endl;
		}
	}
};


//template<class T>
//class CircularBuffer
//{
//private:
//	int _start;
//	int _end;
//	int _size;
//	int _contentSize;
//
//	T array[10];// _buffer;
//
//	    void increment_tail()
//        {
//            ++contents_size_;
//            tail_ = next_tail();
//        }
//
//        int next_tail()
//        {
//            return (tail_+1 == array_size_) ? 0 : tail_+1;
//        }
//
//        void increment_head()
//        {
//            // precondition: !empty()
//            ++head_;
//            --contents_size_;
//            if (head_ == array_size_) head_ = 0;
//        }
//
//public: 
//	CircularBuffer(const int size) : _size(size), _start(0), _end(0)
//	{
//		_buffer = new T[size];
//	}
//
//	bool push(T const &obj){
//	int next = next_tail();
//
//	        _buffer[next] = obj;
//            increment_head();
//            increment_tail();
//
//	}
//
//
//	void popFront(){
//		int destroyPos = _head;
//		increment_head();
//		_buffer.pop(_head);
//	
//	}
//	void popBack(){}
//
//	int getSize(){}
//	T getValue(int location)
//	{
//		return _buffer[location];
//	}
//};
//- start
//- end
//- array[10]
//+ push
//+ popFront
//+ popBack
//+ int getSize
//+ T getValue(int location)