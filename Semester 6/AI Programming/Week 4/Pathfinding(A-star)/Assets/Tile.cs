﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{

    public int TilePosX;
    public int TilePosZ;
    public float distance = 999999;
    public bool addedToOpenList = false;

    public enum TileState
    {
        Default = 0,
        StartNode,
        EndNode,
        Obstacle,
        Settled,
        Path
    }

    public int currentState = 0;

    public GameObject parentNode;

    public TileState state = TileState.Default;

    MeshRenderer m_Renderer;
    void Start()
    {
        m_Renderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        if(currentState == 0)
        {
            state = TileState.Default;
        }
        else if(currentState == 1)
        {
            state = TileState.Obstacle;
        }
        else if(currentState == 2)
        {
            state = TileState.EndNode;
        }
        else if(currentState == 3)
        {
            state = TileState.StartNode;
        }

        if (state == TileState.Default)
        {
            m_Renderer.material.color = Color.white;
        }
        else if (state == TileState.StartNode)
        {
            m_Renderer.material.color = Color.green;
        }
        else if (state == TileState.EndNode)
        {
            m_Renderer.material.color = Color.red;
        }
        else if (state == TileState.Obstacle)
        {
            m_Renderer.material.color = Color.blue;
        }
    }

    public void OnMouseDown()
    {
        currentState++;
        if(currentState > 3)
        {
            currentState = 0;
        }
    }
}
