﻿using UnityEngine;
using System.Collections.Generic;

public class DragObject : MonoBehaviour {

	public int X;
	public int Y;
	public List<TileMap.Node> currentPath = null;
	float snapPosX;
	float snapPosY;
	TileMap tileMap;
	Vector3 myPos;
	Vector3 mousePos;
	public bool isStart;
	// Use this for initialization
	void Start () 
	{
		tileMap = GameObject.Find ("Map").GetComponent<TileMap> ();
		X = (int)transform.position.x;
		Y = (int)transform.position.y;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(isStart)
		{
			tileMap.searchPathFind ((int)GameObject.Find("End").transform.position.x,
			                        (int)GameObject.Find("End").transform.position.y);
			if(currentPath != null)
			{
				int currNode = 0;
				while(currNode < currentPath.Count-1)
				{
					Vector3 start = tileMap.TileCoordToWorldCoord(currentPath[currNode].X, currentPath[currNode].Y) + new Vector3(0,0,-1);
					Vector3 end = tileMap.TileCoordToWorldCoord(currentPath[currNode+1].X, currentPath[currNode+1].Y) + new Vector3(0,0,-1);
					Debug.DrawLine(start, end, Color.red);
					currNode ++;
				}
			}
		}
	}


	void OnMouseDrag()
	{
		if(gameObject.name == "End")
		{
			mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mousePos.z = -0.75f;
			transform.position = mousePos;
		}
	}
	

	void OnMouseUp()
	{

	}
}
