﻿using UnityEngine;
using System.Collections.Generic;

public class TileMap : MonoBehaviour {

	public GameObject tilePrefab;

	public GameObject[,] tiles;
	public Node[,] Path = null;
	public int tempInt = 0;
	float distance;
	List<Node> currentPath = null;
	public int sizeMapX;
	public int sizeMapY;

	DragObject dragObject;
	public bool isDijkstra;
	public bool isAstar;
	// Use this for initialization
	void Start () 
	{
		dragObject = GameObject.Find ("Start").GetComponent<DragObject> ();
		CreatePathFindMap ();
		CreateMap ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void CreateMap()
	{
		tiles = new GameObject[sizeMapX,sizeMapY];
		for(int i=0; i < sizeMapX; i+= 1)
		{
			for(int j=0; j < sizeMapY; j+= 1)
			{
				GameObject tileP = Instantiate(tilePrefab, new Vector3(i, j, 0), Quaternion.identity)as GameObject;
				tiles[i,j] = tileP;
				tiles[i,j].GetComponent<TileProperties>().X = i;
				tiles[i,j].GetComponent<TileProperties>().Y = j;
			}
		}
	}

	public class Node
	{
		public List<Node> checkNeigh;
		public int X;
		public int Y;
		public Node()
		{
			checkNeigh = new List<Node>();
		}

		public float HeuristicDistance(Node n)
		{
			return Vector2.Distance(new Vector2(X,Y), new Vector2(n.X,n.Y));
		}
	}

	/*public float HeuristicCost(int startX, int startY, int endX, int endY)
	{
		float cost = tiles[endX, endY].GetComponent<TileProperties>().movementCost
	}*/

	public float CostEnter(int startX, int startY, int endX, int endY)
	{
		float cost = tiles [endX, endY].GetComponent<TileProperties> ().movementCost;
		if(startX != endX && startY != endY)
		{
			cost += 0.4f;
		}
		return cost;
	}

	void CreatePathFindMap()
	{
		Path = new Node[sizeMapX, sizeMapY];

		for(int i=0; i < sizeMapX; i++)
		{
			for(int j=0; j < sizeMapY; j++)
			{
				Path[i,j] = new Node();
				Path[i,j].X = i;
				Path[i,j].Y = j;
			}
		}
		for(int i=0; i < sizeMapX; i++)
		{
			for(int j=0; j < sizeMapY; j++)
			{
				//Left
				if(i > 0)
				{
					Path[i,j].checkNeigh.Add(Path[i-1, j]);
					if(j > 0)
						Path[i,j].checkNeigh.Add(Path[i-1, j-1]);
					if(j < sizeMapY-1)
						Path[i,j].checkNeigh.Add(Path[i-1, j+1]);
				}
				//Right
				if(i < sizeMapX-1)
				{
					Path[i,j].checkNeigh.Add(Path[i+1, j]);
					if(j > 0)
						Path[i,j].checkNeigh.Add(Path[i+1,j-1]);
					if(j < sizeMapY-1)
						Path[i,j].checkNeigh.Add(Path[i+1,j+1]);
				}
				//Down
				if(j > 0)
					Path[i,j].checkNeigh.Add(Path[i, j-1]);
				//Up
				if(j < sizeMapY-1)
					Path[i,j].checkNeigh.Add(Path[i, j+1]);
			}
		}
	}

	public Vector3 TileCoordToWorldCoord(int x, int y)
	{
		return new Vector3 (x, y, 0);
	}

	public void searchPathFind(int x, int y)
	{
		dragObject.currentPath = null;

		Dictionary<Node, float> dist = new Dictionary<Node, float>();
		Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

		Node start = Path [dragObject.X, dragObject.Y];
		Node end = Path [x,y];
		List<Node> unvisited = new List<Node> ();

		dist [start] = 0;
		prev [start] = null;

		foreach(Node v in Path)
		{
			if(v != start)
			{
				dist[v] = Mathf.Infinity;
				prev[v] = null;
			}
			unvisited.Add(v);
		}


		while(unvisited.Count > 0)
		{
			tempInt++;

			Node u = null;

			foreach(Node posU in unvisited)
			{
				if(u == null || dist[posU] < dist[u])
				{
					u = posU;
				}
			}

			if(u == end)
			{
				break;
			}
			unvisited.Remove(u);

			foreach(Node v in u.checkNeigh)
			{
				//float distance = dist[u] + u.Distance(v);
				if(isDijkstra)
				{
					distance = dist[u] + CostEnter(u.X, u.Y, v.X, v.Y);
					if(distance < dist[v])
					{
						dist[v] = distance;
						prev[v] = u;
					}
				}
				else if(isAstar)
				{
					distance = dist[u] + CostEnter(u.X, u.Y, v.X, v.Y) + 10;
					if(distance < dist[v])
					{
						dist[v] = distance;
						prev[v] = u;
					}
				}
			}
		}
		if(prev[end] == null)
		{
			return;
		}

		currentPath = new List<Node> ();

		Node currNode = end;

		while(currNode != null)
		{
			currentPath.Add(currNode);
			currNode = prev[currNode];
		}

		currentPath.Reverse ();

		dragObject.currentPath = currentPath;
		Debug.Log(tempInt);
		Debug.Log (distance);
	}
}
