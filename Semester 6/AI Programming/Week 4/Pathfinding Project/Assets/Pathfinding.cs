﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pathfinding : MonoBehaviour
{

    Node[,] m_NodeArray;
    int X = 80;
    int Y = 60;

    public enum SearchMethod
    {
        Dijkstra = 0,
        Astar
    }

    public SearchMethod method;

    public bool diagonal = false;

    void Start()
    {
        SpawnArray(X, Y);
    }

    void SpawnArray(int x, int z)
    {
        var tile = Resources.Load("Node") as GameObject;
        var size = tile.GetComponent<SpriteRenderer>().bounds.size.x;
        var container = new GameObject();

        m_NodeArray = new Node[x, z];
        container.name = "ArrayContainer";
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < z; j++)
            {
                var obj = Instantiate(tile);
                var node = m_NodeArray[i, j] = new Node(obj);
                var location = new Vector3(size * i - (size * x - 1) / 2, size * j - (size * z - 1) / 2, 0);

                location += transform.position;
                node.Setup(node, location, container.transform, i, j);
            }
        }
    }

    public void FindStartandEnd()
    {
        ArrayPoint startPoint = new ArrayPoint();
        ArrayPoint endPoint = new ArrayPoint();

        for (int i = 0; i < m_NodeArray.GetLength(0) - 1; i++)
        {
            for (int j = 0; j < m_NodeArray.GetLength(1) - 1; j++)
            {
                var n = m_NodeArray[i, j];
                if (n.m_state == NodeScript.TileState.StartNode)
                {
                    startPoint.SetArrayPoint(i, j);
                }
                else if (n.m_state == NodeScript.TileState.EndNode)
                {
                    endPoint.SetArrayPoint(i, j);
                }
            }
        }

        if (startPoint.X == -1 || endPoint.X == -1)
            return;

        else
            StartPathfind(startPoint, endPoint);
    }

    void StartPathfind(ArrayPoint start, ArrayPoint end)
    {
        StopAllCoroutines();
        Node startNode = m_NodeArray[start.X, start.Y];
        Node endNode = m_NodeArray[end.X, end.Y];

        List<Node> openList = new List<Node>();
        List<Node> unvisitedNodes = new List<Node>();

        SetupNodes(unvisitedNodes);


        startNode.g = 0;
        startNode.h = 0;

        openList.Add(startNode);

        StartCoroutine(NodeSearch(openList, unvisitedNodes, endNode));


    }

    IEnumerator NodeSearch(List<Node> openList, List<Node> unvisitedNodes, Node endNode)
    {
        if (openList.Count == 0)
        {
            Debug.Log("No path found");
            yield break;
        }
        var node = lowestNode(openList, unvisitedNodes);
        openList.Remove(node);
        unvisitedNodes.Remove(node);

        node.SetVisualColour(Color.grey);

        if (node == endNode)
        {
            TracePath(node);
            StopAllCoroutines();
            yield break;
        }

        var neighbourN = new List<Node>();

        var x = node.X;
        var y = node.Y;

        if (x > 0)
            neighbourN.Add(m_NodeArray[x - 1, y]);

        if (y > 0)
            neighbourN.Add(m_NodeArray[x, y - 1]);

        if (x < m_NodeArray.GetLength(0) - 1)
            neighbourN.Add(m_NodeArray[x + 1, y]);

        if (y < m_NodeArray.GetLength(1) - 1)
            neighbourN.Add(m_NodeArray[x, y + 1]);

        if (diagonal)
        {
            if (x > 0 && y > 0)
            {
                neighbourN.Add(m_NodeArray[x - 1, y - 1]);
                m_NodeArray[x - 1, y - 1].diagonal = true;
            }
            if (x < m_NodeArray.GetLength(0) - 1 && y < m_NodeArray.GetLength(1) - 1)
            {
                neighbourN.Add(m_NodeArray[x + 1, y + 1]);
                m_NodeArray[x + 1, y + 1].diagonal = true;
            }
            if (x < m_NodeArray.GetLength(0) - 1 && y > 0)
            {
                neighbourN.Add(m_NodeArray[x + 1, y - 1]);
                m_NodeArray[x + 1, y - 1].diagonal = true;
            }

            if (y < m_NodeArray.GetLength(1) - 1 && x > 0)
            {
                neighbourN.Add(m_NodeArray[x - 1, y + 1]);
                m_NodeArray[x - 1, y + 1].diagonal = true;
            }
        }




        foreach (Node v in neighbourN)
        {
            if (unvisitedNodes.IndexOf(v) > -1 && v.m_state != NodeScript.TileState.Obstacle)
            {
                if (method == SearchMethod.Dijkstra)
                {
                    if (node.g < v.g)
                    {
                        v.g = node.g + 1;
                        if(v.diagonal)
                        {
                            v.g += 0.4f;
                            v.diagonal = false;
                        }
                        v.parent = node;
                    }

                    if (openList.IndexOf(v) == -1)
                    {
                        openList.Add(v);
                    }
                }
                else if (method == SearchMethod.Astar)
                {
                    if (openList.IndexOf(v) == -1)
                    {
                        openList.Add(v);
                        v.parent = node;
                        v.g = node.g + 1;
                        if (v.diagonal)
                        {
                            v.g += 0.4f;
                            v.diagonal = false;
                        }
                        v.h = Mathf.Abs(v.X - endNode.X) + Mathf.Abs(v.Y - endNode.Y);
                    }
                    else
                    {
                        if (v.g < node.g)
                        {
                            v.parent = node;
                            v.g = node.g - 1;
                            if (v.diagonal)
                            {
                                v.g -= 0.4f;
                                v.diagonal = false;
                            }
                        }
                    }
                }

                v.SetVisualColour(Color.yellow);
            }
        }
        yield return new WaitForSeconds(0.0f);
        yield return StartCoroutine(NodeSearch(openList, unvisitedNodes, endNode));

    }



    Node lowestNode(List<Node> OL, List<Node> UN)
    {
        Node rN = null;

        #region Astar SearchMethod
        if (method == SearchMethod.Astar)
        {
            foreach (Node cN in OL)
            {
                if (rN == null)
                {
                    rN = cN;
                }
                if ((rN.g + rN.h) > (cN.g + cN.h))
                {
                    if (UN.IndexOf(cN) > -1)
                    {
                        rN = cN;
                    }
                }
                else if ((rN.g + rN.h) == (cN.g + cN.h))
                {
                    if (rN.h > cN.h)
                    {
                        if (UN.IndexOf(cN) > -1)
                        {
                            rN = cN;
                        }
                    }


                }
            }


        }
        #endregion
        else if (method == SearchMethod.Dijkstra)
        {
            foreach (Node cN in OL)
            {
                if (rN == null || cN.g < rN.g)
                {
                    if (UN.IndexOf(cN) > -1)
                    {
                        rN = cN;
                    }
                }
            }
        }

        return rN;
    }

    void SetupNodes(List<Node> UN)
    {
        for (int i = 0; i < m_NodeArray.GetLength(0) - 1; i++)
        {
            for (int j = 0; j < m_NodeArray.GetLength(1) - 1; j++)
            {
                var n = m_NodeArray[i, j];
                n.g = Mathf.Infinity;
                n.h = Mathf.Infinity;
                
                n.SetVisualColour(Color.white);
                UN.Add(n);
            }
        }
    }

    void TracePath(Node end)
    {
        if (end.parent == null)
        {
            return;
        }
        end.SetVisualColour(Color.magenta);
        TracePath(end.parent);

    }

    public void ToggleSearchMethod()
    {
        if (method == SearchMethod.Dijkstra)
            method = SearchMethod.Astar;

        else
            method = SearchMethod.Dijkstra;
    }

    public void ToggleDiagonal()
    {
        diagonal = !diagonal;
    }

    public void ReloadScene()
    {
        Application.LoadLevel(0);
    }



}
