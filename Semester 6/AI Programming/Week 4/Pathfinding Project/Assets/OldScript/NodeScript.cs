﻿using UnityEngine;
using System.Collections;

public class NodeScript : MonoBehaviour
{

    //    public int TilePosX, TilePosY;
    //public ArrayScript array;
    //public float distance = 999999;
    //public bool addedToOpenList = false;
    public Node parentNode;

    public enum TileState
    {
        Default = 0,
        StartNode,
        EndNode,
        Obstacle,
        Settled,
        Path
    }

    public int currentState = 0;

    //    public GameObject parentNode;

    public TileState state = TileState.Default;

    SpriteRenderer m_Renderer;
    void Start()
    {
        m_Renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {

    }

    public void OnMouseDown()
    {
        currentState++;
        if (currentState > 3)
        {
            currentState = 0;
        }

        UpdateVisual();

    }

    void UpdateVisual()
    {
        if (currentState == 0)
        {
            state = TileState.Default;
        }
        else if (currentState == 1)
        {
            state = TileState.Obstacle;
        }
        else if (currentState == 2)
        {
            state = TileState.EndNode;
        }
        else if (currentState == 3)
        {
            state = TileState.StartNode;
        }

        if (state == TileState.Default)
        {
            m_Renderer.material.color = Color.white;
        }
        else if (state == TileState.StartNode)
        {
            m_Renderer.material.color = Color.green;
        }
        else if (state == TileState.EndNode)
        {
            m_Renderer.material.color = Color.red;
        }
        else if (state == TileState.Obstacle)
        {
            m_Renderer.material.color = Color.blue;
        }

        parentNode.m_state = state;
        GameObject.Find("Array").GetComponent<Pathfinding>().FindStartandEnd();
    }

    public void SetToDefault()
    {
        currentState = 0;
        UpdateVisual();
    }


}
