﻿using UnityEngine;
using System.Collections;

public enum TileState
{
    Empty = 0,
    Black = -1,
    White = 1
}

public class Reversi : MonoBehaviour {

    int BoardSize = 8;
    public Texture2D[] textureList; 
    private Board _Board;
    private Board tempBoard;

    public TileState currentColor = TileState.Black;
    public TileState aiColor = TileState.White;

    //ai
    private int lookAheadDepth = 3;
    private int forfeitWeight = 35;
    private int frontierWeight = 10;
    private int mobilityWeight = 5;
    private int stabilityWeight = 50;

    //private int lookAheadDepth = 2;
    //private int forfeitWeight = 2;
    //private int frontierWeight = 1;
    //private int mobilityWeight = 0;
    //private int stabilityWeight = 3;

    private static int maxRank = System.Int32.MaxValue - 64;

    private struct AIMove
    {
        public int row;
        public int col;
        public int rankOfMove;

        public AIMove(int row, int col)
        {
            this.row = row;
            this.col = col;
            this.rankOfMove = 0;
        }
    }

    private void DoAIMove(Board board)
    {
        int alpha = maxRank + 64;
        int beta = -alpha;
        AIMove bestAiMove = this.GetBestMove(this._Board, currentColor, 1, alpha, beta);
        print("AIMOVE x:" + bestAiMove.row + "y:" + bestAiMove.col);
        if (_Board.isValidMove(currentColor, bestAiMove.row, bestAiMove.col))
            this._Board.MakeMove(currentColor, bestAiMove.row, bestAiMove.col);
        else
            print("NOT VALID MOVE TRIED");

        reverseTurn();
        print("Move Made");
    }

    private AIMove GetBestMove(Board board, TileState color, int depth, int alpha, int beta)
    {
        AIMove bestMove = new AIMove(-1, -1);
        bestMove.rankOfMove = -(int)color * maxRank;

        //amount of valid moves
        int validMoves = board.GetValidMoveCount(color);

        int rowStart = Random.Range(0,BoardSize);
        int colStart = Random.Range(0, BoardSize);

        for (int i = 0; i < BoardSize; i++)
        {
            for (int j = 0; j < BoardSize; j++)
            {

                //int row = (rowStart + i) % 14;
                //int col = (colStart + j) % 14;

                int row = i;
                int col = j;

                if (board.isValidMove(color, row, col))
                {
                    AIMove tempMove = new AIMove(row, col);
                    Board testBoard = new Board(board);
                    //Debug.Log("NEW BOARD MADE");


                    testBoard.MakeMove(color, tempMove.row, tempMove.col);
                    int score = testBoard.WhiteCount - testBoard.BlackCount;

                    int nextColor = -(int)color;
                    TileState forfeit = TileState.Empty;

                    bool isEndGame = false;

                    int opponentValidMoves = testBoard.GetValidMoveCount((TileState)nextColor);
                    if(opponentValidMoves == 0)
                    {
                        forfeit = color;

                        nextColor = -nextColor;

                        if (!testBoard.HasAnyValidMove((TileState)nextColor))
                            isEndGame = true;
                    }
                    //checking for 4 depth
                    if(isEndGame || depth == this.lookAheadDepth)
                    {
                        if (isEndGame)
                        {
                            if (score < 0)
                                tempMove.rankOfMove = -maxRank + score;

                            // Positive value for white win.
                            else if (score > 0)
                                tempMove.rankOfMove = maxRank + score;

                            // Zero for a draw.
                            else
                                tempMove.rankOfMove = 0;
                        }
                        else
                        {
                            tempMove.rankOfMove =
                                this.forfeitWeight * (int)forfeit +
                                this.frontierWeight * (testBoard.BlackFrontierCount - testBoard.WhiteFrontierCount) +
                                this.mobilityWeight * (int)color * (validMoves - opponentValidMoves) +
                                this.stabilityWeight * (testBoard.WhiteSafeCount - testBoard.BlackSafeCount) + score;                                
                        }
                            //rank move depending on if forfeit or not
                            //the amount of counts on the frontier
                            // how many valid moves are left
                            // and the stablilty of the move
                            //tempMove.rankOfMove = forfeit;// + ()
                    }

                    else
                    {
                        AIMove nextMove = this.GetBestMove(testBoard, (TileState)nextColor, depth + 1, alpha, beta);

                        tempMove.rankOfMove = nextMove.rankOfMove;

                        if(forfeit != 0 && Mathf.Abs(tempMove.rankOfMove) < maxRank)
                        {
                            //increase rank depending on forfeit weight
                            tempMove.rankOfMove += (int)forfeit * forfeitWeight;
                        }

                        if (color == TileState.White && tempMove.rankOfMove > beta)
                            beta = tempMove.rankOfMove;
                        if (color == TileState.Black && tempMove.rankOfMove < alpha)
                            alpha = tempMove.rankOfMove;
                    }
                    if (color == TileState.White && tempMove.rankOfMove > alpha)
                    {
                        tempMove.rankOfMove = alpha;
            
                        return tempMove;
                    }

                    if (color == TileState.Black && tempMove.rankOfMove < beta)
                    {
                        tempMove.rankOfMove = beta;
                        return tempMove;
                    }

                    if (bestMove.row < 0)
                        bestMove = tempMove;

                    else if (((int)color * tempMove.rankOfMove > (int)color * bestMove.rankOfMove))
                        bestMove = tempMove;
                }
            }
        }
        return bestMove;

    }

    void Start()
    {
        var temp = new Board(BoardSize, textureList);
        _Board = temp;
    }

    void Update()
    {

    }

    void VisualBoard(int size)
    {
        Tile[,] tempArray = this._Board.tileArray;

        for(int i = 0; i <size; i++)
        {
            for(int j = 0; j < size; j++)
            {      
                if(this._Board.isValidMove(currentColor,i,j))
                {
                    if (GUI.Button(new Rect(i * 30 + size / 2, j * 30 + size / 2, 30, 30), tempArray[i, j].getTexture()))
                    {
                        this._Board.MakeMove(currentColor, i, j);
                        reverseTurn();
                        //Debug.Log(i + " " + j);

                    }
                }
                else
                {
                    GUI.Label(new Rect(i * 30 + size / 2, j * 30 + size / 2, 30, 30), tempArray[i, j].getTexture());
                }

            }
        }


        if(this._Board.GetValidMoveCount(aiColor) > 0)
        {
            if (GUI.Button(new Rect(16 * 30 + size / 2, 7 * 30 + size / 2, 100, 100), "AIMOVE"))
            {
                DoAIMove(this._Board);
            }
        }
        if(this._Board.GetValidMoveCount(TileState.White) > 0 || this._Board.GetValidMoveCount(TileState.Black) > 0)
            GUI.Label(new Rect(16 * 30 + size / 2, 5 * 30 + size / 2, 300, 50), "Current Turn: " + currentColor.ToString());
        else
            GUI.Label(new Rect(16 * 30 + size / 2, 5 * 30 + size / 2, 300, 50), "No moves left!");

        GUI.Label(new Rect(16 * 30 + size / 2, 3 * 30 + size / 2, 300, 50), "BlackCount: " + _Board.BlackCount);
        GUI.Label(new Rect(16 * 30 + size / 2, 3.5f * 30 + size / 2, 300, 50), "White Count: " + _Board.WhiteCount);


        if (GUI.Button(new Rect(16 * 30 + size / 2, 10.5f * 30 + size / 2, 100, 20), "Swap Color"))
        {
            reverseTurn();
        }

        if (GUI.Button(new Rect(16 * 30 + size / 2, 11.5f * 30 + size / 2, 100, 20), "Restart"))
        {
            Start();
        }

        if (GUI.Button(new Rect(16 * 30 + size / 2, 12.5f * 30 + size / 2, 120, 20), "Swap Board Size"))
        {
            if(BoardSize == 8)
            {
                BoardSize = 14;

            }
            else
            {
                BoardSize = 8;
            }
            Start();
        }


    }

    void reverseTurn()
    {
        if(currentColor == TileState.Black)
        {
            currentColor = TileState.White;
        }
        else
        {
            currentColor = TileState.Black;
        }

        //if(!_Board.HasAnyValidMove(currentColor))
        //{
        //    reverseTurn();
        //}

    }


    void OnGUI()
    {
        VisualBoard(BoardSize);
    }

    TileState nextTurnColor()
    {
        if (currentColor == TileState.Black)
            return TileState.White;
        else
            return TileState.Black;
    }
    
}




