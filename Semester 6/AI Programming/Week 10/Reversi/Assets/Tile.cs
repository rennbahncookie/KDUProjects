﻿using UnityEngine;
using System.Collections;

public class Tile
{
    private Texture[] textureList;
    private Texture currentTexture;
    private TileState state;

    public Tile(TileState newState, Texture[] texList)
    {
        state = newState;
        textureList = texList;
    }

    public void setState(TileState newState)
    {
        state = newState;
    }

    public Texture getTexture()
    {
        UpdateTexture();
        return currentTexture;
    }

    public Texture[] getTextureList()
    {
        return textureList;
    }

    public TileState getState()
    {
        return state;
    }

    void UpdateTexture()
    {
        currentTexture = textureList[(int)state+1];
    }


}
