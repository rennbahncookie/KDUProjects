      precision mediump float;
      void main()             
      { 
		//float r = abs(sin(gl_FragCoord.y*0.05)+sin(gl_FragCoord.x*0.05));
		//float g = abs(cos(gl_FragCoord.y*0.05)+0.3);
		//float b = abs(sin(gl_FragCoord.y*0.05)+0.6);
		
		
		float r = cos(gl_FragCoord.y*0.05)*0.5+0.5;
		float g = cos(gl_FragCoord.y*0.05+2.0)*0.5+0.5;
		float b = cos(gl_FragCoord.y*0.05+4.0)*0.5+0.5;
	
		
		gl_FragColor = vec4(r,g,b,1.0);	
        //gl_FragColor = vec4 ( 0.0, 1.0, 1.0, 1.0 );
      }                                            