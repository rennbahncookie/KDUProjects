#define GLFW_INCLUDE_ES2 1
#define GLFW_DLL 1
//#define GLFW_EXPOSE_NATIVE_WIN32 1
//#define GLFW_EXPOSE_NATIVE_EGL 1

#include <GLES2/gl2.h>
#include <EGL/egl.h>

#include <GLFW/glfw3.h>
//#include <GLFW/glfw3native.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <fstream>

#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600

GLint GprogramID = -1;

GLFWwindow* window;



static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

GLuint LoadShader(GLenum type, const char *shaderSrc)
{
	GLuint shader;
	GLint compiled;

	// Create the shader object
	shader = glCreateShader(type);

	if (shader == 0)
		return 0;

	// Load the shader source
	glShaderSource(shader, 1, &shaderSrc, NULL);

	// Compile the shader
	glCompileShader(shader);

	// Check the compile status
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);

	if (!compiled)
	{
		GLint infoLen = 0;

		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			// char* infoLog = malloc (sizeof(char) * infoLen );
			char infoLog[512];
			glGetShaderInfoLog(shader, infoLen, NULL, infoLog);
			printf("Error compiling shader:\n%s\n", infoLog);

			// free ( infoLog );
		}

		glDeleteShader(shader);
		return 0;
	}

	return shader;

}

GLuint LoadShaderFromFile(GLenum shaderType, std::string path)
{
	GLuint shaderID = 0;
	std::string shaderString;
	std::ifstream sourceFile(path.c_str());

	if (sourceFile)
	{
		shaderString.assign((std::istreambuf_iterator<char>(sourceFile)), std::istreambuf_iterator<char>());
		const GLchar* shaderSource = shaderString.c_str();

		return LoadShader(shaderType, shaderSource);
	}
	else
		printf("Unable to open file %s\n", path.c_str());

	return shaderID;
}

int Init(void)
{
	//UserData *userData = esContext->userData;
   /* char vShaderStr[] =
	   "attribute vec4 vPosition;    \n"
	   "void main()                  \n"
	   "{                            \n"
	   "   gl_Position = vPosition;  \n"
	   "}                            \n";

	char fShaderStr[] =
	   "precision mediump float;\n"\
	   "void main()                                  \n"
	   "{                                            \n"
	   "  gl_FragColor = vec4 ( 1.0, 1.0, 0.0, 1.0 );\n"
	   "}                                            \n";*/

	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint programObject;
	GLint linked;

	// Load From file
	vertexShader = LoadShaderFromFile(GL_VERTEX_SHADER, "../vertextShader1.vert");
	fragmentShader = LoadShaderFromFile(GL_FRAGMENT_SHADER, "../fragmentShader1.frag");

	// Load the vertex/fragment shaders
	//vertexShader = LoadShader ( GL_VERTEX_SHADER, vShaderStr );
	//fragmentShader = LoadShader ( GL_FRAGMENT_SHADER, fShaderStr );

	// Create the program object
	programObject = glCreateProgram();

	if (programObject == 0)
		return 0;

	glAttachShader(programObject, vertexShader);
	glAttachShader(programObject, fragmentShader);

	// Bind vPosition to attribute 0   
	glBindAttribLocation(programObject, 0, "vPosition");
	glBindAttribLocation(programObject, 1, "vColor");

	// Link the program
	glLinkProgram(programObject);

	// Check the link status
	glGetProgramiv(programObject, GL_LINK_STATUS, &linked);

	if (!linked)
	{
		GLint infoLen = 0;

		glGetProgramiv(programObject, GL_INFO_LOG_LENGTH, &infoLen);

		if (infoLen > 1)
		{
			//char* infoLog = malloc (sizeof(char) * infoLen );
			char infoLog[512];
			glGetProgramInfoLog(programObject, infoLen, NULL, infoLog);
			printf("Error linking program:\n%s\n", infoLog);

			//free ( infoLog );
		}

		glDeleteProgram(programObject);
		return 0;
	}

	// Store the program object
	GprogramID = programObject;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	return 1;
}

float rf(float temp, float factor1)
{
	return sin(temp + factor1)*0.5 - 0.5;
}

void Draw(void)
{
	static float factor1 = 0.0f;
	static float factor2 = 0.0f;
	factor1 += 0.01f;
	//factor2 += abs(sin(1 * 0.05));
	factor2 += sin(1*0.05);

	//factor1 += abs(cos(12))/100;
	//factor2 += abs(sin(20))/100;// (rand() % 1 + 100) / 100));

	GLint factor1Loc = glGetUniformLocation(GprogramID, "Factor1");
	GLint factor2Loc = glGetUniformLocation(GprogramID, "Factor2");



	if (factor1Loc != -1)
	{
		glUniform1f(factor1Loc, factor1);
		glUniform1f(factor2Loc, factor2);
	}



	/*  GLfloat vVertices[] = {0.0f,  0.5f, 0.0f,
							-0.5f, -0.5f, 0.0f,
							 0.5f, -0.5f,  0.0f};
	  */
	//GLfloat vVertices[] = { 0.5f,  0.5f, 0.0f,
	//						 0.5f, -0.5f, 0.0f,
	//						 -0.5f, -0.5f,  0.0f,
	//						 -0.5f, -0.5f, 0.0f,
	//						 -0.5f, 0.5f, 0.0f,
	//						 0.5f, 0.5f, 0.0f };

	GLfloat vVertices[] = { rf(0.5f,factor1),  rf(0.5f,factor2), 0.0f,
							rf(0.5f,factor2),  rf(-0.5f,factor1), 0.0f,
							rf(-0.5f,factor1), rf(-0.5f,factor2),  0.0f,
							rf(-0.5f,factor2), rf(-0.5f,factor1), 0.0f,
							rf(-0.5f,factor1), rf(0.5f,factor2), 0.0f,
							rf(0.5f,factor2),  rf(0.5f,factor1), 0.0f };

	GLfloat vColours[] = { 1.0f, 0.0f, 0.0f, 1.0f,
						 0.0f, 1.0f, 0.0f, 1.0f,
						 0.0f, 0.0f, 1.0f, 1.0f,
						 0.0f, 0.0f, 1.0f, 1.0f,
						 0.0f, 1.0f, 0.0f, 1.0f,
						 1.0f, 0.0f, 0.0f, 1.0f
	};
	/*
  GLfloat vVertices[] = {0.0f,  1.0f, 0.0f,
					  -1.0f, -1.0f, 0.0f,
					  1.0f, -1.0f,  0.0f};*/

					  // Set the viewport
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

	// Clear the color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	// Use the program object
	glUseProgram(GprogramID);

	// Load the vertex data
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, vVertices);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, vColours);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	glDrawArrays(GL_TRIANGLES, 0, 6);

}

int main(void)
{
	glfwSetErrorCallback(error_callback);

	// Initialize GLFW library
	if (!glfwInit())
		return -1;

	glfwDefaultWindowHints();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// Create and open a window
	window = glfwCreateWindow(WINDOW_WIDTH,
		WINDOW_HEIGHT,
		"Hello World",
		NULL,
		NULL);

	if (!window)
	{
		glfwTerminate();
		printf("glfwCreateWindow Error\n");
		exit(1);
	}

	glfwMakeContextCurrent(window);

	Init();

	// Repeat
	while (!glfwWindowShouldClose(window)) {


		Draw();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);
}
