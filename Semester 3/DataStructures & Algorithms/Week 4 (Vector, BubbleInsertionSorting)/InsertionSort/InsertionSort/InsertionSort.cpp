// InsertionSort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <time.h>
#include <cstdlib>

using namespace std;

vector<int>numberList;

void DisplayNumber()
{
	for(int i=0; i<numberList.size(); i++)
	{
		cout<<numberList[i]<<" ";
	}
}
void GenerateRandomNumber()
{
	for(int i=0; i <10; i++)
	{
		numberList.push_back(rand()%100);
	}
}
//! 0 1 2
//! 34  45
//! tempValue = 23
void InsertionSortASC()
{
	for(int i=0; i<numberList.size()-1; i++)
	{
		if(numberList[i]>numberList[i+1])
		{
			int tempValue = numberList[i+1];
			numberList.erase(numberList.begin() + i+1);	

			for(int j=0; j<numberList.size(); j++)
			{
				if(tempValue<numberList[j])
				{
					numberList.insert(numberList.begin()+j,tempValue);
					break;
				}
			}
		}
	}
}
int _tmain(int argc, _TCHAR* argv[])
{
	srand(time(NULL));
	cout<<"Before sorting"<<endl;
	GenerateRandomNumber();
	DisplayNumber();
	cout<<endl<<"After sorting"<<endl;
	InsertionSortASC();
	DisplayNumber();
	system("PAUSE");
	return 0;
}

