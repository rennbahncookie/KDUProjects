// StackQueueExercise2.1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <stack>
#include <string>
#include <queue>

using namespace std;

stack<char> myStack;
queue<char> myQueue;

int _tmain(int argc, _TCHAR* argv[])
{
	string str;
	bool isPalindrome = true;
	cout<<"enter a word: "<<endl;
	getline(cin,str);

	for(int i=0; i<str.length();i++)
	{
		//push the char to stack
		myStack.push(str[i]);
		myQueue.push(str[i]);
	}

	while(myStack.size()>0)
	{
		if(myStack.top()!=myQueue.front())
		{
			isPalindrome = false;
			break;
		}
		myStack.pop();
		myQueue.pop();
	}

	if(isPalindrome)
	{
		cout<<"The " << str <<" is palindrome"<<endl;
	}
	else
	{
		cout<<"The " << str <<" is not palindrome"<<endl;
	}
	cout<<endl;
	system("PAUSE");
	return 0;
}