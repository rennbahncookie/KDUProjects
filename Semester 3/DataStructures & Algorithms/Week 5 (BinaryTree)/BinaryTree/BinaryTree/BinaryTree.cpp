// BinaryTree.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

//! tree structure
struct node
{
	int key_value;
	node* left;
	node* right;
};
//! binary tree class
class btree
{
public:
	node* root;

	btree();
	~btree();
	void insert (int key);
	node*search(int key);
	void destroy_tree();
	void print(node* left, int &id);

private:
	void destroy_tree(node* left);
	void insert(int key, node* left);
	node* search(int key, node* left);
};

btree::btree()
{
	root = NULL;
}
btree::~btree()
{
	destroy_tree();
}
void btree::destroy_tree()
{
	destroy_tree(root);
}
void btree::destroy_tree(node* leaf)
{
	if(leaf != NULL)
	{
		destroy_tree(leaf->left);
		destroy_tree(leaf->right);
		delete leaf;
	}
}

void btree::insert(int key)
{
	if(root != NULL)
	{
		//! will pas in the key value with the exsisting root elements
		insert(key, root);
	}
	else //! root element == NULL
	{
		//! since it's an empty tree, the new node will be assigned as the root of the tree
		root = new node;
		//! initialize all the variables in the node struct
		root->key_value = key;
		root->left = NULL;
		root->right = NULL;
	}
}
void btree::insert(int key, node* leaf)
{
	//! take the key value compare with the root->key_value
	//! smaller value will be placed on the left, larger value will be placed on the right
	if(key<leaf->key_value)
	{
		if(leaf->left != NULL)
		{
			insert(key, leaf->left);
		}

		else
		{
			leaf->left = new node;
			leaf->left->key_value = key;
			leaf->left->left = NULL;
			leaf->left->right = NULL;
		}
	}
	else if(key>leaf->key_value)
	{
		if(leaf->right != NULL)
		{
			insert(key, leaf->right);
		}
		else
		{
			leaf->right = new node;
			leaf->right->key_value = key;
			leaf->right->left = NULL;
			leaf->right->right = NULL;
		}
	}
}
//! rearching in BT
node* btree::search(int key)
{
	return search(key, root);
}
node* btree::search(int key, node* leaf)
{
	if(leaf != NULL)
	{
		if(key == leaf->key_value)
			return leaf;
		if(key<leaf->key_value)
			return search(key, leaf->left);
		else
			return search(key, leaf->right);
	}
	else
		return NULL;
}
void btree::print(node* leaf, int &id)
{
	if(!leaf) return;
	print(leaf->left, id);
	id++;
	cout<<id<<" : "<<leaf->key_value<<endl;

	print(leaf->right,id);
}





int _tmain(int argc, _TCHAR* argv[])
{
	int num;
	int searchNum;
	btree bt;
	for(int i=0; i<5; i++)
	{
		cout<<"Enter a number: "<<endl;
		cin>>num;
		bt.insert(num);
	}
	cout<<endl<<"Search a number: "<<endl;
	cin>>searchNum;

	node* searchNode = bt.search(searchNum);
	if(searchNode != NULL)
	{
		cout<<"Node found! The value is: "<<searchNode->key_value<<endl;
	}
	else
	{
		cout<<"Not found! Please try again"<<endl;
	}
	int pNum = 0;
	bt.print(bt.root, pNum);
	system("PAUSE");
	return 0;
}

