#include <iostream>
#include <windows.h>
#include <time.h>
#include "conio_yp.h"

using namespace std;

//! enumeration
enum DIRECTION
{
    NORTH = 0,
    EAST,
    SOUTH,
    WEST,
    NONE,
    TOTAL_DIRECTION
};
enum GRID_TYPE
{
    SOURCE =0,
    TARGET,
    EMPTY,
    STRAIGHT_VERTICAL,
    STRAIGHT_HORIZONTAL,
    LSHAPE_0_DEGREE,
    LSHAPE_90_DEGREE,
    LSHAPE_180_DEGREE,
    LSHAPE_270_DEGREE,
    TSHAPE_0_DEGREE,
    TSHAPE_90_DEGREE,
    TSHAPE_180_DEGREE,
    TSHAPE_270_DEGREE,
    TOTAL_TYPE
};

struct Node
{
    GRID_TYPE type;
    char** sprite;
    int color;

    void Initialize(GRID_TYPE type);
    void SetType(GRID_TYPE type);
    void CopySprite(char tempSprite[][3]);
};

Node** gridNode;
int totalRow = 0;
int totalCol = 0;
int sourceRow = 0;
int sourceCol = 0;

bool isSolved = false;

//!initlaize the 2D array (3rows 2cols)
void Node::Initialize(GRID_TYPE type)
{
    for(int i=0; i<3; i++)
    {
        sprite = new char*[3];
        for(int j=0; j<3; j++)
        {
            sprite[i] = new char[3];
        }
    }
}
//!drawing the texture for the type
void Node::SetType(GRID_TYPE type)
{
    this->type = type;
    if(type == SOURCE)
    {
        color = YELLOW;
        char tempSprite[3][3] = {
                                    {'#','#','#'},
                                    {'#','#','#'},
                                    {'#','#','#'}
                                };
        CopySprite(tempSprite);
    }
    else if(type == TARGET)
    {
        color = BLUE;
        char tempSprite[3][3] = {
                                    {'#','#','#'},
                                    {'#','#','#'},
                                    {'#','#','#'}
                                };
        CopySprite(tempSprite);
    }
    else if(type == EMPTY)
    {
        color = YELLOW;
        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {' ',' ',' '},
                                    {' ',' ',' '}
                                };
        CopySprite(tempSprite);
    }
    else if(type == STRAIGHT_HORIZONTAL)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {'#','#','#'},
                                    {' ',' ',' '}
                                };
        CopySprite(tempSprite);
    }
    else if(type == STRAIGHT_VERTICAL)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {' ','#',' '},
                                    {' ','#',' '}
                                };
        CopySprite(tempSprite);
    }
    else if(type == LSHAPE_0_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {' ','#','#'},
                                    {' ',' ',' '}
                                };
        CopySprite(tempSprite);
    }
        else if(type == LSHAPE_90_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {' ','#','#'},
                                    {' ','#',' '}
                                };
        CopySprite(tempSprite);
    }
    else if(type == LSHAPE_180_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ',' ',' '},
                                    {'#','#',' '},
                                    {' ','#',' '}
                                };
        CopySprite(tempSprite);
    }
    else if(type == LSHAPE_270_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {'#','#',' '},
                                    {' ',' ',' '}
                                };
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_0_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {'#','#','#'},
                                    {' ','#',' '},
                                    {' ','#',' '}
                                };
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_90_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ',' ','#'},
                                    {'#','#','#'},
                                    {' ',' ','#'}
                                };
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_180_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {' ','#',' '},
                                    {' ','#',' '},
                                    {'#','#','#'}
                                };
        CopySprite(tempSprite);
    }
    else if(type == TSHAPE_270_DEGREE)
    {
        color = LIGHTBLUE;
        char tempSprite[3][3] = {
                                    {'#',' ',' '},
                                    {'#','#','#'},
                                    {'#',' ',' '}
                                };
        CopySprite(tempSprite);
    }
}
void Node::CopySprite(char tempSprite[][3])
{
    for(int i=0; i<3; i++)
    {
        for(int j=0; j<3; j++)
        {
            sprite[i][j] = tempSprite[i][j];
        }
    }
}
void MarkLink(Node* node, DIRECTION direction)
{

}
void Unlink()
{

}
bool ParseLink()
{

}
void ReinitializeLink()
{

}
//! design the map with different types of grid
void InitializeGrid()
{
    totalRow = 6;
    totalCol = 9;
    GRID_TYPE tempGrid[6][9] = {
                                    {               TARGET, TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,               EMPTY,               EMPTY, LSHAPE_270_DEGREE, STRAIGHT_HORIZONTAL,                TARGET},
                                    {               EMPTY,            EMPTY, STRAIGHT_HORIZONTAL,   LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,   TSHAPE_0_DEGREE,               EMPTY,                 EMPTY},
                                    {               EMPTY,LSHAPE_270_DEGREE,     TSHAPE_0_DEGREE,     TSHAPE_0_DEGREE,     TSHAPE_0_DEGREE,     TSHAPE_0_DEGREE,   TSHAPE_0_DEGREE,               EMPTY,                 EMPTY},
                                    {               TARGET, TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,               EMPTY, LSHAPE_270_DEGREE, STRAIGHT_HORIZONTAL,     LSHAPE_270_DEGREE},
                                    {               EMPTY,  TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE, STRAIGHT_HORIZONTAL,     TSHAPE_0_DEGREE,   LSHAPE_270_DEGREE, LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,                TARGET},
                                    {               EMPTY,LSHAPE_270_DEGREE,   LSHAPE_270_DEGREE,               EMPTY,              SOURCE,               EMPTY,             EMPTY,               EMPTY,                 EMPTY}
                               };
    gridNode = new Node*[totalRow];
    for(int i=0; i<totalRow; i++)
    {
        gridNode[i] = new Node[totalCol];
        for(int j = 0; j<totalCol; j++)
        {
            Node tempNode;
            tempNode.Initialize(tempGrid[i][j]);
            if(tempGrid[i][j] == SOURCE)
            {
                sourceRow = i;
                sourceCol = j;
            }
            gridNode[i][j] = tempNode;
        }
    }
    ReinitializeLink();
    ParseLink();
}
//! drawing
void DrawGrid()
{
    system("CLS");
    //!bottom col guide
    for(int i=0; i<totalCol; i++)
    {
        textcolor(WHITE);
        gotoxy(i * 4 + 3, 3* totalRow + 1 + totalRow);
        cout<<"-----";
        gotoxy(i * 4 + 5, 3* totalRow + 2 + totalRow);
        cout<<i;

    }
    //! left row guide
        for(int i=0; i<totalRow; i++)
    {
        textcolor(WHITE);
        gotoxy(1, i*4 + 4);
        cout<<i;
    }
    for(int i=0; i<totalRow;i++)
    {
        for(int j=0; j<totalCol; j++)
        {
            //!drawing the horizontal border
            textcolor(WHITE);
            int xPos = 2 * j + 3 + j;
            int yPos = 3 * i + 1 + i;

            //! drawing the left border
            gotoxy(xPos, yPos);
            cout<<"--------------";
            gotoxy(xPos + j, yPos + 1);
            cout<<"|";
            gotoxy(xPos + j, yPos + 2);
            cout<<"|";
            gotoxy(xPos + j, yPos + 3);
            cout<<"|";
            //! drawing the grid type
            textcolor(gridNode[i][j].color);
            for(int m=0; m<3; m++)
            {
                for(int n=0; n<3; n++)
                {
                    gotoxy(xPos+n+j+1, yPos+m+1);
                    cout<<gridNode[i][j].sprite[m][n];
                }
            }
        }
    }
    //! drawing for the right border
    for(int i=0; i<totalRow; i++)
    {
        textcolor(WHITE);
        int xPos = 2*totalCol + 3 + totalCol;
        int yPos = 3*i + 1 + i;
        gotoxy(xPos+totalCol, yPos+1);
        cout<<"|";
        gotoxy(xPos+totalCol, yPos+2);
        cout<<"|";
        gotoxy(xPos+totalCol, yPos+3);
        cout<<"|";
    }
}
void RotateGrid(int row, int col)
{
    if(gridNode[row][col].type == STRAIGHT_VERTICAL)
    {
        gridNode[row][col].SetType(STRAIGHT_HORIZONTAL);
    }
    else if(gridNode[row][col].type == STRAIGHT_HORIZONTAL)
    {
        gridNode[row][col].SetType(STRAIGHT_VERTICAL);
    }
    //! lshape rotation
    else if(gridNode[row][col].type == LSHAPE_0_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_90_DEGREE);
    }
    else if(gridNode[row][col].type == LSHAPE_90_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_180_DEGREE);
    }
    else if(gridNode[row][col].type == LSHAPE_180_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_270_DEGREE);
    }
    else if(gridNode[row][col].type == LSHAPE_270_DEGREE)
    {
        gridNode[row][col].SetType(LSHAPE_0_DEGREE);
    }
    //! tshape rotation
    else if(gridNode[row][col].type == TSHAPE_0_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_90_DEGREE);
    }
    else if(gridNode[row][col].type == TSHAPE_90_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_180_DEGREE);
    }
    else if(gridNode[row][col].type == TSHAPE_180_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_270_DEGREE);
    }
    else if(gridNode[row][col].type == TSHAPE_270_DEGREE)
    {
        gridNode[row][col].SetType(TSHAPE_0_DEGREE);
    }
}
void RotateGridMenu()
{
    int xGrid = -1;
    int yGrid = -1;
    textcolor(WHITE);
    gotoxy(1, totalRow * 3 + totalRow + 4);
    cout<<"Gride to rotate [x,y] e.g. 1 3: ";
    cin>>xGrid>>yGrid;

    RotateGrid(xGrid, yGrid);
    Unlink();
    ReinitializeLink();
    isSolved = ParseLink();
    system("PAUSE");
}

int main()
{
    system("mode con: cols= 80 lines = 30");
    InitializeGrid();
    DrawGrid();
    do
    {
        RotateGridMenu();
        DrawGrid();
    }while(!isSolved);

    return 0;
}
