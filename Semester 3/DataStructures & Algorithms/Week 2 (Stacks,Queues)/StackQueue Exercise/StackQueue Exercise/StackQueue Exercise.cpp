// StackQueue Exercise.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
#include "windows.h"
#include "stack"
#include "queue"
#include "string"

using namespace std;

stack<char> charStack;

void Display(stack<char> tempStack)
{
	cout<<"===================="<<endl;
	if(tempStack.empty())
	{
		cout<<"Stack is EMPTY"<<endl;
	}
	else
	{
		while(tempStack.size() > 0)
		{
			cout<<tempStack.top()<<endl;
			tempStack.pop();
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	//! store the user string input
	string str;
	cout<<"Please enter a word: "<<endl;
	// cin>>str; problem with spaces
	getline(cin,str);
	//! get each char from the string to be inserted
	for(int i=0; i<str.length(); i++)
	{
		//! push the char to stack
		charStack.push(str[i]);
	}
	while(charStack.size()>0)
	{
		cout<<charStack.top();
		charStack.pop();
	}
	cout << endl;
	system("PAUSE");
	return 0;
}