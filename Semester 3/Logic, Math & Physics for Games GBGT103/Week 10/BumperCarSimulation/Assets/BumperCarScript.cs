﻿using UnityEngine;
using System.Collections;

//! instructions:
//! 1. Simulate velocity and acceleration (5%)
//! 2. Simulate frictions & momentum (5%)
//! 3. Simulate collision respone (10%)
//! 

public class BumperCarScript : MonoBehaviour 
{
	float moveSpeed = 1.5f;
	//float resistance = 100.0f;

	Vector3 moveDirection;



	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		//! Initiate Movement
		//! use transform
		if(Input.GetKey(KeyCode.A))
		{
			moveDirection += new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime;
			//transform.Translate(new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(-moveSpeed, 0.0f, 0.0f) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.D))
		{
			moveDirection += new Vector3(moveSpeed, 0.0f, 0.0f) * Time.deltaTime;
			//transform.Translate(new Vector3(moveSpeed, 0.0f, 0.0f)  * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(moveSpeed, 0.0f, 0.0f)  * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.W))
		{
			moveDirection += new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime;
			//transform.Translate(new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(0.0f, 0.0f, moveSpeed) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.S))
		{
			moveDirection += new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime;
			//transform.Translate(new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(0.0f, 0.0f, -moveSpeed) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.R))
		{
			//transform.Translate(new Vector3(0.0f, moveSpeed, 0.0f) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(0.0f, moveSpeed, 0.0f) * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.F))
		{
			//transform.Translate(new Vector3(0.0f, -moveSpeed, 0.0f) * Time.deltaTime);
			//rigidbody.AddForce(new Vector3(0.0f, -moveSpeed, 0.0f) * Time.deltaTime);
		}
		//! Initiate Friction when release

	}

	void FixedUpdate()
	{
		moveDirection *= 0.995f;
		rigidbody.transform.position += moveDirection;

		if(moveDirection.magnitude <= float.Epsilon)
		{
			moveDirection = Vector3.zero;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.name == "Wall_3" || other.gameObject.name == "Wall_4")
		{
			moveDirection.x = moveDirection.x * -1.0f;
			rigidbody.transform.position += 2.0f * moveDirection * Time.fixedDeltaTime;
		}
		else if(other.gameObject.name == "Wall_1" || other.gameObject.name == "Wall_2")
		{
			moveDirection.z = moveDirection.z * -1.0f;
			rigidbody.transform.position += 2.0f * moveDirection * Time.fixedDeltaTime;
		}

		else if(other.gameObject.name =="BumperCarGreen")
		{
			//! F=ma
			moveDirection.x = moveDirection.x / rigidbody.mass * -3.0f;
			//rigidbody.transform.position += 2.0f * moveDirection * Time.fixedDeltaTime;
			moveDirection.y = moveDirection.y / rigidbody.mass * -3.0f;
			//rigidbody.transform.position += 2.0f * moveDirection * Time.fixedDeltaTime;
			//! m -> rigidbody.mass
			//! a -> moveDirection
		}
		else if(other.gameObject.name =="BumperCar")
		{
			moveDirection.y = moveDirection.y / rigidbody.mass * -1.0f;
		}

	}
}
